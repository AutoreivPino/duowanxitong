/**
 * Created by fujianguo on 2017/6/23.
 */

/**
 * 地区联动下拉选择
 * @returns {boolean}
 */
function getCity() {
    //获得省份下拉框的对象
    //获取省份
    var html = "";
    var val = $('#province').val();
    if (val === '请选择省份') {
        $("#city").html("<option>" + '请选择城市' + "</option>");
        return false;
    } else {
        var data = {}, hello = 'province';
        data[hello] = val;
        $.ajax({
            type: "POST",
            url: 'http://www.zaijiahome.com/trends/index.php?m=Activity&a=city',
            data: data,
            success: function (msg) {
                $("#city").empty();
                var data = JSON.parse(msg);
                data.forEach(function (v) {
                    html += "<option>" + v + "</option>";
                });
                $("#city").html(html);
            }
        });
    }
}

var watermark = [];

// 点击提交按钮前，循环所有drag，根据type不同，放置在表单里
$("form").submit(function (e) {

    var watermarks = $("#post").contents().find(".drag");

    for (var i = 0; i < watermarks.length; i++) {
        createInput(watermarks.eq(i));
    }
    //获取海报背景
    var imgUrl = $("#post").contents().find("input[name='imgUrl']");
    var img = imgUrl.val() || '';
    if (img) {
        $("input[name='backgroud_path']").val(img);
    }
    var params = JSON.stringify(watermark);
    console.dir(params);
    if (params) {
        $("input[name='poster_params']").val(params);
    }

    // e.preventDefault();



});

var createInput = function (ele) {
    var water = {
        type: ele.attr('type'),
        x: ele.css('left'),
        y: ele.css('top'),
        w: ele.width(),
        h: ele.height(),
    };
    watermark.push(water);
}