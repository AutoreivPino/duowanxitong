<?php
return array(
	'HOUSE'	=>	'房型管理',
	'HOUSE_INDEX' => '房型列表',
	'HOUSE_ADD'	=>	'添加房型',
	'HOUSE_EDIT' =>	'编辑房型',
	
	'SITE_KEY_HOME'=>'悦邸乐居',
	'SITE_KEY_AUTO'=>'丰尚汽车',
	'SITE_KEY_FASHION'=>'丰尚时尚',
	'NAME' => '房型名称',
	'HOUSE_TYPE' => '户型',
	'THUMBNAIL' => '户型缩略图',
	'CONTENT' => '房型介绍',
	'TAGS' => 'flash显示标签',
	'BRAND_TAG' => '开发商',
	'MONTH' => '月份',
	'3DFLASH' => '3dflash',	
	
	'NAME_EMPTY_TIP' =>	'名称不能为空',
	'NAME_UNIQUE' =>	'此房型已存在',
);
?>