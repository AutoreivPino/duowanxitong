<?php
$array = array(
	'GOODSCATEGORYTAGS'=>'商品分类标签管理',
	'GOODSCATEGORYTAGS_INDEX'=>'商品分类标签列表',
	'GOODSCATEGORYTAGS_SETTING'=>'修改商品分类标签',
	'RETURN_CATEGORY_LIST'=>'返回商品分类',
	'TAG_NAME'=>'标签名称',
	'TAG_TYPE'=>'标签类型',
		
	'TAGALL'=>'所有类型',
	'NORMAL'=>'普通标签',
	'STYLE'=>'风格',
	'ITEM'=>'元素',
	'SPEC'=>'款型',
	'BRAND'=>'品牌',
	'MATERIAL'=>'材质',
	'GOODS'=>'品类',
		
	'IS_INDEX'=>'是否首页显示',
	'IS_INDEX'=>'是否首页显示',
	'IS_HOT'=>'是否热度标签',
	'WEIGHT'=>'权重',
	'RIGHT_ALL'=>'<<<',
	'LEFT_ALL'=>'>>>',
);
return $array;
?>