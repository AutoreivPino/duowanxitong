<?php
return array(
	'WXAUTOANSWER'	=>	'自动回复',
	'WXAUTOANSWER_INDEX' => '回复列表',
	'WXAUTOANSWER_ADD'	=>	'添加自动回复',
	'WXAUTOANSWER_EDIT' =>	'编辑自动回复',
	
	'title' => '名称',	
	'AUTOTYPE' => '回复类型',	
	'GTYPE'=>'类型',
	'GRID'=>'关键词',
	'CONTENT'=>'文字',
	'MEDIA_PIC'=>'图片',
	'MEDIA_NEW'=>'图文',
	'MEDIA_NEW_OWN'=>'已选',
	'SORT'=>'排序',
	'TYPE_1'=>'文本',
	'TYPE_2'=>'图片',
	'TYPE_3'=>'图文',
	'ATYPE_1'=>'关注自动回复',
	
);
?>