<?php
return array(
	'ALBUMAUDIT'	=>	'专辑审核',
	'ALBUMAUDIT_LOG' => '审核日志',
	'ALBUMAUDIT_STATISTICS'	=>	'审核统计',
	
	'SOURCE'=>'所属站点',
	'AUDIT_ID' => '审核员',
	'ALBUM_ID' => '专辑名称',
	'EDITOR_ID' => '编辑',
	'STATUS' => '审核状态',
	'IDEA' => '评语',
	'TIME' => '时间',
	'PASS_NUMS' => '通过总数',
	'FAIL_NUMS' => '未通过总数',
	'RATE' => '未通过率',
	'TOTAL' => '总数'
);
?>