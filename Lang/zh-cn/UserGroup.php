<?php
return array(
	'USERGROUP'	=>	'用户组',
	'USERGROUP_INDEX'	=>	'用户组列表',
	'USERGROUP_ADD'=>'添加用户组',
	'USERGROUP_EDIT'=>'用户用户组',
	'NAME'=>'名称',
	'TYPE'=>'类型',
	'TYPE_SYSTEM'=>'系统内置',
	'TYPE_USER'=>'用户',
	'ACCESS'=>'前台权限设置',
	
	'NAME_REQUIRE'=>'用户组名称不能为空',
	'NAME_UNIQUE'=>'用户组名称已经存在',
	
	'GROUP_EXIST_USER'=>'用户组下存在用户,不能进行删除',
	'GROUP_SYSTEY_DEL'=>'系统内置用户组,不能进行删除',
);
return $array;
?>