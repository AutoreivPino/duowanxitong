<?php
$array = array(
	'MEDAL'=>'勋章管理',
	'MEDAL_INDEX'=>'勋章列表',
	'MEDAL_ADD'=>'添加勋章',
	'MEDAL_EDIT'=>'编辑勋章',
	'MEDAL_USER'=>'勋章会员',
	'MEDAL_SEND'=>'颁发勋章',
	'MEDAL_CHECK'=>'审核勋章',
	'NAME'=>'勋章名称',
	'IMAGE'=>'勋章图片',
	'GIVE_TYPE'=>'发放类型',
	'GIVE_TYPE_0'=>'自动发放',
	'GIVE_TYPE_1'=>'手动发放',
	'EXPIRATION'=>'勋章有效期',
	'CONDITIONS'=>'勋章自动颁发条件',
	'DESC'=>'勋章描述',
	'ALLOW_GROUP'=>'可领取用户组',
	'DAY'=>'天',
	'EXPIRATION_TIPS'=>'为空或为0则永不过期',
	
	'MEDAL_ALL'=>'所有勋章',
	'SEND_TYPE'=>'勋章获取途径',
	'SEND_ALL'=>'所有途径',
	'SEND_TYPE_0'=>'系统自动发放',
	'SEND_TYPE_1'=>'用户申请通过',
	'SEND_TYPE_2'=>'管理员发放',
	
	'USER'=>'会员',
	'RECOVER'=>'摘除',
	'CREATE_TIME'=>'发放日期',
	
	'SEND_MEDAL'=>'颁发勋章',
	'RECOVER_MEDAL'=>'摘除勋章',
	'BEIZU'=>'备注',
	'REASON'=>'申请陈述',
	
	'REASON'=>'申请陈述',
	'ADOPT'=>'通过',
	'REFUSE'=>'不通过',
	'USER_LEVEL'=>'用户等级',
	'CONDITIONS_CONTINUE_READ'=>'累计阅读文章篇数',
	'CONDITIONS_CONTINUE_READ_DAY'=>'每天阅读5篇连续天数',
	'CONDITIONS_CONTINUE_COLLECT'=>'累计收藏篇数',
	'CONDITIONS_CONTINUE_SHARE'=>'累计分享篇数',
	'CONDITIONS_CONTINUE_COMMENT'=>'累计评论篇数',
	'CONDITIONS_CONTINUE_LIFESTYLE'=>'累计发布生活态度篇数',
	'CONDITIONS_CONTINUE_LIFETRIPS'=>'累计发布生活经验篇数',
	'CONDITIONS_CONTINUE_COMMENT_USER'=>'被评论篇数',
	'CONDITIONS_CONTINUE_SHARE_USER'=>'被分享篇数',
	'CONDITIONS_CONTINUE_NEW_USER'=>'推荐新人',
	
	'CONDITIONS_TIPS'=>'当用户满足此条件时，勋章自动发放',
	'GIVE_TYPE_TIPS'=>'勋章添加成功后，此类型不可修改。自动发放是指满足设定条件后系统自动发放勋章；手动发放指用户申请或管理员手动颁发勋章。',
	'IMAGE_TIPS'=>'填写格式：直接输入图片名称即可，如 home.jpg<br/>图片规范：大图尺寸为120 X 120,小图尺寸为24 X 24，大小图片名称格式一致<br/>存放位置：大图位置public/medal/big目录，小图位置public/medal/small目录',
	'CONDITIONS_TIPS'=>'当用户满足此条件时，勋章自动发放。',
	'ALLOW_GROUP_TIPS'=>'全选或者全部不选择，则所有用户都可以申请',

	'CONFIRM_DELETE'=>'删除勋章将同时删除会员的相关勋章数据\r\n\r\n你确定要删除选择项吗？',
	
	'SEND_SUCCESS'=>'颁发成功',
);
return $array;
?>