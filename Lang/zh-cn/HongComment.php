<?php
$array = array(
	'HONGCOMMENT'=>'反馈管理',
	'HONGCOMMENT_INDEX'=>'反馈列表',
	'HONGCOMMENT_ADD'=>'添加反馈',
	'HONGCOMMENT_EDIT'=>'修改反馈',
	'HONGCOMMENT_INSERT'=>'添加反馈',
	'HONGCOMMENT_REMOVE'=>'删除反馈',
	
	'HONGCOMMENT_NAME'=>'反馈名称',
	'PASSWORD'=>'密码',
	'CONFIRM_PWD'=>'确认密码',
	'AVATAR'=>'头像',
	'DELETE_AVATAR'=>'删除头像',
	
	'CREDITS'=>'账号金额',
	'MONEY'=>'反馈金额',
	'GET_MONEY'=>'所得金额',
	'UP_MONEY'=>'税金',
	'TYPE'=>'状态',
	'STATUS'=>'显示',
	'CREATE_TIME'=>'申请时间',
	
	'FLLOWNUM'=>'关注人数',
	'FLLOWNUM1'=>'推荐关注',
	
	
	'SELECT_GROUP'=>'所有反馈',
	
	'SEARCH_HONGCOMMENT'=>'输入反馈名进行搜索',
	'EMPTY_HONGCOMMENT'=>'未搜索到反馈',
	
	'PASSWORD_TIPS'=>'如果不更改密码此处请留空',
	
	'HONGCOMMENT_NAME_REQUIRE'=>'反馈名称不能为空',
	'HONGCOMMENT_NAME_EXIST'=>'反馈名称已存在',
	'EMAIL_REQUIRE'=>'Email不能为空',
	'EMAIL_EXIST'=>'Email已存在',
	'PASSWORD_REQUIRE'=>'密码不能为空',
	'CONFIRM_ERROR'=>'密码和确认密码不一致',
	
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_HONGCOMMENT'=>'请选择要删除的反馈',
	'DELETE_TIPS_1'=>'删除反馈相关信息...',
	'DELETE_TIPS_2'=>'删除反馈发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除反馈发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除反馈成功',
);
return $array;
?>