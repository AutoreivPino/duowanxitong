<?php
return array(
	'USERMSG'	=>	'用户信件',
	'USERMSG_INDEX'	=>	'用户信件列表',
	'USERMSG_SHOW'=>'查看用户信件',
	'USERMSG_GROUPLIST'=>'系统信件列表',
	'USERMSG_GROUPSEND'=>'发送系统信件',
	'USERMSG_GROUPEDIT'=>'用户系统信件',
	
	'AUTHOR_ID'=>'发件人',
	'USERS'=>'用户',
	'TITLE'=>'标题',
	'CONTENT'=>'内容',
	'TIME'=>'时间',
	'END_TIME'=>'接收有效期至',
	'END_TIME_TIP'=>'在有效期内，用户登陆时将自动检测是否已接收此系统信件，设置为空则永不过期',
	'SHOW'=>'查看',
	'USER_TIPS'=>'用户组：设置接收信件的用户组，可以按住 CTRL 多选；<br/>接收用户：可接收系统信件用户，多个用户名用半角逗号 "," 隔开；<br/><span style="color:#f00;">用户组和接收用户必须设置其中一项，用于确定接收的用户；</span><br/>屏蔽用户：不可接收系统信件的用户，多个用户名用半角逗号 "," 隔开；',
	'USER_GROUP'=>'用户组',
	'USER_YES'=>'接收用户',
	'USER_NO'=>'屏蔽用户',
	
	'MSG_COUNT'=>'信件内容',
	'MSG_USER'=>'接收用户',
	
	'TITLE_REQUIRE'=>'信件标题不能为空',
	'CONTENT_REQUIRE'=>'信件内容不能为空',
	'USER_REQUIRE'=>'用户组和接收用户必须设置其中一项，用于确定接收的用户',
);
return $array;
?>