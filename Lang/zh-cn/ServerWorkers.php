<?php
return array(
	'SERVERWORKERS'	=>	'服务号员工',
	'SERVERWORKERS_INDEX' => '员工列表',
	'SERVERWORKERS_ADD'	=>	'添加员工',
	'SERVERWORKERS_EDIT' =>	'编辑员工',
	
	'USERNAME' => '昵称',	
	'SHOWNAME' => '姓名',	
	'GTYPE'=>'类型',
	'GRID'=>'员工',
	'CONTENT'=>'文字',
	'USERID'=>'身份证',
	'MEDIA_NEW'=>'图文',
	'MEDIA_NEW_OWN'=>'已选',
	'SORT'=>'排序',
	'TYPE_1'=>'文本',
	'TYPE_2'=>'图片',
	'TYPE_3'=>'图文',
	
);
?>