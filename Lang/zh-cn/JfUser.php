<?php
$array = array(
	'JFUSER'=>'用户管理',
	'JFUSER_INDEX'=>'用户列表',
	'JFUSER_ADD'=>'添加用户',
	'JFUSER_EDIT'=>'修改用户',
	'JFUSER_REMOVE'=>'删除用户',
	
	'USERNAME'=>'昵称',
	'PIC'=>'头像',
	'SCORES'=>'积分',
	'QD_NUM'=>'连续签到天数',
	'DELETE_AVATAR'=>'删除头像',
	'CREATE_TIME'=>'注册时间',
	'CREATE_DAY'=>'注册日期',
	
);
return $array;
?>