<?php
return array(
	'SERVERWXMENU'	=>	'服务号菜单',
	'SERVERWXMENU_INDEX' => '菜单列表',
	'SERVERWXMENU_ADD'	=>	'添加菜单',
	'SERVERWXMENU_EDIT' =>	'编辑菜单',
	
	'title' => '名称',	
	'GTYPE'=>'类型',
	'SORT'=>'排序',
	'TYPE_1'=>'跳转链接',
	'TYPE_2'=>'文字',
	'TYPE_3'=>'图片',
	'TYPE_4'=>'图文',
	'PARENT_ID'=>'所属菜单',
	'ROOT_ID'=>'顶级菜单',
	'SHOW_CHILD'=>'二级菜单',
	'RETURN_PREV'=>'返回上一级',
	'MEDIA_NEW'=>'图文',
	'MEDIA'=>'图片',
	'MEDIA_NEW_OWN'=>'已选图文',
	
);
?>