<?php
$array = array(
	'ALBUMSETTING'=>'配置管理',
	'ALBUMSETTING_INDEX'=>'专辑配置',
	'ALBUMSETTING_AUTOINDEX'=>'汽车配置',
	'ALBUMSETTING_FASHIONINDEX'=>'时尚配置',
	'AUTO_DEFAULT_TAGS'=>'汽车默认标签',
	'ALBUM_DEFAULT_TAGS'=>'杂志社默认标签',
	'FASHION_DEFAULT_TAGS'=>'时尚默认标签',
	
	'ALBUM_DEFAULT_TAGS_TIPS'=>'以空格分隔',
	'AUTO_DEFAULT_TAGS_TIPS'=>'以空格分隔',
	'FASHION_DEFAULT_TAGS_TIPS'=>'以空格分隔',
	'ALBUM_TAG_COUNT'=>'杂志社标签最大数量',
	'AUTO_TAG_COUNT'=>'汽车标签最大数量',
	'FASHION_TAG_COUNT'=>'时尚标签最大数量',
);
return $array;
?>