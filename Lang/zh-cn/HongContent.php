<?php
$array = array(
	'HONGCONTENT'=>'随机内容管理',
	'HONGCONTENT_INDEX'=>'随机内容列表',
	'HONGCONTENT_ADD'=>'添加随机内容',
	'HONGCONTENT_EDIT'=>'修改随机内容',
	'HONGCONTENT_INSERT'=>'添加随机内容',
	'HONGCONTENT_REMOVE'=>'删除随机内容',
	
	'CONTENT'=>'内容',
	'TYPE'=>'等级',
	
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_HONGCONTENT'=>'请选择要删除的随机内容',
	'DELETE_TIPS_1'=>'删除随机内容相关信息...',
	'DELETE_TIPS_2'=>'删除随机内容发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除随机内容发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除随机内容成功',
);
return $array;
?>