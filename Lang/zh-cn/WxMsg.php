<?php
$array = array(
	'WXMSG'=>'消息管理',
	'WXMSG_INDEX'=>'消息列表',
	'WXMSG_EDIT'=>'编辑消息',
	'WXMSG_ADD'=>'添加消息',
	
	'UNAME'=>'用户昵称',
	'OUTDATA'=>'导出数据',
	'MONEY'=>'金额',
	'GRID'=>'订单详情',
	'MTYPE'=>'类型',
	'SEND_TIME'=>'发送时间',
	'READ_TIME'=>'预订时间',
	'IS_READ'=>'是否已看',
	'IS_over'=>'是否已答',
	
	'ADD_SUBMIT'=>'提交',
	'TITLE_REQUIRE'=>'名称不能为空',
	
	'CONFIRM_DELETE'=>'删除文章将同时删除文章下的分享数据\r\n\r\n你确定要删除选择项吗？',
);
return $array;
?>