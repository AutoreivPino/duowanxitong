<?php
$array = array(
	'HONGMONEY'=>'提现管理',
	'HONGMONEY_INDEX'=>'提现列表',
	'HONGMONEY_ADD'=>'添加提现',
	'HONGMONEY_EDIT'=>'修改提现',
	'HONGMONEY_INSERT'=>'添加提现',
	'HONGMONEY_REMOVE'=>'删除提现',
	
	'HONGMONEY_NAME'=>'提现名称',
	'PASSWORD'=>'密码',
	'CONFIRM_PWD'=>'确认密码',
	'AVATAR'=>'头像',
	'DELETE_AVATAR'=>'删除头像',
	
	'CREDITS'=>'账号金额',
	'MONEY'=>'提现金额',
	'GET_MONEY'=>'所得金额',
	'UP_MONEY'=>'税金',
	'TYPE'=>'状态',
	'STATUS'=>'显示',
	'CREATE_TIME'=>'申请时间',
	
	'FLLOWNUM'=>'关注人数',
	'FLLOWNUM1'=>'推荐关注',
	
	
	'SELECT_GROUP'=>'所有提现',
	
	'SEARCH_HONGMONEY'=>'输入提现名进行搜索',
	'EMPTY_HONGMONEY'=>'未搜索到提现',
	
	'PASSWORD_TIPS'=>'如果不更改密码此处请留空',
	
	'HONGMONEY_NAME_REQUIRE'=>'提现名称不能为空',
	'HONGMONEY_NAME_EXIST'=>'提现名称已存在',
	'EMAIL_REQUIRE'=>'Email不能为空',
	'EMAIL_EXIST'=>'Email已存在',
	'PASSWORD_REQUIRE'=>'密码不能为空',
	'CONFIRM_ERROR'=>'密码和确认密码不一致',
	
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_HONGMONEY'=>'请选择要删除的提现',
	'DELETE_TIPS_1'=>'删除提现相关信息...',
	'DELETE_TIPS_2'=>'删除提现发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除提现发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除提现成功',
);
return $array;
?>