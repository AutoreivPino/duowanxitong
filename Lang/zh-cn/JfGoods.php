<?php
$array = array(
	'JFGOODS'=>'商品管理',
	'JFGOODS_INDEX'=>'商品列表',
	'JFGOODS_ADD'=>'添加商品',
	'JFGOODS_EDIT'=>'修改商品',
	'JFGOODS_REMOVE'=>'删除商品',
	
	'TITLE'=>'标题',
	'PIC'=>'头像',
	'DELETE_AVATAR'=>'删除头像',
	'CREATE_TIME'=>'注册时间',
	'CREATE_DAY'=>'注册日期',
	
	'U_SCORE'=>'兑换积分',
	'U_NUM'=>'商品数量',
	'L_NUM'=>'可兑换数量',
);
return $array;
?>