<?php
$array = array(
	'STAR'=>'小贴士',
	'STAR_INDEX'=>'贴士列表',
	'STAR_EDIT'=>'编辑文章',
	'STAR_ADD'=>'添加文章',
	'STAR_INSERT'=>'添加图片',
	
	'TITLE'=>'标题',
	'UNAME'=>'作者',
	'CID'=>'分类',
	'IMGS'=>'用户头像',
	'IS_INDEX'=>'推荐',
	'SORT'=>'排序',	
	'CONTENT'=>'文字内容',
	'STATUS'=>'显示',
	'EDIT_PIC'=>'图文',
	'ADD_SUBMIT'=>'提交',
	'TITLE_REQUIRE'=>'名称不能为空',
	
	'CONFIRM_DELETE'=>'删除文章将同时删除文章下的分享数据\r\n\r\n你确定要删除选择项吗？',
);
return $array;
?>