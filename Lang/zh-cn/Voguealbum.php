<?php
$array=array(
	"TITLES"=>"标题",
	"VOGUEALBUM"=>"大片管理",
	"VOGUEALBUM_INDEX"=>"大片列表",
	"MODEL"=>"model",
	"ISSHOW"=>"是否显示",
	"THUMBS"=>"缩略图",
	"VOGUEALBUM_EDIT"=>"编辑大片",
	"VOGUEALBUM_ADD"=>"添加大片",
	"STYLIST"=>"排版样式",
);

return $array;
?>
