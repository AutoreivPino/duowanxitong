<?php
$array = array(
	'ADV'=>'广告',
	'ADV_INDEX'=>'广告列表',
	'ADV_ADD'=>'添加广告',
	'ADV_EDIT'=>'编辑广告',
	
	'NAME'=>'广告名称',
	'POSITION_ID'=>'所属广告位',
	'POSITION_ALL'=>'所有广告位',
	'CODE'=>'广告图片',
	'TYPE'=>'广告类型',
	'TYPE_1'	=>	'图片广告',
	'TYPE_2'	=>	'FLASH广告',
	'TYPE_3'	=>	'自定义代码广告',
	'URL'=>'广告链接',
	'DESC'	=>	'广告描述',
	'TARGET_KEY'	=>	'指定到',
	'NAME_REQUIRE'=>'广告位名称不能为空',
    'EFFECTIVE_TIME'=>'广告生效日期',
    'INEFFECTIVE_TIME'=>'广告失效日期',
	'PRO_TYPE'=>'所属板块',
	'BELONG_DOMAIN'=>'所属网站',
    'GESHI'=>'广告生效、失效格式均为：2015-03-08 22:00:00',
	'CURRENT'=>'当前',
	'INTIVE_TIME'=>'失效时间',
	'EFTIVE_TIME'=>'开始时间',
	'WIDTH'=>'宽度',
	'HEIGHT'=>'高度',
);
   
return $array;
?>