<?php
return array(
	'CAR'	=>	'汽车车型管理',
	'CAR_INDEX' => '车型列表',
	'CAR_ADD'	=>	'添加车型',
	'CAR_EDIT' =>	'编辑车型',
	
	'SITE_KEY_HOME'=>'悦邸乐居',
	'SITE_KEY_AUTO'=>'丰尚汽车',
	'SITE_KEY_FASHION'=>'丰尚时尚',
	'NAME' => '汽车名称',
	'EN_NAME' => '汽车英文名称',
	'CONTENT' => '汽车介绍',
	'BRAND_TAG' => '品牌标签',
	'SERIES_TAG' => '车型系列',
	'ICON' => '略缩图',
	'NORM_IMG' => '标准图',
	'COVER_360' => '外观360',
	'COVER_720' => '外观720',
	'CAR_SHARELIST' => '分享关联',
	'CAR_ID'=>'车型编号',
	'SHARE_ID'=>'分享编号',
	'SHARE_NAME'=>'专辑名称',
	'SHARE_TYPE'=>'专辑类型',
	
	
	'NAME_EMPTY_TIP' =>	'名称不能为空',
	'ENNAME_EMPTY_TIP' =>	'英文名称不能为空',
	'NAME_UNIQUE' =>	'此车型已存在',
);
?>