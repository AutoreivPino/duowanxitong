<?php
$array = array(
	'MASTERALBUM'=>'文章',
	'MASTERALBUM_INDEX'=>'文章列表',
	'MASTERALBUM_EDIT'=>'编辑文章',
	'MASTERALBUM_ADD'=>'添加文章',
	'MASTERALBUM_INSERT'=>'添加图片',
	'MASTERALBUM_EDITPIC'=>'编辑图文',
	
	'TITLE'=>'标题',
	'CURL'=>'跳转链接',
	'IMGS'=>'图片',
	'IS_INDEX'=>'首页推荐',
	'SHOW_TYPE'=>'内容形式',
	'SHOW_TYPE_1'=>'图片',
	'SHOW_TYPE_2'=>'链接',
	'SORT'=>'排序',	
	'GRID'=>'文字内容',
	'STATUS'=>'显示',
	'ADD_SUBMIT'=>'下一步',
	'EDIT_PIC'=>'图文',
	'TITLE_REQUIRE'=>'名称不能为空',
	
	'CONFIRM_DELETE'=>'删除文章将同时删除文章下的分享数据\r\n\r\n你确定要删除选择项吗？',
);
return $array;
?>