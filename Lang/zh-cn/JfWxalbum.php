<?php
$array = array(
	'JFWXALBUM'=>'分享文章管理',
	'JFWXALBUM_INDEX'=>'文章列表',
	'JFWXALBUM_ADD'=>'添加文章',
	'JFWXALBUM_EDIT'=>'修改文章',
	'JFWXALBUM_REMOVE'=>'删除文章',
	
	'TITLE'=>'标题',
	'PIC'=>'图片',
	'DELETE_AVATAR'=>'删除图片',
	'CREATE_TIME'=>'注册时间',
	'CREATE_DAY'=>'注册日期',
	
	'U_SCORE'=>'兑换积分',
	'U_NUM'=>'商品数量',
	'L_NUM'=>'可兑换数量',
);
return $array;
?>