<?php
$array = array(
	'JFUSERADDRESS'=>'用户兑换商品管理',
	'JFUSERADDRESS_INDEX'=>'用户兑换商品列表',
	'JFUSERADDRESS_SENDMSG'=>'短信列表',
	'JFUSERADDRESS_ADDMSG'=>'发送短信',
	'JFUSERADDRESS_ADD'=>'添加用户兑换商品',
	'JFUSERADDRESS_EDIT'=>'修改用户兑换商品',
	'JFUSERADDRESS_REMOVE'=>'删除用户兑换商品',
	
	'TITLE'=>'商品名',
	'USERNAME'=>'用户名',
	'PIC'=>'头像',
	'CONTENTS'=>'内容',
	'DELETE_AVATAR'=>'删除头像',
	'CREATE_TIME'=>'注册时间',
	'CREATE_DAY'=>'注册日期',
	
	'U_SCORE'=>'兑换积分',
	'U_NUM'=>'用户兑换商品数量',
	'L_NUM'=>'可兑换数量',
	'UNAME'=>'姓名',
	'PHONE'=>'电话',
);
return $array;
?>