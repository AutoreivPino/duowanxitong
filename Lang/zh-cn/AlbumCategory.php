<?php
return array(
	'ALBUMCATEGORY'	=>	'导航菜单管理',
	'ALBUMCATEGORY_INDEX' => '菜单列表',
	'ALBUMCATEGORY_ADD'	=>	'添加菜单',
	'ALBUMCATEGORY_EDIT' =>	'编辑菜单',
	
	'SITE_KEY'=>'所属站点',	
	'SITE_KEY_'=>'暂未分类',		
	'SITE_KEY_HOME'=>'悦邸乐居',
	'SITE_KEY_AUTO'=>'丰尚汽车',
	'SITE_KEY_FASHION'=>'丰尚时尚',
	'NAME' => '名称',
	'cate' => '英文名称',
	'CHANNEL' => '所属频道',
	'IMG' => '图片',
	'IMG_HOVER' => '分类选中图片',
	'SEO_TITLE' => 'SEO标题',
	'SEO_KEYWORDS' => 'SEO关键字',
	'SEO_DESC' => 'SEO描述',
	
	'NAME_EMPTY_TIP' =>	'名称不能为空',
	'ALBUM_EXIST' =>	'已存在',
);
?>