<?php
$array = array(
	'USER'=>'用户管理',
	'USER_INDEX'=>'用户列表',
	'USER_ADD'=>'添加用户',
	'USER_EDIT'=>'用户用户',
	'USER_REMOVE'=>'删除用户',
	'GID'=>'用户组',
	'SITE_KEY'=>'所属站点',
	'SITE_KEY_'=>'暂未分类',
	'SITE_KEY_FASHION'=>'网擎动力',		
	'EMAIL'=>'Email',
	'SHOW_NAME'=>'昵称',
	'USER_NAME'=>'用户名称',
	'PASSWORD'=>'密码',
	'CONFIRM_PWD'=>'确认密码',
	'AVATAR'=>'头像',
	'DELETE_AVATAR'=>'删除头像',
	'REG_TIME'=>'注册时间',
	'REG_IP'=>'注册IP',
	
	'GENDER'=>'性别',
	'GENDER_0'=>'女',
	'GENDER_1'=>'男',
	'WEIBO'=>'个人博客',
	'RESIDE'=>'所在地',
	'INTRODUCE'=>'个人介绍',
	
	'LAST_TIME'=>'上次访问时间',
	'LAST_IP'=>'上次访问IP',
	'CREDITS'=>'积分',
	
	'SHARES'=>'分享',
	'PHOTOS'=>'分享图片',
	'READ_COUNT'=>'累计阅读',
	'COLLECT_COUNT'=>'累计收藏',
	'COMMENT_COUNT'=>'累计评论',
	'SHARE_COUNT'=>'累计分享',
	'LIFESTYLE_COUNT'=>'累计发布态度',
	'LIFETRIPS_COUNT'=>'累计发布生活',
	'COMMENT_OTHER'=>'被评论',
	'SHARE_OTHER'=>'被分享',
	'NEW_COUNT'=>'推荐新人',
	'MEDAL_COUNT'=>'勋章',
	'MISSION_COUNT'=>'任务',
	'ATTENTION_COUNT'=>'关注',
	'BEST_COUNT'=>'优质文章',
	'AVATAR_IMG'=>'头像',
	
	'TAB_1'=>'帐户信息',
	'TAB_2'=>'用户资料',
	'TAB_3'=>'用户统计',
	'TAB_4'=>'用户权限',
	
	'SELECT_GROUP'=>'所有用户',
	
	'SEARCH_USER'=>'输入用户名进行搜索',
	'EMPTY_USER'=>'未搜索到用户',
	
	'PASSWORD_TIPS'=>'如果不更改密码此处请留空',
	
	'USER_NAME_REQUIRE'=>'用户名称不能为空',
	'USER_NAME_EXIST'=>'用户名称已存在',
	'EMAIL_REQUIRE'=>'Email不能为空',
	'EMAIL_EXIST'=>'Email已存在',
	'PASSWORD_REQUIRE'=>'密码不能为空',
	'CONFIRM_ERROR'=>'密码和确认密码不一致',
	
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_USER'=>'请选择要删除的用户',
	'DELETE_TIPS_1'=>'删除用户相关信息...',
	'DELETE_TIPS_2'=>'删除用户发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除用户发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除用户成功',
);
return $array;
?>