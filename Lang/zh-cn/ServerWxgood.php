<?php
return array(
	'SERVERWXGOOD'	=>	'抽奖奖品',
	'SERVERWXGOOD_INDEX' => '奖品列表',
	'SERVERWXGOOD_ADD'	=>	'添加奖品',
	'SERVERWXGOOD_EDIT' =>	'编辑奖品',
	
	'title' => '名称',	
	'BEST_IMG' => '图片',
	'NUMS'=>'可用数量',
	'MIN_RAND'=>'最小角度',
	'MAX_RAND'=>'最大角度',
	'UNUMS'=>'总量',
	'SORT'=>'概率',
	'GTYPE'=>'类型',
	'G_LEVEL'=>'等级',
	
);
?>