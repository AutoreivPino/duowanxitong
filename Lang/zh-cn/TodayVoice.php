<?php
$array = array(
	'TODAYVOICE'=>'今日话题',
	'TODAYVOICE_ADD'=>'今日话题添加',
	'TODAYVOICE_PUSH'=>'今日话题发布',
	'ADD_SOUND'=>'添加今日话题',
	'CH_SOUND'=>'选择发布的今日话题',
	'ADD_URL'=>'URL路径',
	'ADD_IMG'=>'上传图片',
	'EXISTS_SOUND'=>'已存在今日话题',
	'ROLE_NAV_NAME_REQUIRE'=>'菜单名称名称不能为空',
);
return $array;
?>