<?php
$array = array(
	'MASTERCOMMENT'=>'评论',
	'MASTERCOMMENT_INDEX'=>'评论列表',
	'MASTERCOMMENT_EDIT'=>'编辑评论',
	'MASTERCOMMENT_ADD'=>'添加评论',

	'CONTENT'=>'评论内容',
	'STATUS'=>'显示',
	'CTYPE'=>'类别',
	'ADD_SUBMIT'=>'提交',
	'TITLE_REQUIRE'=>'名称不能为空',
	
	'CONFIRM_DELETE'=>'删除文章将同时删除文章下的分享数据\r\n\r\n你确定要删除选择项吗？',
);
return $array;
?>