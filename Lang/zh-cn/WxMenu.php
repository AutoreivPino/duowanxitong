<?php
return array(
	'WXMENU'	=>	'订阅号菜单',
	'WXMENU_INDEX' => '菜单列表',
	'WXMENU_ADD'	=>	'添加菜单',
	'WXMENU_EDIT' =>	'编辑菜单',
	
	'title' => '名称',	
	'GTYPE'=>'类型',
	'SORT'=>'排序',
	'TYPE_1'=>'跳转链接',
	'TYPE_2'=>'文字',
	'TYPE_3'=>'图片',
	'TYPE_4'=>'图文',
	'PARENT_ID'=>'所属菜单',
	'ROOT_ID'=>'顶级菜单',
	'SHOW_CHILD'=>'二级菜单',
	'RETURN_PREV'=>'返回上一级',
	'MEDIA_NEW'=>'图文',
	'MEDIA'=>'图片',
	'MEDIA_NEW_OWN'=>'已选图文',
	
);
?>