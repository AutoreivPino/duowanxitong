<?php
$array = array(
	'HONGTONG'=>'通知管理',
	'HONGTONG_INDEX'=>'通知列表',
	'HONGTONG_ADD'=>'添加通知',
	'HONGTONG_EDIT'=>'修改通知',
	'HONGTONG_INSERT'=>'添加通知',
	'HONGTONG_REMOVE'=>'删除通知',
	
	'HONGTONG_NAME'=>'通知名称',
	
	'TITLE'=>'标题',
	'CONTENT'=>'内容',
	'STATUS'=>'显示',
	'CREATE_TIME'=>'发送时间',
	
	'SELECT_GROUP'=>'所有通知',
	
	
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_HONGTONG'=>'请选择要删除的通知',
	'DELETE_TIPS_1'=>'删除通知相关信息...',
	'DELETE_TIPS_2'=>'删除通知发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除通知发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除通知成功',
);
return $array;
?>