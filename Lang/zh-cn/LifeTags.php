<?php
$array = array(
	'LIFETAGS'=>'生活态度标签管理',
	'LIFETAGS_INDEX'=>'态度标签列表',
	'LIFETAGS_ADD'=>'添加标签',
	'LIFETAGS_EDIT'=>'编辑标签',
	
	'CATE_NAME'=>'标签名',
	'CATE_ID'=>'编号',
	'CATE_ICON'=>'图标',
	'TYPE'=>'类型',
	'DESC'=>'描述',
);
return $array;
?>