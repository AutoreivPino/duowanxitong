<?php
$array = array(
	'MID'=>'管理者名称',
	'PEOPLE'=>'被管理者名称',
	'ROLEDETAIL'=>'个人权限管理',
	'ROLEDETAIL_ADD'=>'添加权限',
	'ROLEDETAIL_INDEX'=>'个人权限',
	'MOKUAI'=>'网站名称',
	'shenfen'=>'管理者所属身份',
	'REDIS'=>'管理范围',
	'ROLEDETAIL_EDIT'=>'个人权限编辑',
	'UID'=>'被管理人名',
	'TYPE'=>'类型',
	'FROM'=>'表名',
	'FASHION'=>'时尚家居编辑',
);
return $array;
?>