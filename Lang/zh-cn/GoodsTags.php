<?php
$array = array(
	'GOODSTAGS'=>'分享分类标签管理',
	'GOODSTAGS_INDEX'=>'分享分类标签列表',
	'GOODSTAGS_ADD'=>'添加分享分类标签',
	'GOODSTAGS_EDIT'=>'编辑分享分类标签',
	'WEIGHT'=>'权重',
	'TAG_NAME'=>'标签名称',
	'TAG_ENGLISH_NAME'=>'标签英文名称',
	'COUNT'=>'搜索数量',
	'TYPE'=>'标签类型',
	'IS_HOT'=>'热门',
	'TAG_TYPE'=>'标签类型',
	'SITE_KEY'=>'所属站点',
	'LOGO'=>'图标',
	'DESC'=>'seo描述',
	'CONTENT'=>'介绍',

	'TAGALL'=>'所有类型',
	'NORMAL'=>'普通标签',
	'STYLE'=>'风格',
	'ITEM'=>'元素',		
	'SPEC'=>'款型',
	'BRAND'=>'品牌',
	'MATERIAL'=>'材质',
	'GOODS'=>'品类',
	
	'TAG_NAME_REQUIRE'=>'标签名称不能为空',
	'TAG_NAME_UNIQUE'=>'标签名称已经存在',
);
return $array;
?>