<?php
$array = array(
	'SUBSCRIPTION'=>'邮件订阅管理',
	'SUBSCRIPTION_INDEX'=>'邮件订阅列表',
	'ID'=>'ID编号',
	'ADDTIME'=>'添加时间',
	'CONTENT'=>'内容简介',
	'SUBSCRIPTION_EDIT'=>'邮件订阅编辑',
	'SUBSCRIPTION_ADD'=>'邮件订阅添加',
	'TITLE'=>'订阅标题',
	'CONTETN'=>'URL路径',
	'SEND_CONTENT'=>'发布选项',
	'SHOW'=>'显示实例',
	'SUBMIT'=>'生成',
	'SUBSCRIPTION_EMAIL_SEND'=>'邮件生成',
	'IMG_ONE_TITLE'=>'图一标题',
	'IMG_TWO_TITLE'=>'图二标题',
	'IMG_THREE_TITLE'=>'图三标题',
	'IMG_ONE_CONTENT'=>'图一内容',
	'IMG_TWO_CONTENT'=>'图二内容',
	'IMG_THREE_CONTENT'=>'图三内容',
	'PREVIEW'=>'预览内容',
	'SUBSCRIPTION_PREVIEW'=>'预览页面',
	'SEND'=>'发送',
	);
return $array;
?>