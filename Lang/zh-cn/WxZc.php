<?php
$array = array(
	'WXZC'=>'星象文章',
	'WXZC_INDEX'=>'文章列表',
	'WXZC_EDIT'=>'编辑文章',
	'WXZC_ADD'=>'添加文章',
	'WXZC_INSERT'=>'添加图片',
	
	'TITLE'=>'标题',
	'CID'=>'分类',
	'IMGS'=>'图片',
	'IS_INDEX'=>'推荐',
	'SORT'=>'排序',	
	'CONTENT'=>'文字内容',
	'STATUS'=>'显示',
	'EDIT_PIC'=>'图文',
	'ADD_SUBMIT'=>'提交',
	'TITLE_REQUIRE'=>'名称不能为空',
	
	'CONFIRM_DELETE'=>'删除文章将同时删除文章下的分享数据\r\n\r\n你确定要删除选择项吗？',
);
return $array;
?>