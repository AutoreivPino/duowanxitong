<?php
$array = array(
	'USERPHONE'=>'用户管理',
	'USERPHONE_INDEX'=>'用户列表',
	'USERPHONE_ADD'=>'添加用户',
	'USERPHONE_EDIT'=>'修改用户',
	'USERPHONE_INSERT'=>'添加用户',
	'USERPHONE_REMOVE'=>'删除用户',
	
	'EDIT'=>'地址',
	'CONTENT'=>'内容',
	'TYPE'=>'等级',
	'AVATAR_IMG'=>'头像',
	'P_USE'=>'状态',
	'GTYPE'=>'类型',
	'OID'=>'用户OPENID',
	'CONFIRM_DELETE'=>'删除会员将同时删除会员所有的相关数据\r\n\r\n你确定要删除选择项吗？',
	
	'SELECT_USERPHONE'=>'请选择要删除的用户',
	'DELETE_TIPS_1'=>'删除用户相关信息...',
	'DELETE_TIPS_2'=>'删除用户发布杂志社%s到%s行...',
	'DELETE_TIPS_3'=>'删除用户发布分享%s到%s行...',
	'DELETE_TIPS_4'=>'删除用户成功',
);
return $array;
?>