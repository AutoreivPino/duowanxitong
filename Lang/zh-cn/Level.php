<?php
$array = array(
	'LEVEL'=>'等级管理',
	'LEVEL_INDEX'=>'等级列表',
	'LEVEL_ADD'=>'添加等级',
	'LEVEL_EDIT'=>'编辑等级',
	
	'NAME'=>'等级名称',
	'IMAGE'=>'图片',
	'DESC'=>'等级描述',
	'LEVEL_NUM'=>'等级数字',
	'CONDITIONS'=>'条件',
	'CONDITIONS_CONTINUE_SCORE'=>'用户积分',
	'CONDITIONS_CONTINUE_MEDAL'=>'勋章至少获取',
	'CONDITIONS_CONTINUE_MISSION'=>'任务至少完成',
	'CONDITIONS_CONTINUE_ATTENTION'=>'关注大账号',
	'CONDITIONS_CONTINUE_BEST'=>'优质内容',
	'NUM_COMMEN'=>'填写等级名称对应的数字（1,2,3...）如：初级，此处输入数字1即可；',
	'GIVE_TYPE_TIPS'=>'等级添加成功后，此类型不可修改。自动发放是指满足设定条件后系统自动发放勋章；手动发放指用户申请或管理员手动颁发勋章。',
	'IMAGE_TIPS'=>'填写格式：直接输入图片名称即可，如 home.jpg<br/>图片规范：大图尺寸为120 X 120,小图尺寸为24 X 24，大小图片名称格式一致<br/>存放位置：大图位置public/LEVEL/big目录，小图位置public/LEVEL/small目录',
	'CONDITIONS_TIPS'=>'当用户满足此条件时，用户自动获取。',
	'ALLOW_GROUP_TIPS'=>'全选或者全部不选择，则所有用户都可以申请',

	'CONFIRM_DELETE'=>'你确定要删除选择项吗？',
);
return $array;
?>