<?php
return array(
	'SERVERWXKW'	=>	'服务号关键词',
	'SERVERWXKW_INDEX' => '关键词列表',
	'SERVERWXKW_ADD'	=>	'添加关键词',
	'SERVERWXKW_EDIT' =>	'编辑关键词',
	
	'TITLE' => '名称',	
	'GTYPE'=>'类型',
	'GRID'=>'关键词',
	'CONTENT'=>'文字',
	'MEDIA_PIC'=>'图片',
	'MEDIA_NEW'=>'图文',
	'MEDIA_NEW_OWN'=>'已选',
	'SORT'=>'排序',
	'TYPE_1'=>'文本',
	'TYPE_2'=>'图片',
	'TYPE_3'=>'图文',
	
);
?>