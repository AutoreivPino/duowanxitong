<?php
define('SUB_DIR','/'.APP_NAME);
define('MANAGE_HANDLER',true);

session_start();
error_reporting(E_ALL ^ E_NOTICE);

require STRENDS_ROOT.'core/strends.php';

$strends = &STrendsService::instance();

$strends->is_admin = true;
$strends->is_session = false;
$strends->is_user = false;
$strends->is_cron = false;
$strends->is_misc = false;
$strends->cache_list = array();
$strends->initialize();

if(!MAGIC_QUOTES_GPC)
{
	$_GET = stripslashesDeep($_GET);
	$_POST = stripslashesDeep($_POST);
	$_COOKIE = stripslashesDeep($_COOKIE);
}

include_once STRENDS_ROOT.'./common/common.php';

?>