<?php

/**
 * +------------------------------------------------------------------------------
 * 用户模型
 * +------------------------------------------------------------------------------
 * @property integer $id
 * @property string $username
 * @property string $pic
 * @property integer $type
 * @property string $app_name
 * @property string $openid
 * @property string $unionid
 * @property integer $sex
 * @property integer $status
 * @property integer $follow_time
 * @property integer $isshare
 * @property integer $create_time
 * @property integer $join_time
 * @property string $server_oid
 * @property integer $scores
 * @property integer $activity_id
 * @property integer $hb
 * @property integer $kq
 * @property integer $qd_num
 * @property string $province
 * @property string $city
 * @property integer $parent_id
 * @property integer $gparent_id
 */
class JfUserModel extends CommonModel
{

    /**
     * 关注公众号，关联用户信息
     * @param $openid
     * @param $app_name
     * @return JfUserModel
     */
    public function relatedUser($openid, $app_name)
    {
        /** @var JfUserModel $user */
        D('JfUser');
        $user = new JfUserModel();

        $res = $user->where(['openid' => $openid, 'app_name' => $app_name])->find();

        $app = WeChatApp::instance();
        $weChatUser = $app->getUserInfo($openid);
        /** @var WeChatApp $app */

        // 已经存在，已参与过活动
        if ($res) {
            $user->follow_time += 1;
            $user->save();
        } else {
            // 新用户

            // 是不是老用户
            if ($user->where(['openid' => $weChatUser['openid']])->find()) {
                // 如果是老用户，删除新纪录的用户信息，更新至旧数据，更新积分记录
                $newUser = new JfUserModel();
                $arr = $newUser->where(['unionid' => $weChatUser['unionid'], 'app_name' => $app_name])->find();
                $newUser->delete();
                $user->where(['openid' => $weChatUser['openid']])->find();
                $user->openid = $weChatUser['openid'];
                $user->activity_id = $arr['activity_id'];
                $user->parent_id = $arr['parent_id'];
                $user->unionid = $weChatUser['unionid'];
                $user->pic = $weChatUser['headimgurl'];
                $user->sex = $weChatUser['sex'];
                $user->province = $weChatUser['province'];
                $user->city = $weChatUser['city'];
                $user->join_time = time();
                $user->app_name = $app_name;
                $user->follow_time = 1;
                $user->save();
                if ($activity = $user->getActivity()) {
                    $activity->sendAward($user);
                }
            } elseif ($user->where(['unionid' => $weChatUser['unionid'], 'app_name' => $app_name])->find()) {
                // 新用户：参与活动的新用户
                $user->openid = $weChatUser['openid'];
                $user->pic = $weChatUser['headimgurl'];
                $user->sex = $weChatUser['sex'];
                $user->province = $weChatUser['province'];
                $user->city = $weChatUser['city'];
                $user->join_time = time();
                $user->follow_time = 1;
                $user->app_name = $app_name;
                $user->save();
                if ($activity = $user->getActivity()) {
                    $activity->sendAward($user);
                }
            } else {
                // 新用户：未参与活动,直接关注公众号的用户

                $user->username = $weChatUser['nickname'];
                $user->pic = $weChatUser['headimgurl'];
                $user->openid = $weChatUser['openid'];
                $user->unionid = $weChatUser['unionid'];
                $user->sex = $weChatUser['sex'];
                $user->follow_time = 1;
                $user->province = $weChatUser['province'];
                $user->city = $weChatUser['city'];
                $user->app_name = $app_name;
                $user->join_time = $user->create_time = time();
                $user->add();
                $app->sendCustomMessage([
                    'touser' => $user->openid,
                    'msgtype' => 'text',
                    'text' => [
                        'content' => "欢迎",
                    ]
                ]);
            }
        }

    }

    /**
     * @return static
     */
    public function getParent()
    {

        $model = D('JfUser');
        $model->find($this->parent_id);
        return $model;
    }

    /**
     * @return ActivityModel
     */
    public function getActivity()
    {
        $model = D('Activity');
        $model->find($this->activity_id);
        return $model;
    }

    /**
     * 增加活动奖励
     * @param $type
     * @param $number
     * @param $from_user_id integer
     * @return boolean
     */
    public function addAward($type, $number, $from_user_id = null)
    {
        D('ActivityModel');
        $column = ActivityModel::$award[$type];
        $this->$column += $number;
        $res = $this->save();
		D('JfUser')->query('update tbl_jf_user set chb='.$this->column.' where id='.$this->id);
        if ($res) {
            /** @var JfRecordModel $model */
            $model = D('JfRecord');
            $model->user_id = $this->id;
            $model->from_user_id = $from_user_id;
            $model->type = $type;
            $model->number = $number;
            $model->created_at = time();
            $model->add();
        }

        return $res;
    }
}

?>