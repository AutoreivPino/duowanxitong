<?php
/**
 +------------------------------------------------------------------------------
 * 搭配分类模型
 +------------------------------------------------------------------------------
 */
class StyleCategoryModel extends CommonModel
{
	protected $_validate = array(
		array('cate_name','require','{%CATE_NAME_REQUIRE}'),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
}
?>