<?php

/**
 * Created by PhpStorm.
 * User: jingx
 * Date: 2017/6/26
 * Time: 9:52
 */
class MergeImageModel
{
    /**
     * @var resource
     */
    public $background;

    /**
     * 合成头像
     * @param $params array
     */
    protected function mergeImg($params)
    {
        $result = $this->imageCreateFromAny($params['content']);
        list($originWidth, $originHeight) = $result['imageSize'];

        $thumb = $this->ImageShrink(array_merge($result, $params));

        imagecopymerge($this->background, $thumb, $params['x'], $params['y'], 0, 0, $params['w'], $params['h'], 100);
    }

    /**
     * 合成昵称
     * @param $params array
     */
    protected function mergeName($params)
    {
        $im = imagecreatetruecolor(100, 30);
        $text_color = imagecolorallocate($im, 0, 0, 0);
        $text = $params['content'];
        $font = './public/FZHTJW.TTF';

        imagettftext($this->background, 20, 0, $params['x'], $params['y'], $text_color, $font, $text);
    }

    /**
     * 合成二维码
     * @param $params array
     */
    protected function mergeQr($params)
    {

        $result = $this->imageCreateFromAny(RES_ROOT.$params['content']);
        list($originWidth, $originHeight) = $result['imageSize'];
        $thumb = $this->ImageShrink(array_merge($result, $params));
        imagecopymerge($this->background, $thumb, $params['x'], $params['y'], 0, 0, $params['w'], $params['h'], 100);
    }


    /**
     * 缩小图片
     * @param $params
     * @return resource
     */
    protected function imageShrink($params)
    {
        $origin = $params['origin'];
        $imageSize = $params['imageSize'];
        $thumbWidth = $params['w'];
        $thumbHeight = $params['h'];
        list($originWidth, $originHeight, $originType, $originMime) = $imageSize;

        //缩放判断
        if (($thumbWidth / $originWidth) > ($thumbHeight / $originHeight)) {
            $scale = $thumbHeight / $originHeight;
        } else {
            $scale = $thumbWidth / $originWidth;
        }

        //对所求值进行取整
        $thumbWidth = floor($originWidth * $scale);
        $thumbHeight = floor($originHeight * $scale);
        //添加小图
        $thumb = imagecreatetruecolor($thumbWidth, $thumbHeight);

        //缩放函数
        imagecopyresampled($thumb, $origin, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $originWidth, $originHeight);

        return $thumb;
    }


    /**
     * @param $filePath
     * @return array
     */
    protected function imageCreateFromAny($filePath)
    {
        $type = getimagesize($filePath); // [] if you don't have exif you could use getImageSize()
        switch ($type[2]) {
            case IMAGETYPE_GIF :
                $im = imagecreatefromgif($filePath);
                $imSize = getimagesize($filePath);
                break;
            case IMAGETYPE_JPEG :
                $im = imagecreatefromjpeg($filePath);
                $imSize = getimagesize($filePath);
                break;
            case IMAGETYPE_PNG :
                $im = imagecreatefrompng($filePath);
                $imSize = getimagesize($filePath);
                break;
            case IMAGETYPE_BMP :
                $im = imagecreatefromwbmp($filePath);
                $imSize = getimagesize($filePath);
                break;
            default:
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $filePath);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // good edit, thanks!
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); // also, this seems wise considering output is image.
                $data = curl_exec($ch);
                curl_close($ch);

                $im = imagecreatefromstring($data);
                $imSize = getimagesizefromstring($data);
                break;
        }

        return [
            'origin' => $im,
            'imageSize' => $imSize,
        ];
    }

    /**
     * @param $params
     * @return bool
     * @throws Exception
     */
    public function generatePoster($params)
    {
        /** @var ActivityModel $activity */
        $activity = $params['activity'];
        $result = $this->imageCreateFromAny(RES_ROOT . $activity->backgroud_path);
        $this->background = $result['origin'];

        foreach ($params['watermarkParams'] as $param) {

            $methodName = 'merge' . ucfirst($param['type']);
            if (method_exists($this, $methodName)) {
                $this->$methodName($param);
            } else {
                throw new Exception('合成' . $param['type'] . '的方法未定义');
            }
        }

        return imagejpeg($this->background, $params['savePath']);
    }
}