<?php
/**
 +------------------------------------------------------------------------------
 * 后台商品分类标签模型
 +------------------------------------------------------------------------------
 */
class GoodsTagsModel extends CommonModel
{
	protected $_validate = array(
		array('tag_name','require','{%TAG_NAME_REQUIRE}'),
		array('tag_name','','{%TAG_NAME_UNIQUE}',0,'unique',2),
		array('tag_code','require','{%TAG_CODE_REQUIRE}'),
		array('tag_code','','{%TAG_CODE_UNIQUE}',0,'unique',2),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
	
	public function getTagIDsByType($tag_ids,$tagtype)
	{
		$list = $this->where('tag_id not in ('.$tag_ids.') and type='.$tagtype)->field('tag_id')->findAll();
		$ids = array();
		foreach($list as $tag)
		{
			$ids[] = $tag['tag_id'];
		}
	
		return $ids;
	}
}
?>