<?php
class AlbumEtaoModel extends CommonModel
{
	public $_validate = array(
		array('etao_id','require','{%ETAOID_EMPTY_TIP}'),
		array('uid','require','{%UID_EMPTY_TIP}'),
		array('album_id','require','{%ALBUMID_EMPTY_TIP}')
	);
}
?>