<?php
/**
 +------------------------------------------------------------------------------
 * 后台分类标签模型
 +------------------------------------------------------------------------------
 */
class GoodsCategoryTagsModel extends CommonModel
{
	public function getTags($cate_id)
	{
		return $this->where('cate_id = '.$cate_id)->findAll();
	}
	
	public function getTagIDs($cate_id)
	{
		$list = $this->where('cate_id = '.$cate_id)->field('tag_id')->findAll();
		$ids = array();
		foreach($list as $tag)
		{
			$ids[] = $tag['tag_id'];
		}
		
		return $ids;
	}
	
	public function getTagIDsByCateIDs($cateIds){
		$list = $this->where('cate_id in ('.$cateIds.')')->field('tag_id')->findAll();
		$ids = array();
		foreach($list as $tag)
		{
			$ids[] = $tag['tag_id'];
		}
		
		return $ids;
	}
}
?>