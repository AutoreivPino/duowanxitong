<?php
/**
 +------------------------------------------------------------------------------
 * 后台权限节点模型
 +------------------------------------------------------------------------------
 */
class RoleNodeModel extends CommonModel
{
	protected $_validate = array(
		array('module','require','{%ROLE_NODE_MODULE_REQUIRE}'),
	);
	
	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
}
?>