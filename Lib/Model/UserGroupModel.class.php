<?php
/**
 +------------------------------------------------------------------------------
 * 会员组模型
 +------------------------------------------------------------------------------
 */
class UserGroupModel extends CommonModel
{
	public $_validate = array(
		array('name','require','{%NAME_REQUIRE}'),
		array('name','','{%NAME_UNIQUE}',0,'unique',2),
	);
}
?>