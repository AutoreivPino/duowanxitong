<?php
/**
 +------------------------------------------------------------------------------
 * 后台导航模型
 +------------------------------------------------------------------------------
 */
class RoleNavModel extends CommonModel
{
	protected $_validate = array(
		array('name','require','{%ROLE_NAV_NAME_REQUIRE}'),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
}
?>