<?php
/**
 +------------------------------------------------------------------------------
 * 后台商品分类模型
 +------------------------------------------------------------------------------
 */
class GoodsCategoryModel extends CommonModel
{
	protected $_validate = array(
		array('cate_name','require','{%CATE_NAME_REQUIRE}'),
	);

	protected $_auto = array( 
		array('create_time','gmtTime',1,'function'),
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
	
	public function getCateIdsNotInSelCateId($cate_id){
		$list = $this->where('cate_code <> \'\' and cate_id <> '.$cate_id)->field('cate_id')->findAll();
		$ids = array();
		foreach($list as $tag)
		{
			$ids[] = $tag['cate_id'];
		}
		
		return $ids;
	}
}
?>