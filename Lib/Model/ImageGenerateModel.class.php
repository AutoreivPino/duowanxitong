<?php

/**
 * Created by PhpStorm.
 * User: YCJ
 * Date: 2017/6/13
 * Time: 11:01
 */
class ImageGenerateModel
{

    /**
     * @param $user JfUserModel
     * @param $activity ActivityModel
     * @return mixed
     */
    public function generateQrcode($user, $activity)
    {
        include_once __DIR__ . '/../ORG/phpqrcode/qrlib.php';

        $subPath = 'public/upload/images/' . date('Ym') . '/' . date('d') . '/';
        $imageRoot = RES_ROOT . $subPath;
        mkdir($imageRoot, 0777, true);
        $filename = $activity->id . '_qrcode_' . $user->openid . '.png';
        QRcode::png('http://' . $_SERVER['SERVER_NAME'] . U('WeChat/signup', ['activity_id' => $activity->id, 'open_id' => $user->openid]), $imageRoot . $filename);
        return $subPath . $filename;
    }

}