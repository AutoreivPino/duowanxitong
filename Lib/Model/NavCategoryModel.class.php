<?php
/**
 +------------------------------------------------------------------------------
 * 前台菜单分类模型
 +------------------------------------------------------------------------------
 */
class NavCategoryModel extends CommonModel
{
	protected $_validate = array(
		array('name','require','{%NAME_REQUIRE}'),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
}
?>