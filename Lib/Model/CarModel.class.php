<?php
/**
 +------------------------------------------------------------------------------
 * 汽车车型模型
 +------------------------------------------------------------------------------
 */
class CarModel extends CommonModel
{
	protected $_validate = array(
		array('name','require','{%NAME_EMPTY_TIP}'),
		array('en_name','require','{%ENNAME_EMPTY_TIP}'),
		array('name','','{%NAME_UNIQUE}',0,'unique',2),
	);

}
?>