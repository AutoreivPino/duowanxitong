<?php
/**
 +------------------------------------------------------------------------------
 * 前台菜单模型
 +------------------------------------------------------------------------------
 */
class NavModel extends CommonModel
{
	protected $_validate = array(
		array('name','require','{%NAME_REQUIRE}'),
		array('url','require','{%URL_REQUIRE}'),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1	
	);
}
?>