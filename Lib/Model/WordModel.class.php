<?php
/**
 +------------------------------------------------------------------------------
 * 敏感词模型
 +------------------------------------------------------------------------------
 */
class WordModel extends CommonModel
{
	public $_validate = array(
		array('word','require','{%NAME_REQUIRE}',2),
		array('word','','{%NAME_UNIQUE}',0,'unique',2),
	);

	protected $_auto = array( 
		array('status','1'),  // 新增的时候把status字段设置为1
	);
}
?>