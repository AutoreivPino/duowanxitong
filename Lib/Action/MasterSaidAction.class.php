<?php

class MasterSaidAction extends CommonAction
{
	public function index()
	{
		$master = D("MasterSaid")->find();
		$this->assign("master",$master);
		$this->display();
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$model = D("MasterSaid");
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					if($upload_item['key']=="bg_img")
					{
						$bg_img = $upload_item['recpath'].$upload_item['savename'];
						if(!empty($bg_img))
						{
							D("MasterSaid")->setField('bg_img',$bg_img);
						}
					}
					if($upload_item['key']=="right_img")
					{
						$right_img = $upload_item['recpath'].$upload_item['savename'];
						if(!empty($right_img))
						{
							D("MasterSaid")->setField('right_img',$right_img);
						}
					}
					if($upload_item['key']=="logo")
					{
						$logo = $upload_item['recpath'].$upload_item['savename'];
						if(!empty($logo))
						{
							D("MasterSaid")->setField('logo',$logo);
						}
					}
				}
			}
			
			$this->saveLog(1,$id);
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			$this->saveLog(1,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
}
?>