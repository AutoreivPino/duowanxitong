<?php
/**
 +------------------------------------------------------------------------------
 广告位
 +------------------------------------------------------------------------------
 */
class AdvPositionAction extends CommonAction
{
	public function add()
	{
		$adflashdir = STRENDS_ROOT."public/adflash/";
		$adflashlist = new Dir($adflashdir);
		$adflashs = array();
		foreach($adflashlist as $adflash)
		{
			if($adflash['ext'] == "swf")
				$adflashs[] = str_replace(".swf", "", $adflash['filename']);
		}
		$this->assign('adflashs',$adflashs);
		parent::add();
	}
	
	public function insert()
	{
		if(!isset($_POST['is_flash']))
		{
			$_POST['is_flash'] = 0;
			$_POST['flash_style'] = '';
		}
		
		$_POST['width'] = intval($_POST['width']);
		$_POST['height'] = intval($_POST['height']);
		parent::insert();
	}
	
	public function edit()
	{
		$adflashdir = STRENDS_ROOT."public/adflash/";
		$adflashlist = new Dir($adflashdir);
		$adflashs = array();
		foreach($adflashlist as $adflash)
		{
			if($adflash['ext'] == "swf")
				$adflashs[] = str_replace(".swf", "", $adflash['filename']);
		}
		$this->assign('adflashs',$adflashs);
		parent::edit();
	}
	
	public function update()
	{
		if(!isset($_POST['is_flash']))
		{
			$_POST['is_flash'] = 0;
			$_POST['flash_style'] = '';
		}
		$_POST['width'] = intval($_POST['width']);
		$_POST['height'] = intval($_POST['height']);
		parent::update();
	}
}

?>