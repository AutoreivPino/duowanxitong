<?php

class BaiduAction extends CommonAction
{
	public function index()
	{
		$file = RES_ROOT.'public/xml/home/schema.xml';
		$contents = file_get_contents($file);
		$this->assign("contents",$contents);
		$this->display();
	}
	public function indexzaijia()
	{
		$file = RES_ROOT.'public/xml/zaijia/schema.xml';
		$contents = file_get_contents($file);
		$this->assign("contents",$contents);
		$this->display();
	}
	public function add()
	{
		
		include '/server/www/zt/wxlist.php';
		$mode = D('AlbumXml');
		$file = RES_ROOT.'public/xml/home/schema.xml';
		$contents = file_get_contents($file);
		$con_arr = explode('</DOCUMENT>',$contents);
		$list = $mode->where('is_xml=0')->field('id,title,grid,content,best_img,create_time,content_url')->order('create_time desc,id asc')->select();
		if(count($list)>0){
			foreach($list as $k=>$v){
				if(!empty($v['content']) && isset($v['content'])){
					$mode->query('update tbl_album_xml set is_xml=1 where id='.$v['id']);
					$time = date('Y-m-d',$v['create_time']);
					$content .= '<item>
								<key>家居</key>
								<display>
								<title><![CDATA['.$v['title'].']]></title>
								<url><![CDATA['.$v['content_url'].']]></url>
								<provider>时尚家居TrendsHome</provider>
								<time>'.$time.'</time>
								<pagetype>image-text</pagetype>
							<contentlist>
								<content>   
									<type>image</type>
									<data><![CDATA['.$v['best_img'].']]></data>
									<width>800</width>
									<height>600</height>
									<alt><![CDATA['.$v['title'].']]></alt>
									<extradata><![CDATA['.$v['grid'].']]></extradata>
								</content>
								<content>
									<type>text</type>
									<data>
										<![CDATA['.$v['content'].']]>        
									</data>
									<extradata></extradata>
								</content>	        
							</contentlist>
							<adlist>
								<ad>
									<type>image</type>
									<imagesrc></imagesrc>
									<url></url>
								</ad>
							</adlist>
							<pix>
								<url></url>   
							</pix>
							<from></from>
						</display>
					</item>';
				}
				
			}
			$xml = $con_arr[0].$content.'</DOCUMENT>';
		}else{
			$xml = $contents;
		}		
		
		$file = RES_ROOT.'public/xml/home/schema.xml';
		file_put_contents($file,$xml);
		$this->redirect('Baidu/index');
		
	}
	public function addzaijia()
	{
		include '/server/www/zt/zaijialist.php';
		$mode = D('ZaijiaXml');
		$file = RES_ROOT.'public/xml/zaijia/schema.xml';
		$contents = file_get_contents($file);
		$con_arr = explode('</DOCUMENT>',$contents);
		$list = $mode->where('is_xml=0')->field('id,title,grid,content,best_img,create_time,content_url')->order('create_time desc,id asc')->select();
		if(count($list)>0){
			foreach($list as $k=>$v){
				if(!empty($v['content']) && isset($v['content'])){
					$mode->query('update tbl_zaijia_xml set is_xml=1 where id='.$v['id']);
					$time = date('Y-m-d',$v['create_time']);
					$content .= '<item>
								<key>家居</key>
								<display>
								<title><![CDATA['.$v['title'].']]></title>
								<url><![CDATA['.$v['content_url'].']]></url>
								<provider>在家ZaiJia</provider>
								<time>'.$time.'</time>
								<pagetype>image-text</pagetype>
							<contentlist>
								<content>   
									<type>image</type>
									<data><![CDATA['.$v['best_img'].']]></data>
									<width>800</width>
									<height>600</height>
									<alt><![CDATA['.$v['title'].']]></alt>
									<extradata><![CDATA['.$v['grid'].']]></extradata>
								</content>
								<content>
									<type>text</type>
									<data>
										<![CDATA['.$v['content'].']]>        
									</data>
									<extradata></extradata>
								</content>	        
							</contentlist>
							<adlist>
								<ad>
									<type>image</type>
									<imagesrc></imagesrc>
									<url></url>
								</ad>
							</adlist>
							<pix>
								<url></url>   
							</pix>
							<from></from>
						</display>
					</item>';
				}	
			}
			$xml = $con_arr[0].$content.'</DOCUMENT>';
		}else{
			$xml = $contents;
		}		
		
		$file = RES_ROOT.'public/xml/zaijia/schema.xml';
		file_put_contents($file,$xml);
		$this->redirect('Baidu/indexzaijia');
		
	}
	public function insert()
	{
		$mode = D('Album');
		
		$head = '<?xml version="1.0" encoding="UTF-8"?>
		<DOCUMENT content_method="full">';
		$foot = '</DOCUMENT>';
		
		$list = $mode->where('is_xml=0')->field('id,title,grid,best_img,online_time')->select();
		
		if(count($list)>0){
			foreach($list as $k=>$v){				
				$mode->query('update tbl_album set is_xml=1 where id='.$v['id']);
				
				$time = date('Y-m-d H:i',$v['online_time']);
				$content .= '<item>
							<key>家居</key>
							<display>
							<title>'.$v['title'].'</title>
							<url>http://www.zaijiahome.com/detail/'.$v['id'].'.html</url>
							<provider>在家ZaiJia</provider>
							<time>'.$time.'</time>
							<pagetype>image-text</pagetype>
						<contentlist>
							<content>   
								<type>image</type>
								<data>'.$v['best_img'].'</data>
								<width>800</width>
								<height>600</height>
								<alt>'.$v['title'].'</alt>
								<extradata></extradata>
							</content>
							<content>
								<type>text</type>
								<data>
									'.$v['grid'].'  
								</data>
								<extradata></extradata>
							</content>	        
						</contentlist>
						<adlist>
							<ad>
								<type>image</type>
								<imagesrc></imagesrc>
								<url></url>
							</ad>
						</adlist>
						<pix>
							<url></url>   
						</pix>
						<from></from>
					</display>
				</item>';
			}
		}else{			
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
		$xml = $head.$content.$foot;
		$file = RES_ROOT.'public/xml/baidu/schema.xml';
		file_put_contents($file,$xml);
		$this->saveLog(1,$id);
		
		$this->redirect('Baidu/index');
	}
}
?>