<?php

class SubscriptionAction extends CommonAction{

	public function update() {
		//B('FilterString');
		$name=$this->getActionName();
		$model = D ( $name );
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		// 更新数据
		$list=$model->save ();
		$id = $data[$model->getPk()];
		if (false !== $list) {
			//成功提示
			$this->saveLog(1,$id);
			
			$this->success (L('EDIT_SUCCESS'));
		} else {
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true){
		if(function_exists("mb_substr"))
		{
			if ($suffix && strlen($str)>$length)
				return mb_substr($str, $start, $length, $charset)."...";
			else
				return mb_substr($str, $start, $length, $charset);
		}elseif(function_exists('iconv_substr')){
			
			if ($suffix && strlen($str)>$length)
				return iconv_substr($str,$start,$length,$charset)."...";
			else
				return iconv_substr($str,$start,$length,$charset);
		}

		$re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
		$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
		$re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
		$re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
		preg_match_all($re[$charset], $str, $match);
		$slice = join("",array_slice($match[0], $start, $length));
		if($suffix)
			return $slice."…";
		return $slice;
	}
	
	public function send(){
		
		$adv=D('adv');
		$res=$adv->field('code,subcription_order,url,`desc`,`name`')->where('subcription=1 and current=1')->select();
		echo "<pre>"; var_dump($res); exit;
		foreach($res as $key=>$val){
			foreach($val as $k=>$v){
				if($k=="code"){
					$res[$key][$k]="http://s.trendstyle3d.com/".$v;
				}
				
				if($k=="subcription_order"){
					if($v==1){
						$data[0]=$key;
					}elseif($v==2){
						$data[1]=$key;
					}elseif($v==3){
						$data[2]=$key;
					}else{
						$data[]=$key;
					}
				}
			}
		}
		
		$res[$data[0]]['desc']=$this->msubstr($res[$data[0]]['desc'],0,70,"utf-8");
		$res[$data[1]]['desc']=$this->msubstr($res[$data[1]]['desc'],0,40,"utf-8");
		$res[$data[2]]['desc']=$this->msubstr($res[$data[2]]['desc'],0,40,"utf-8");
		
		
		$img_one_title=$res[$data[0]]['name'];
		$img_one_content=$res[$data[0]]['desc'];
		$img_two_title=$res[$data[1]]['name'];
		$img_two_content=$res[$data[1]]['desc'];
		$img_three_title=$res[$data[2]]['name'];
		$img_three_content=$res[$data[2]]['desc'];
		
		$p=array(
			'/\{IMG_ONE\}/',
			'/\{IMG_TWO\}/',
			'/\{IMG_THREE\}/',
			'/\{IMG_ONE_TITLE\}/',
			'/\{IMG_ONE_CONTENT\}/',
			'/\{IMG_TWO_TITLE\}/',
			'/\{IMG_TWO_CONTENT\}/',
			'/\{IMG_THREE_TITLE\}/',
			'/\{IMG_THREE_CONTENT\}/',
			'/\{IMG_ONE_URL\}/',
			'/\{IMG_TWO_URL\}/',
			'/\{IMG_THREE_URL\}/',
		);
		
		$replace=array(
			$res[$data[0]]['code'],
			$res[$data[1]]['code'],
			$res[$data[2]]['code'],
			$img_one_title,
			$img_one_content,
			$img_two_title,
			$img_two_content,
			$img_three_title,
			$img_three_content,
			$res[$data[0]]['url'],
			$res[$data[1]]['url'],
			$res[$data[2]]['url'],
		);
		
		$content_info=iconv("gb2312","UTF-8", $this->content());
		//$content_info=iconv("UTF-8","gb2312", $content_info);
		
		$content_info=preg_replace($p,$replace,$content_info);
		
		//  F:/AppServ/www/WEB/manli/Tpl/Default/Static/work.html  /data/www/3drich/manli/Tpl/Default/Public
		
		$dirfile="/data/www/resources/3drich/tpl/stylemode/work.html";
		//$dirfile="F:/AppServ/www/WEB/manli/Tpl/Default/Static/work.html";
	
		unlink($dirfile);
		header("content=text/html; charset=gb2312");
		file_put_contents($dirfile,$content_info);
		
		if($content_info){
			$this->saveLog(1,$id);
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function send_email(){
		$send=$_REQUEST['send'];
		$style_email=D('StyleEmail');
		$info=$style_email->select();
		
		foreach($info as $key=>$val){
			$email[$key]['email']=$val['email'];
			$email[$key]['id']=$val['id'];
		}  
		
		if($send=='send'){
		//if(1){
			//$dirfile="F:\AppServ\www\WEB\manli\Tpl\Default\Static\work.html";
			$dirfile="/data/www/resources/3drich/tpl/stylemode/work.html";
			
			 /*$email=array(
				'0'=>array("email"=>'1724827556@qq.com','id'=>'10'),
				'1'=>array("email"=>'xiaojie@3dtrendstyle.com','id'=>'11'),
				'2'=>array("email"=>'linchen@3dtrendstyle.com','id'=>'12'),
				'3'=>array("email"=>'12383503@qq.com','id'=>'13'),
				'4'=>array("email"=>'libin@3dtrendstyle.com','id'=>'13'),
			 ); */
			
			foreach($email as $key=>$val){
				$p=array(
				'/\{ID\}/',
				);
				$ids="";
				$ids=$val['id'];
				$replace=array(
					$ids,
				);
				$new_content=file_get_contents($dirfile);
				$new_content=preg_replace($p,$replace,$new_content);
				
				if(!empty($ids)){
					$new_content=preg_replace($p,$replace,$new_content);
				
					$nickname = 'StyleMode Weekly Digest';
					$email = $val['email'];
					
					$contents = iconv("UTF-8","gbk",$new_content);
					//$contents = iconv("UTF-8","gbk",$new_content);
					
					$nickname = "=?gb2312?B?".base64_encode($nickname)."?=";
					
					$flag=$this->face_mailsend($nickname,array($email),'',$contents);
				}
			}
		
		}
		
		if($flag){
			$this->saveLog(1,$id);
		
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	
	}
	public  function  preview(){
		$url=time();
		$this->assign('url',$url);
		$this->display();
	}

	public function face_mailsend($subject, $mailList, $mailCcList, $body, $attachment) {
		global $_D3RICH;
		import("@.ORG.phpmailer");
		
		$mail = new PHPMailer();
		
		$mail->IsSMTP();                       // set mailer to use SMTP
		$mail->SMTPAuth = true;                // turn on SMTP authentication
		$mail->Host     = 'smtp.126.com';   // specify main and backup server
		$mail->Username = 'd3rich@126.com';     // SMTP username
		$mail->Password = 't1mIa1wJ4';     // SMTP password  t1mIa1wJ4
		$mail->From     = 'd3rich@126.com';
		$mail->FromName = "StyleMode"; 
		
		/* $mail->Host     = 'smtp.stylemode.com';   // specify main and backup server
		$mail->Username = 'stylemode@stylemode.com';    // SMTP username
		$mail->Password = 'stylemode';     // SMTP password  t1mIa1wJ4
		$mail->From     = 'stylemode@stylemode.com';
		$mail->FromName = "三滴瑞淇"; */

		$mail->IsHTML(true);                   // set email format to HTML
		$mail->CharSet  = "GBK";            // set mail charset
		$mail->WordWrap = 80;                  // set word wrap to 80 characters

		$mail->Subject  = $subject;            // mail subject
		foreach($mailList as $k=>$v) {
			$mail->AddAddress($v);              // name is optional
		}
		foreach($mailCcList as $kc=>$vc) {
			$mail->AddCC($vc);              // name is optional
		}
		if(!empty($body)) {
			$mail->Body  = $body;               // set mail body
		}
		if(!empty($attachment)) {
			$mail->AddAttachment($attachment);  // add attachments
		}
		
		if($mail->Send()) {
			
			return true;
		} else {
			return false;
		}
	}
	
	public function  content(){
		
		$content='
			<body style="background:#FFF;">
			<style>
				div{border:0px;}
				p{border:0px;}
				img{border:0px;}
				a{border:0px;}
				.clear{clear:both;}
			</style>
			
				<center>
				<a target="_blank" href="javascript:;" style="border:0px;">
					<img width="780"  src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/logo.jpg" />
				</a>
				<table width="605">
					<tr align="center" width="605">
						<td colspan="2">
							<a target="_blank" href="{IMG_ONE_URL}" style="border:0px;">
								<img src="{IMG_ONE}" width="605"  />
							</a>
						</td>
					</tr>
					<tr>
						<td height="10"></td>
					</tr>
					<tr align="center" >
						<td colspan="2" >
							<a target="_blank" href="{IMG_ONE_URL}" style="color:#3A3A3A; text-decoration:none;font-size:20px;margin-top:20px; margin-bottom: 10px;">{IMG_ONE_TITLE}</a>
						</td>
					</tr>
					<tr>
						<td height="2"></td>
					</tr>
					<tr align="center" >
						<td colspan="2" width="605"  >
							
							<a target="_blank" href="{IMG_ONE_URL}" style="text-decoration:none;font-size:14px;letter-spacing:0.1em;margin:0px;padding:0px; line-height:24px;color:#606060;margin-bottom:10px;" >{IMG_ONE_CONTENT}</a>
							
						</td>
					</tr>
					<tr>
						<td height="10"></td>
					</tr>
					<tr >
						<td align="right" style="padding-right:8px" >
							
							<a target="_blank" href="{IMG_TWO_URL}">
								<img src="{IMG_TWO}" width="295"  />
							</a>
							
						</td>
						<td>
							
							<a target="_blank" href="{IMG_THREE_URL}">
								<img src="{IMG_THREE}" width="295"  />
							</a>
						
						</td>
					</tr>
					<tr>
						<td height="10"></td>
					</tr>
					<tr >
						<td align="center"  style="padding-right:8px">
							
							<a target="_blank" href="{IMG_TWO_URL}" style="color:#3A3A3A; text-decoration:none;">{IMG_TWO_TITLE}</a>
							
						</td>
						<td align="center" >
							
							<a target="_blank" href="{IMG_THREE_URL}" style="color:#3A3A3A; text-decoration:none; ">{IMG_THREE_TITLE}</a>
							
						</td>
					</tr>
					<tr>
						<td height="0"></td>
					</tr>
					<tr >
						<td align="center" style="padding-right:8px">
						
							<a target="_blank" href="{IMG_TWO_URL}" style="text-align:center;text-decoration:none;color:#979797;font-size:12px;letter-spacing:0.1em;line-height:20px;" >{IMG_TWO_CONTENT}</a>
						
						</td>
						<td align="center" >
							
							<a target="_blank" href="{IMG_THREE_URL}" style="text-align:center;text-decoration:none;color:#979797;font-size:12px;letter-spacing:0.1em;line-height:20px;" >{IMG_THREE_CONTENT}</a>
						
						</td>
					</tr>
					<tr>
						<td height="7"></td>
					</tr>
					<tr >
						<td align="right" style="padding-right:8px">
							
								<a target="_blank" href="http://www.stylemode.com.cn/index.php?action=guanzhu" >
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/guanzhu.jpg" width="295"  />
								</a>
							
						</td>
						<td>
							
								<a target="_blank" href="javascript:;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/img4.jpg" width="295"  />
								</a>
							
						</td>
					</tr>
					
					
				</table>
				<table width="702">
					<tr>
						<td height="15"></td>
					</tr>
					<tr align="center">
						<td colspan="2" style="border-bottom:1px solid black;border-top:1px solid black;">
							
								<a href="javascript:;" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share1.jpg" />
								</a>
								<a href="javascript:;" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share2.jpg" style="margin-left:25px;" />
								</a>
								<a href="javascript:;" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share3.jpg" style="margin-left:25px;" />
								</a>
								<a href="javascript:;" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share4.jpg" style="margin-left:25px;" />
								</a>
								<a href="javascript:;" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share5.jpg" style="margin-left:25px;" />
								</a>
								<a href="http://www.stylemode.com/index.php?action=sina" target="_blank" style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share6.jpg" style="margin-left:25px;" />
								</a>
								<a href="http://www.stylemode.com/index.php?action=tqq" target="_blank"  style="cursor:pointer;text-decoration:none;">
									<img src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/share7.jpg" style="margin-left:25px;" />
								</a>
							
						</td>
					</tr>
				</table>
				<table width="780">
					<tr  >
						<td colspan="2" >
							<img width="780" src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/footer_top.jpg" />
							
						</td>
					</tr>
					<tr>
						<td align="right"  width="780" >
							<a href="http://www.stylemode.com.cn/index.php?action=remove&id={ID}" target="_blank"  style="color:gray;cursor:pointer;text-decoration:none;font-size:14px;">退订邮件</a>
						</td>
					</tr>
					<tr>
						<td  colspan="2">
							<img width="780" src="http://www.stylemode.com.cn/manli/Tpl/Default/Static/Images/footer_bottom.jpg" />
						</td>
					</tr>
				</table>
				</center>
			</body>
			';

			return $content;
	
	}

}


?>