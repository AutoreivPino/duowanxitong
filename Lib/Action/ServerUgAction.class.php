<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class ServerUgAction extends CommonAction
{
	public function index()
	{
		$where = 'gid not in(1,6) ';
		$parameter = array();

		$model = D('ServerWxflUg');

		if(!empty($where))
			$where = 'WHERE 1 and ' . $where;

		$sql = 'SELECT COUNT(id) AS tcount FROM '.C("DB_PREFIX").'server_wxfl_ug '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'server_wxfl_ug '.$where;
		//echo $sql; exit;
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	
	public function insertinfo()
	{		
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		$list=$model->save($data);
		if ($list !== false)
		{	
			//$this->saveLog(1,$list);
			//$this->success (L('ADD_SUCCESS'));
			$this->display('Hong/add');
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	public function edit()
	{
		$id = intval($_REQUEST['id']);
		$d = D('ServerWxflUg')->where('id='.$id)->find();
		$uid = $d['uid'];
		$gid = $d['gid'];
		$vo = D('ZjUser')->where('id='.$uid)->find();
		$g = D('ServerWxgood')->where('id='.$gid)->find();
		$list = D('UserPhoneAddress')->where('uid='.$uid.' and gid='.$id)->find();
		$p_c = $list['p_c'];
		
		$pc_arr = explode('_',$p_c);
		$p = $pc_arr[0];
		$c = $pc_arr[1];
		
		$pname = D('Region')->where('id='.$p)->getField('name');
		$cname = D('Region')->where('id='.$c)->getField('name');
		
		$this->assign ('list', $list );
		$this->assign ('vo', $vo );
		$this->assign ('g', $g );
		$this->assign ('pname', $pname );
		$this->assign ('cname', $cname );
		
		$this->display();
	}

	public function update()
	{
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		$old_user = $model->getById($uid);
		
		$old_user_name = $old_user['user_name'];
		$old_email = $old_user['email'];
		
		$new_user_name = $_REQUEST['user_name'];
		$new_email = $_REQUEST['email'];
				
		if ($old_email == $new_email){
			$new_email = '';
		}

		if ($old_user_name == $new_user_name){
			$new_user_name = '';
		}
					
		
		if ($_REQUEST['password'] == ''){
			$new_pwd = '';
		}else{
			$new_pwd = $_REQUEST['password'];
		}

		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		if($_REQUEST['password'] == ''){
			unset($data['password']);
		}else{
			$password = $_REQUEST['password'];
			$data['password'] = md5($password);
		}
		
		$list=$model->save($data);
		if (false !== $list)
		{
			
			$avatar_img= '';
			if($upload_list = $this->uploadImages()){
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					
					if($upload_item['key'] == 'avatar_img')
					{
						D("Hong")->where('uid = '.$uid)->setField('avatar_img',$img);
					}elseif($upload_item['key'] == 'file_url')
					{
						D("Hong")->where('uid = '.$uid)->setField('file_url',$img);
					}
				}
			}
			
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}

	}

	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('avatar_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['avatar_img']))
						@unlink(RES_ROOT.$data['avatar_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}

function fllownum($uid){
	$list = D("ZjUser")->where('id='.$uid)->find();
	return $list['username'];
}
function fllownum1($good_id){
	$ulist = D('ServerWxgood')->where('id='.$good_id)->find();
	return $ulist['title'];
}
function fllownum2($uid){
	$ulist = D('ZjUser')->where('id='.$uid)->find();
	return "<img src='".$ulist['pic']."' width='50'>";
}
?>