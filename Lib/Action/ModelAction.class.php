<?php
/**
 +------------------------------------------------------------------------------
 * 模特管理
 +------------------------------------------------------------------------------
 */

class  ModelAction extends CommonAction{
	
	public function index() {
		//列表过滤器，生成查询Map对象
		$map = $this->_search ();
		if (method_exists ( $this, '_filter' )) {
			$this->_filter ( $map );
		}
		$user_names=$this->file_dirname("cctv/public/upload/models");
		$name=$this->getActionName();
		$model = D ($name);
		$a=$model->field('name')->select();
		foreach($a as $key=>$val){
			$user_group[]=$val['name'];
		}
		$new_user_group=array_unique($user_group);
		foreach($user_names as $key=>$val){
			if(!in_array($val,$new_user_group)){
				$time=date('Y-m-d H:i',time());
				$model_sql="insert into  tbl_model (name,create_time) values('".$val."','".$time."')";
				$model_res=mysql_query($model_sql);
				$mid=mysql_insert_id();
				$rank_sql="insert into tbl_model_rank(mid,rank_time) values(".$mid.",'".$time."')";
				$rank_res=mysql_query($rank_sql);
				$brofirm_sql="insert into tbl_model_brofirm(mid) values(".$mid.")";
				$brofirm_res=mysql_query($brofirm_sql);
			}
		}
		if (! empty ( $model )) {
			$this->_list ( $model, $map );
		}
		$this->display ();
		return;
	}


	public function insert(){
		$name=$this->getActionName();
		
		//var_dump($_POST);
		if(!empty($_POST['name'])){
			$key.="name,";
			$val.="'".$_POST['name']."',";
		}
		if(!empty($_POST['country'])){
			$key.="country,";
			$val.="'".$_POST['country']."',";
		}
		if(!empty($_POST['birthday'])){
			$key.="birthday,";
			$val.="'".$_POST['birthday']."',";
		}
		if(!empty($_POST['assess1'])){
			$key.="assess1,";
			$val.="'".$_POST['assess1']."',";
		}if(!empty($_POST['assess2'])){
			$key.="assess2,";
			$val.="'".$_POST['assess2']."',";
		}if(!empty($_POST['assess3'])){
			$key.="assess3,";
			$val.="'".$_POST['assess3']."',";
		}if(!empty($_POST['sex_sort'])){
			$key.="sex_sort,";
			$val.="'".$_POST['sex_sort']."',";
		}if(!empty($_POST['money_sort'])){
			$key.="money_sort,";
			$val.="'".$_POST['money_sort']."',";
		}
		if(!empty($_POST['contents'])){
			$key.="contents,";
			$val.="'".$_POST['contents']."',";
		}
		if(!empty($_POST['hot'])){
			$key.="hot,";
			$val.="'".$_POST['hot']."',";
		}
		
		if(!empty($_POST['tag'])){
			$tags=explode(" ",$_POST['tag']);
			$allnum ="";
			$num=count($tags);
			for($i=0;$i<$num;$i++){
				switch(trim($tags[$i])){
					case "最性感":
						$allnum+=1;
						break;
					case "最吸金":
						$allnum+=2;
						break;
					case "最顶级":
						$allnum+=4;
						break;
					default:
						$allnum="";
				}
			}
			$key.="tag,";
			$val.="'".$_POST['tag']."',";
		}
		$key.="create_time";
		$val.="'".date("Y-m-d H:i",time())."'";
		$model = D ( $name );
		$set=rtrim($set,",");
	    $sql="insert into  tbl_model (".$key." ) values(".$val.")";
		$model_res=$model->execute($sql);
		$mid=mysql_insert_id();
		if($model_res && mysql_affected_rows()){
			if(!empty($_POST['rank'])){
				$rank_arr=explode("|^|",$_POST['rank']);
				$time=date("Y-m-d H:i",time());
				if(is_array($rank_arr)){
					foreach($rank_arr as $key=>$val){
						$sql="insert into tbl_model_rank (mid,rank,rank_time) values('".$mid."','".$val."','".$time."')";
						$res_ranks=mysql_query($sql);
					}
				}
			}else{
				$this->saveLog(0,$role_id);
				$this->error (L('EDIT_ERROR'));
			}
			if(!empty($_POST['firm_name'])){
				$brofirm_arr=explode("|^|",$_POST['rank']);
				if(is_array($brofirm_arr)){
					foreach($brofirm_arr as $key=>$val){
						$sql="insert into  tbl_model_brofirm (mid,firm_name) values('".$mid."','".$val."')";
						$brofirm_res=mysql_query($sql);
					}
				}
			}else{
				$this->saveLog(0,$role_id);
				$this->error (L('EDIT_ERROR'));
			}
		}
		if ($model_res && mysql_affected_rows() || $res_ranks || $brofirm_res)
		{
			if($upload_list = $this->uploadImages(0,$_POST['name'],false,'',true,true,'old'))
			{
				foreach($upload_list as $upload_item)
				{
                    $img = $upload_item['recpath'].$upload_item['savename'];
                    if($upload_item['key'] == 'imgs' ||$upload_item['key'] == 'seximg' || $upload_item['key'] == 'moneyimg' || $upload_item['key'] == 'topimg'){
                        $model->where("id=".$model_id)->setField($upload_item['key'],$img);
                    }
				}
			}
			
			$this->saveLog(1,$model_id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$role_id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	
	public function add(){
		$this->display();
	}

	public function file_dirname($dir){
		$path=dirname(dirname(dirname(dirname(dirname(__FILE__)))));
		$newdir=$path.'/'.$dir;	
		if(file_exists($newdir)){
			$res=opendir($newdir);
			while($filename=readdir($res)){
				if($filename != "." && $filename!= ".."){
					$newpath=$newdir.'/'.$filename;
					if(is_file($newpath) or is_dir($newpath)){
						$name[]=$filename;
					}
				}				
			}
			return $name;	
		}
	}


	public function edit()
	{
		$mid=$_GET['id'];
		$Rank=D("ModelRank");
		$ranks=$Rank->field('rank')->where(array("mid"=>$mid))->select();
		$Rofirm=D("ModelBrofirm");
		$rofirms=$Rofirm->field('firm_name')->where(array("mid"=>$mid))->select();
		$model_list = D("model")->order('id desc')->findAll();
		foreach($ranks as $key=>$val){
			$ranks_new[]=$val['rank'];
		}
		foreach($rofirms as $key=>$val){
			$rofirms_new[]=$val['firm_name'];
		}
		
		$rank_new=implode(" |^| ",$ranks_new);
		$rofirm_new=implode(" |^| ",$rofirms_new);
		iconv("GB2312","UTF-8",$rank_new);		
		iconv("GB2312","UTF-8",$rofirm_new);		
		$this->assign("ranks",$rank_new);
		$this->assign("rofirms",$rofirm_new);
		$name = $this->getActionName();
		$model = D($name);
		$id = $_REQUEST [$model->getPk ()];
		$vo = $model->getById($id);

        $vo["effective_time"]=date("Y-m-d H:i:s",$vo["effective_time"]);
        $vo["ineffective_time"]=date("Y-m-d H:i:s",$vo["ineffective_time"]);
		switch($vo['tag']){
			case 7:
			$vo['tag']="最性感 最吸金 最顶级";
			break;
			case 4:
			$vo['tag']="最顶级 ";
			break;
			case 2:
			$vo['tag']="最吸金 ";
			break;
			case 1:
			$vo['tag']="最性感 ";
			break;
			case 5:
			$vo['tag']="最性感 最顶级 ";
			break;
			case 3:
			$vo['tag']="最性感 最吸金 ";
			break;
			case 6:
			$vo['tag']="最顶级 最吸金 ";
			break;
		}
		
		$this->assign ( 'vo', $vo );
		$this->display ();
	}
	
	public function update()
	{
		$model_id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		
		//var_dump($_POST);
		
		$set="name='".$_POST['name']."',";
		
		$set.="country='".$_POST['country']."',";
		$set.="birthday='".$_POST['birthday']."',";
		$set.="contents='".addslashes($_POST['contents'])."',";
		$set.="assess1='".addslashes($_POST['assess1'])."',";
		$set.="assess2='".addslashes($_POST['assess2'])."',";
		$set.="assess3='".addslashes($_POST['assess3'])."',";
		$set.="hot='".addslashes($_POST['hot'])."',";
		$set.="sex_sort='".(int)$_POST['sex_sort']."',";
		$set.="money_sort='".(int)$_POST['money_sort']."',";
		if(!empty($_POST['tag'])){
			$tags=explode(" ",$_POST['tag']);
			$allnum ="";
			$num=count($tags);
			$tags=array_filter($tags);
			for($i=0;$i<$num;$i++){
				$test=trim($tags[$i]);
				switch($test){
					case "最性感":
						$allnum+=1;
						break;
					case "最吸金":
						$allnum+=2;
						break;
					case "最顶级":
						$allnum+=4;
						break;
					default:
						$allnum+="";
				}
			}
			$set.="tag='".$allnum."',";
		}
		$time=date("Y-m-d H:i",time());
		$set.="create_time='".$time."'";
		if(!empty($_POST['rank'])){
			$rank_arr=explode("|^|",$_POST['rank']);
			$res_sql="select rank from tbl_model_rank where id='".$model_id."'";
			$sel_res=mysql_query($res_sql);
			$sql="delete from tbl_model_rank where mid='".$model_id."'";
			$del_ranks=mysql_query($sql);
			if($del_ranks){
				foreach($rank_arr as $key=>$val){
					$val=trim($val);
					$sql="insert into  tbl_model_rank (rank,mid,rank_time) values('".$val."','".$model_id."','".$time."')";
					$res_ranks=mysql_query($sql);
				}
			}
		}
		if(!empty($_POST['firm_name'])){
			$brofirm_arr=explode("|^|",$_POST['firm_name']);
			$sql="delete from tbl_model_brofirm where mid='".$model_id."'";
			$del_brofirm=mysql_query($sql);
			if($del_brofirm){
				foreach($brofirm_arr as $key=>$val){
					$val=trim($val);
					$sql="insert  into tbl_model_brofirm (mid,firm_name) values('".$model_id."','".$val."')";
					$brofirm_res=mysql_query($sql);
				}
			}
		}
		$model = D ( $name );
		$sql="update tbl_model set ".$set." where id=".$model_id;
		
		$model_res=$model->execute($sql);
		if ($model_res && mysql_affected_rows() || $res_ranks || $brofirm_res)
		{
			if($upload_list = $this->uploadImages(0,$_POST['name'],false,'',true,true,'old'))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'imgs' ||$upload_item['key'] == 'seximg' || $upload_item['key'] == 'moneyimg' || $upload_item['key'] == 'topimg'){
						$model->where("id=".$model_id)->setField($upload_item['key'],$img);
                    }
				}
			}
			
			$this->saveLog(1,$model_id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$role_id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove() {
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$Rank = D("ModelRank");
			$Brofirm = D("ModelBrofirm");
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$Rank_where = array ("mid" => array ('in', explode ( ',', $id ) ) );
			$Brofirm_where = array ("mid" => array ('in', explode ( ',', $id ) ) );
			if(false !== $model->where ( $condition )->delete () && false !== $Rank->where ( $Rank_where )->delete () && false !== $Brofirm->where ( $Brofirm_where )->delete () )
			{
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}

	
}
