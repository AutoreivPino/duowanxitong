<?php

class JfWxalbumAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$keyword = trim($_REQUEST['keyword']);
		
		if(!empty($keyword))
		{
			$this->assign("keyword",$keyword);
			$parameter['keyword'] = $keyword;
			$where.=" AND title LIKE '%".mysqlLikeQuote($keyword)."%' ";
		}
		
		$model = M();

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(DISTINCT id) AS scount 
			FROM '.C("DB_PREFIX").'jf_wxalbum '.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['scount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'jf_wxalbum '.$where;
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	
	public function update()
	{
        $id = intval($_REQUEST['id']);
		
		$model = D ('JfWxalbum');
		$album = $model->getById($id);
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$url = $_POST['url'];	
		
		$content = file_get_contents($url);
		
		$content = str_replace('data-src','src',$content);

		preg_match_all("/<img([^>]*)\s*src=('|\")([^'\"]+)('|\")/", $content,$matches);

		$new_arr=array_unique($matches[0]);//去除数组中重复的值

		foreach($new_arr as $key=>$val){
			$i_arr = explode('src=',$new_arr[$key]);
			$ppp = $i_arr[1];
			$ppp=substr($ppp,1,strlen($ppp));  
			$pppp = substr($ppp,0,-1);  
			
			$path = $pppp.'&amp;wxfrom=5&amp;wx_lazy=1';
			$path = str_replace('0?wx_fmt=jpeg','640?wx_fmt=jpeg',$path);
			$path = str_replace('0?wx_fmt=png','640?wx_fmt=png',$path);
			//echo $path; exit;
			$picurl=get_name($path,$key);//这里处理图片并得到处理后的地址
			
			$content = str_replace($pppp,$picurl,$content);
		  
		}
		
		$data['content'] = $content;
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where("id=".$id)->setField("pic",$img);
				}
			}
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('JfWxalbum');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		$url = $_POST['url'];	
		
		$content = file_get_contents($url);
		$content = str_replace('data-src','src',$content);

		preg_match_all("/<img([^>]*)\s*src=('|\")([^'\"]+)('|\")/", $content,$matches);

		$new_arr=array_unique($matches[0]);//去除数组中重复的值

		foreach($new_arr as $key=>$val){
			$i_arr = explode('src=',$new_arr[$key]);
			$ppp = $i_arr[1];
			$ppp=substr($ppp,1,strlen($ppp));  
			$pppp = substr($ppp,0,-1);  
			
			$path = $pppp.'&amp;wxfrom=5&amp;wx_lazy=1';
			$path = str_replace('0?wx_fmt=jpeg','640?wx_fmt=jpeg',$path);
			$path = str_replace('0?wx_fmt=png','640?wx_fmt=png',$path);
			//echo $path; exit;
			$picurl=get_name($path,$key);//这里处理图片并得到处理后的地址
			
			$content = str_replace($pppp,$picurl,$content);
		  
		}
		
		$data['content'] = $content;
		// 更新数据
		$list=$model->data($data)->add();
		
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where("id=".$list)->setField("pic",$img);
				}
			}
			$this->saveLog(1,$id);
			$this->success (L('ADD_SUCCESS'));

		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
                
				D('JfWxalbum')->where('id='.$aid)->delete();
				
            }

			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}
}
function get_name($pic_item,$key)    
{         
	$p_arr = explode('wx_fmt=',$pic_item);
	$p_type = $p_arr[1];
	if(strpos($p_type,'png')==0){
		$type = '.png';
	}else{
		$type='.jpeg';
	}
	$img_data = @file_get_contents($pic_item);
	if(!empty($img_data)){
		$mtime = date('Ym');
		$dtime = date('d');
		$dir = RES_ROOT.'public/upload/images/'.$mtime.'/'.$dtime.'/';
		
		if(!file_exists($dir)){
			mkdir(RES_ROOT.'public/upload/images/'.$mtime.'/'.$dtime.'/');
		}
		$num = rand(1000000,9999999);
		$img_index = 'public/upload/images/'.$mtime.'/'.$dtime.'/'.$num.$key.$type;
		$img_size = @file_put_contents(RES_ROOT.$img_index, $img_data);
		return PIC_ROOT.$img_index; 
	}else{
		return;   
	}      
	 
}
?>