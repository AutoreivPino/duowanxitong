<?php
/**
 +------------------------------------------------------------------------------
 车型分类
 +------------------------------------------------------------------------------
 */
class CarAction extends CommonAction
{
	public function add()
	{
		$category_list = D("GoodsTags")->where('site_key="auto"')->order('sort asc')->findAll();
		$this->assign("category_list",$category_list);
		$this->display();
	}
	
	public function insert()
	{
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		//保存当前数据对象
		//$data['create_time'] = gmtTime();
		$data['create_time'] = date('Y-m-d H:i:s');
		//var_dump($data);die;
		$list=$model->add($data);
		if ($list !== false)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'icon')
						$model->where("id=".$list)->setField("icon",$img);
					elseif($upload_item['key'] == 'norm_img')
						$model->where("id=".$list)->setField("norm_img",$img);
					elseif($upload_item['key'] == 'cover_360')
						$model->where("id=".$list)->setField("cover_360",$img);
					elseif($upload_item['key'] == 'cover_720')
						$model->where("id=".$list)->setField("cover_720",$img);
				}
			}
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
	}
	
	public function sharelist(){
		$model = D("sharelist");
		$sql = 'SELECT cs.car_id,cs.share_id,c.name,s.title,s.type FROM '.C("DB_PREFIX").'car_share_related AS cs LEFT JOIN '.C("DB_PREFIX").'car AS c ON c.id=cs.car_id LEFT JOIN '.C("DB_PREFIX").'share as s on cs.share_id = s.share_id';
		$content_list = $model->query($sql);
		for( $i=0; $i<count($content_list); $i++){
			if($content_list[$i]['type']=='album'){
				$content_list[$i]['type'] = '专辑';
			}else{
				$content_list[$i]['type'] = '图片';	
			}
		}
		$this->assign("content_list",$content_list);
		$this->display ();
	}
	
	public function edit()
	{
		$id = intval($_REQUEST['id']);
		$vo = D("Car")->getById($id);
		$this->assign ( 'vo', $vo );
		
		$category_list = D("GoodsTags")->where('site_key="auto"')->order('sort asc')->findAll();
		$this->assign("category_list",$category_list);
		
		$this->display();
	}

	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				$car = $model->getById($id);
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'icon')
					{
						if(!empty($car['icon']))
							@unlink(STRENDS_ROOT.$car['icon']);
						$model->where("id=".$id)->setField("icon",$img);
					}
					if($upload_item['key'] == 'norm_img')
					{
						if(!empty($car['norm_img']))
							@unlink(STRENDS_ROOT.$car['norm_img']);
						$model->where("id=".$id)->setField("norm_img",$img);
					}
					elseif($upload_item['key'] == 'cover_360')
					{
						if(!empty($car['cover_360']))
							@unlink(STRENDS_ROOT.$car['cover_360']);
						$model->where("id=".$id)->setField("cover_360",$img);
					}
					elseif($upload_item['key'] == 'cover_720')
					{
						if(!empty($car['cover_720']))
							@unlink(STRENDS_ROOT.$car['cover_720']);
						$model->where("id=".$id)->setField("cover_720",$img);
					}
				}
			}
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}

	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();

			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('icon,cover_360,cover_720')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['icon']))
						@unlink(STRENDS_ROOT.$data['icon']);
					
					if(!empty($data['cover_360']))
						@unlink(STRENDS_ROOT.$data['cover_360']);
					
					if(!empty($data['cover_720']))
						@unlink(STRENDS_ROOT.$data['cover_720']);
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}
?>