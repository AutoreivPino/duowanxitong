<?php
// +----------------------------------------------------------------------
// | 方维购物分享网站系统 (Build on ThinkPHP)
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: awfigq <awfigq@qq.com>
// +----------------------------------------------------------------------
/**
 +------------------------------------------------------------------------------
 * 商品标签管理
 +------------------------------------------------------------------------------
 */
class GoodsTagsAction extends CommonAction
{
    public function index()
	{
		$where = '';
		$parameter = array();
		$tag_name = trim($_REQUEST['tag_name']);
		$tagtype = trim($_REQUEST['tagtype']);
		
		if(strlen($tagtype)>0)
			$where .= ' AND type = '.$tagtype;
		else $tagtype = "-1";
		$tagtype = intval($tagtype);
		$this->assign("tagtype",$tagtype);
		
		if(!empty($tag_name))
		{
			$this->assign("tag_name",$tag_name);
			$parameter['tag_name'] = $tag_name;
			$where .= " AND tag_name LIKE '%".mysqlLikeQuote($tag_name)."%'";
		}

		$model = M();

		if(!empty($where))
			$where = 'WHERE 1' . $where;

		$sql = 'SELECT COUNT(tag_id) AS tcount
			FROM '.C("DB_PREFIX").'goods_tags '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'goods_tags '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'tag_id');
		$this->display();
	}

    public function insert()
	{
		$_POST['tag_code'] = $_POST['tag_name'];
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		//保存当前数据对象
		$list=$model->add($data);
		if ($list !== false)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'logo')
						$model->where("tag_id=".$list)->setField("logo",$img);
				}
			}
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
	}

	public function update()
	{
		$id = intval($_REQUEST['tag_id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				$goodstags = $model->getById($id);
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'logo')
					{
						if(!empty($goodstags['logo']))
							@unlink(STRENDS_ROOT.$goodstags['logo']);
						$model->where("tag_id=".$id)->setField("logo",$img);
					}
				}
			}
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}

	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$model = D("GoodsTags");
			$condition = array('tag_id' => array('in',explode (',',$id)));
			$datas = $model->where($condition)->field('logo')->findAll();
			if(false !== $model->where ( $condition )->delete())
			{
				foreach($datas as $data)
				{
					if(!empty($data['logo']))
						@unlink(STRENDS_ROOT.$data['logo']);
				}
				$this->saveLog(1,$id);
				
				D("GoodsCategoryTags")->where($condition)->delete();
				$this->saveLog(1,$id);
			}
			else
			{
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}

	public function search()
	{
		$cid = intval($_REQUEST['cid']);
		$sid = intval($_REQUEST['sid']);
		$key = trim($_REQUEST['key']);
		$type = intval($_REQUEST['type']);
		$tagtype = $_REQUEST['tagtype'];
		$tag_name = trim($_REQUEST['tag_name']);
		$custom_tags = trim($_REQUEST['custom_tags']);

		$where = '';
		
		if(strlen($tagtype)>0)
			$where .= ' AND type = '.$tagtype;

		if(!empty($key))
			$where .= ' AND tag_name LIKE \'%'.mysqlLikeQuote($key).'%\'';

		if($cid > 0){
			$cids = D('GoodsCategoryTags')->getTagIDs($cid);
			if(count($cids) > 0)
				$where .= ' AND tag_id NOT IN ('.implode(',',$cids).')';
			if($tagtype=="3"){
				$noSelCids = D('GoodsCategory')->getCateIdsNotInSelCateId($cid);
				$noSelTagIds = D('GoodsCategoryTags')->getTagIDsByCateIDs(implode(',',$noSelCids));
				//$tagids = D('GoodsTags')->getTagIDsByType(implode(',',$noSelTagIds),$tagtype);
				if(count($noSelTagIds) > 0)
					$where .= ' AND tag_id not IN ('.implode(',',$noSelTagIds).')';
			}
		}

		if($sid > 0)
		{
			$sids = D('StyleCategoryTags')->getTagIDs($sid);
			if(count($cids) > 0)
				$where .= ' AND tag_id NOT IN ('.implode(',',$sids).')';
		}

		$tag_names = array();
		if(!empty($tag_name))
		{
			$tag_name = explode('   ',$tag_name);
			foreach($tag_name as $name)
			{
				$tag_names[] = addslashes($name);
			}
		}

		if(!empty($custom_tags))
		{
			$custom_tags = explode(',',$custom_tags);
			foreach($custom_tags as $custom)
			{
				$custom = explode('|',$custom);
				$tag_names[] = addslashes($custom[0]);
			}
		}

		if(count($tag_names) > 0)
			$where .= ' AND tag_name NOT '.createIN($tag_names);

		$list = array();
		if(empty($where))
			$list = D('GoodsTags')->order('sort ASC,tag_id ASC')->findAll();
		else
			$list = D('GoodsTags')->where('1'.$where)->order('sort ASC,tag_id ASC')->findAll();

		if($type == 1)
			echo json_encode($list);
		else
		{
			$this->assign("tag_list",$list);
			echo $this->fetch('GoodsTags:tags');
		}
	}
	
	public function toggleType(){
		$id = intval($_REQUEST['id']);
		if($id == 0)
			exit;
		$result = array('isErr'=>0,'content'=>'');
		$field = "type";
		$type = intval($_REQUEST['type']);
		$sql = "update ".C("DB_PREFIX")."goods_tags set ".$field."= ".$type." where tag_id=".$id;
		$list = M()->execute($sql);
		if(false !== $list){
			$this->saveLog(1,$id,$field);
			$result['content'] = $type;
		}else{
			$this->saveLog(0,$id,$field);
			$result['isErr'] = 1;
		}
		
		die(json_encode($result));
	}
}


function getTagType($type){
	switch($type){
		case 0:
			return "普通标签";
		case 1:
			return "<b><font color='purple'>风格</font></b>";
		case 2:
			return "<b><font color='red'>元素</font></b>";
		case 3:
			return "<b><font color='green'>款型</font></b>";
		case 4:
			return "<b><font color='blue'>品牌</font></b>";
		case 5:
			return "<b><font color='maroon'>材质</font></b>";
		case 6:
			return "<b><font color='brown'>品类</font></b>";			
	}
}
?>