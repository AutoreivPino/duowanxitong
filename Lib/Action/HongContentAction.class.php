<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class HongContentAction extends CommonAction
{
	public function index()
	{
		$parameter = array();
		
		$model = D('HongContent');

		$sql = 'SELECT COUNT(id) AS tcount FROM '.C("DB_PREFIX").'hong_content ';

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'hong_content ';
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	
	public function insert()
	{		
		$model = D("HongContent");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		
		$list=$model->add($data);
		if ($list !== false)
		{	
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));

		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		$list=$model->save($data);
		if (false !== $list)
		{
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}

	}

	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('avatar_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}

function getUserAvatar($uid)
{
	$avatar_path = D('User')->getUserAvatarPath($uid);
	$avatar_url = $avatar_path['url'].'_middle.jpg';
	$avatar_path = $avatar_path['path'].'_middle.jpg';
	if(!file_exists($avatar_path))
		$avatar_url = __ROOT__.'/public/upload/avatar/noavatar_middle.jpg';
	return $avatar_url;
}
function showtype($id){
	if($id==0){
		return '未中奖';
	}elseif($id==1){
		return '一等';
	}elseif($id==2){
		return '二等';
	}elseif($id==3){
		return '三等';
	}
}
function fllownum1($uid){
	$ulist = D('Hong')->where('status=1 and invite_id='.$uid)->select();
	$ucount = 0;
	if(count($ulist)>0){
		foreach($ulist as $k=>$v){
			$list = D("UserPhone")->where('use_type=1 and hid='.$v['uid'])->count();
			$ucount += $list;
		}
	}
	
	
	return $ucount;
}
?>