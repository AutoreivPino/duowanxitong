<?php
/**
 +------------------------------------------------------------------------------
 * 会员
 +------------------------------------------------------------------------------
 */
class HongAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$user_name = trim($_REQUEST['user_name']);
		$email = trim($_REQUEST['email']);

		if(!empty($user_name))
		{
			$this->assign("user_name",$user_name);
			$parameter['user_name'] = $user_name;
			//$match_key = segmentToUnicodeA($user_name,'+');
			//$where.=" AND match(user_name_match) against('".$match_key."' IN BOOLEAN MODE) ";
            $like_name = mysqlLikeQuote($user_name);
            $where .= ' AND user_name LIKE \'%'.$like_name.'%\'';
		}

		if(!empty($email))
		{
			$this->assign("email",$email);
			$parameter['email'] = $email;
			$where .= " AND email = '$email'";
		}

		$model = D('Hong');

		if(!empty($where))
			$where = 'WHERE 1' . $where;

		$sql = 'SELECT COUNT(uid) AS tcount FROM '.C("DB_PREFIX").'hong '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'hong '.$where;
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	public function add(){
		$model = D("Hong");
		$list = $model->where('status=1')->select();
		$this->assign('list',$list);
		parent::add();
	}
	
	public function insert()
	{		
		$model = D("Hong");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['reg_time'] = time();
		$password = $_REQUEST['password'];

		$data['password'] = md5($password);
		$list=$model->add($data);
		if ($list !== false)
		{	
			$url = 'http://www.trendszaijia.cn/wx/fl.php?hid='.$list;
			D("Hong")->where('uid = '.$list)->setField('url',$url);
			
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'avatar_img')
					{
						D("Hong")->where('uid = '.$list)->setField('avatar_img',$img);
					}
					elseif($upload_item['key'] == 'file_url')
					{
						D("Hong")->where('uid = '.$list)->setField('file_url',$img);
					}
				}
			}
			
			$this->assign('url',$url);
			$this->assign('uid',$list);
			$this->display('Hong/addinfo');

			//$this->saveLog(1,$list);
			//$this->success (L('ADD_SUCCESS'));

		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	
	public function insertinfo()
	{		
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		$list=$model->save($data);
		if ($list !== false)
		{	
			//$this->saveLog(1,$list);
			//$this->success (L('ADD_SUCCESS'));
			$this->display('Hong/add');
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	public function edit()
	{
		$uid = intval($_REQUEST['uid']);
		$model = D("Hong");
		$list = $model->where('status=1')->select();
		$this->assign('list',$list);
		$vo = M()->query('SELECT * FROM '.C("DB_PREFIX").'hong WHERE uid= '.$uid);
		if(count($vo) > 0)
			$vo = $vo[0];

		$this->assign ('vo', $vo );
		
		$this->display();
	}

	public function update()
	{
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		$old_user = $model->getById($uid);
		
		$old_user_name = $old_user['user_name'];
		$old_email = $old_user['email'];
		
		$new_user_name = $_REQUEST['user_name'];
		$new_email = $_REQUEST['email'];
				
		if ($old_email == $new_email){
			$new_email = '';
		}

		if ($old_user_name == $new_user_name){
			$new_user_name = '';
		}
					
		
		if ($_REQUEST['password'] == ''){
			$new_pwd = '';
		}else{
			$new_pwd = $_REQUEST['password'];
		}

		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		if($_REQUEST['password'] == ''){
			unset($data['password']);
		}else{
			$password = $_REQUEST['password'];
			$data['password'] = md5($password);
		}
		
		$list=$model->save($data);
		if (false !== $list)
		{
			
			$avatar_img= '';
			if($upload_list = $this->uploadImages()){
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					
					if($upload_item['key'] == 'avatar_img')
					{
						D("Hong")->where('uid = '.$uid)->setField('avatar_img',$img);
					}elseif($upload_item['key'] == 'file_url')
					{
						D("Hong")->where('uid = '.$uid)->setField('file_url',$img);
					}
				}
			}
			
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}

	}

	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('avatar_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['avatar_img']))
						@unlink(RES_ROOT.$data['avatar_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}

function getUserAvatar($uid)
{
	$avatar_path = D('User')->getUserAvatarPath($uid);
	$avatar_url = $avatar_path['url'].'_middle.jpg';
	$avatar_path = $avatar_path['path'].'_middle.jpg';
	if(!file_exists($avatar_path))
		$avatar_url = __ROOT__.'/public/upload/avatar/noavatar_middle.jpg';
	return $avatar_url;
}
function fllownum($uid){
	$list = D("UserPhone")->where(' use_type=1 and hid='.$uid)->count();
	return $list;
}
function fllownum1($uid){
	$ulist = D('Hong')->where('status=1 and invite_id='.$uid)->select();
	$ucount = 0;
	if(count($ulist)>0){
		foreach($ulist as $k=>$v){
			$list = D("UserPhone")->where('use_type=1 and hid='.$v['uid'])->count();
			$ucount += $list;
		}
	}
	
	
	return $ucount;
}
?>