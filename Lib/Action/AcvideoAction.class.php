<?php
/**
 +------------------------------------------------------------------------------
 专辑
 +------------------------------------------------------------------------------
 */
class AcvideoAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$keyword = trim($_REQUEST['keyword']);
		
		if(!empty($keyword))
		{
			$this->assign("keyword",$keyword);
			$parameter['keyword'] = $keyword;
			$where.=" AND a.title LIKE '%".mysqlLikeQuote($keyword)."%' ";
		}

		$model = M();

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(DISTINCT a.id) AS tcount 
			FROM '.C("DB_PREFIX").'acvideo AS a 
			LEFT JOIN '.C("DB_PREFIX").'admin AS u ON u.id = a.uid 
			'.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT a.*,u.admin_name   
			FROM '.C("DB_PREFIX").'acvideo AS a 
			LEFT JOIN '.C("DB_PREFIX").'admin AS u ON u.id = a.uid '.$where.' GROUP BY a.id';
		
		$this->_sqlList($model,$sql,$count,$parameter,'a.id');
		
		$this->display ();
		return;
	}
	
	public function downPic(){
		$id = intval($_REQUEST['id']);
		$model = D ('Acvideo');
		$data = $model->getById($id);
		$filename = RES_ROOT.$data['v_url'];
		
		$fileinfo = pathinfo($filename);
		header('Content-type: application/x-'.$fileinfo['extension']);
		header('Content-Disposition: attachment; filename='.$fileinfo['basename']);
		header('Content-Length: '.filesize($filename));
		readfile($filename);
		exit();
	}
	
	public function update()
	{
        $id = intval($_REQUEST['id']);
		$model = D ('Acvideo');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				$cate = $model->getById($id);
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];

					if(!empty($cate['v_url']))
						@unlink(RES_ROOT.$cate['v_url']);
					
					$model->where("id=".$id)->setField("v_url",$img);
				}
			}

			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('Acvideo');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		$data['create_time'] = time();
		$data['uid'] = $_SESSION['strends_shop_share'];
		// 更新数据
		$list=$model->data($data)->add();
		
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					D("Acvideo")->where('id = '.$list)->setField('v_url',$img);
				}
			}

			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));

		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
                $album_data = D('Acvideo')->where('id='.$aid)->find();
				if(!empty($album_data['v_url']))
					@unlink(RES_ROOT.$album_data['v_url']);
				D('Acvideo')->where('id='.$aid)->delete();
                
            }

			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}
	public function uploadpic(){
		$albumid = $_GET['albumid'];
		import("@.ORG.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = 3292200;
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg');
		$id_path = getDirsById($albumid);
		$dir = RES_ROOT.'public/upload/share/'.$id_path.'/';
		if(!file_exists($dir)){
			createFolder($dir);
		}
		
		$upload->savePath = RES_ROOT.'public/upload/share/'.$id_path.'/';
		 
		$upload->imageClassPath = '@.ORG.Image';
		 
		 //设置上传文件规则
		$upload->saveRule = 'uniqid';
		
		 if (!$upload->upload()) {
			//捕获上传异常
			$this->error($upload->getErrorMsg());
		 } else {
			$uploadList = $upload->getUploadFileInfo();
			//import("@.ORG.Image");
			//Image::water($uploadList[0]['savepath'].$uploadList[0]['savename'], APP_PATH.'Tpl/Public/Images/logo.png');
			$data = array();
			$data['index_img'] = 'public/upload/share/'.$id_path.'/'.$uploadList[0]['savename'];
			$data['rec_id'] = $albumid;
			$data['sort'] = 100;
			$data['uid'] = $_SESSION['strends_shop_share'];
			$data['create_time'] = time();
			$model = D ('Acshare');
			$list=$model->data($data)->add();
			$album_share = array();
			$album_share['ac_id'] = $albumid;
			$album_share['share_id'] = $list;
			D('ActiveAcshare')->data($album_share)->add();
			D("Active")->query("update tbl_active set img_count = img_count+1 where id=".$albumid);
			$result=array('shareid'=>$list,'albumid'=>$albumid,'shareimg'=>'public/upload/share/'.$id_path.'/'.$uploadList[0]['savename']);
			die(json_encode($result));
		 }
	}
	public function textedit(){
		$aid = $_POST['aidArray'];
		$contentArray = $_POST['contentArray'];
		$pnameArray = $_POST['pnameArray'];
		$sortArray = $_POST['sortArray'];
		$model = D ('Acshare');
		$data = array();
		foreach($aid as $k=>$v){
			$data['share_id'] = $v;
			$data['title'] = $pnameArray[$k];
			$data['content'] = $contentArray[$k];
			$data['sort'] = $sortArray[$k];
			$data['create_time'] = time();
			// 更新数据
			$model->save($data);
		}
		$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
		$this->success (L('EDIT_SUCCESS'));
	}
	public function makeImg(){
		$img = $_GET['loc'];
		$shareid = $_GET['shareid'];
		$albumid = $_GET['albumid'];
		$dir=dirname($img);
		$text_arr=D('share')->where('share_id='.$shareid)->find();
		$texts=explode("|^|",$text_arr['edit_text']);
		$resdomain = PIC_ROOT;
		
		$this->assign('dir',$dir);
		$this->assign('img',PIC_ROOT.$img);
		$this->assign('shareid',$shareid);
		$this->assign('albumid',$albumid);
		$this->assign('texts',$texts);
		$this->assign('resdomain',$resdomain);
		$this->display();
	}
	public function createimg(){
		if($_GET['issave']){
			$strsave = file_get_contents('php://input');
			$saveArr = explode('|',ltrim($strsave,'|^|'));
			$width = (int)$saveArr[1];
			$height = (int)$saveArr[2];
			$shareid = (int)$saveArr[4];
			$albumid = (int)$saveArr[3];
			$newPic = $saveArr[0];
			
			$data = array();
			$data['id'] = $albumid;
			$data['is_best'] = 1;
			$data['best_img'] = $newPic;
			$result = D('Album')->save($data);
			echo $result;
			exit;
		}
		mb_internal_encoding("UTF-8");
		$str = file_get_contents('php://input');
		if( $str=='' ) $str =  "w=remingbisjfdksd&p=/tpl/strends/tmp/1.jpg&p_s=&x=0&y=0&size=12&color=#000000&family=方正粗倩简体&fontLeading=0&linespacing=0";
		
		parse_str($str,$paraArr);
		$imgpath = substr($paraArr['imgpath'],strpos($paraArr['imgpath'],'public/upload/'));
		
		$txt = str_replace('+','%2B',$paraArr['text_input']); 	//解析时把成了%2B转换+,解码前应将+转换为%2B
		$txt = urldecode($txt);
		$font_size = $paraArr['font_size'];
		$font_color = $paraArr['font_color'];
		$space = $paraArr['spacing'];
		$linespacing = $paraArr['linespacing'];
		$txt_x = $paraArr['font_x'];
		$txt_y = $paraArr['font_y'];
		$font = $paraArr['selectFont'];
		
		$area_x = intval($paraArr['area_x']);
		$area_y = intval($paraArr['area_y']);
		$area_w = intval($paraArr['area_w']);
		$area_h = intval($paraArr['area_h']);
		$shareid= intval($paraArr['shareid']);
		$albumid= intval($paraArr['albumid']);
		//if($linespacing<2) $linespacing = 1;
		$txt_arr = explode('|^|',$txt);
		$font_size_arr = explode('|^|',$font_size);
		$font_color_arr = explode('|^|',$font_color);
		$spacing_arr = explode('|^|',$space);
		$linespacing_arr = explode('|^|',$linespacing);
		$txt_x_arr = explode('|^|',$txt_x);
		$txt_y_arr = explode('|^|',$txt_y);
		$font_arr = explode('|^|',$font);
		
				
		//要裁剪图像的宽高
		$width=$area_w;
		$height=$area_h;
		if(!$width || !$height){
			echo 1;
			exit;
		}

		//要裁剪图像的顶点位置
		$img_x=$area_x;
		$img_y=$area_y;

		//获取原图像的尺寸
		list($src_width, $src_height,$type) = getimagesize(RES_ROOT.$imgpath);
		
		if($width!=612){
			if($src_width<$width || $src_height<$height){
				echo 2;
				exit;
			}
		}
		
		//更新数据库中图片加上的文字
		$share_data = array();
		$share_data['share_id'] = $shareid;
		$share_data['edit_text'] = $txt;
		$result = D("Share")->save($share_data);
		//判断图片类型是否为jpg类型
		if($type != 2){ 
			echo 3;
			exit;
		}
		$src_radio = $src_width/$src_height;
		$radio = $width/$height;
		if($radio >= $src_radio){
			$newsrc_width = $width;
			$newsrc_height = $newsrc_width/$src_radio;
		}else{
			$newsrc_height = $height;
			$newsrc_width = $newsrc_height*$src_radio;
		}
		//裁剪从客户端获取到的尺寸的图片
		$im = @imagecreatefromjpeg(RES_ROOT.$imgpath);
		$sizeim=imagecreatetruecolor($newsrc_width,$newsrc_height);
		$tumb=imagecopyresampled ( $sizeim, $im, 0, 0, 0, 0, $newsrc_width, $newsrc_height, $src_width , $src_height);

		if(!$tumb){
			echo 4;
			exit;
		}

		$newim=imagecreatetruecolor($width,$height);
		$copy=imagecopy ( $newim, $sizeim, 0, 0, $img_x, $img_y, $width, $height );

		if(!$copy){
			echo 5;
			exit;
		}
	   
		function getfontPath($font){
			$docloc = RES_ROOT."tpl/strends/font/";
			if( $font=='宋体' ) {
				$font = $docloc."simsun.ttc";
			}else if( $font=='Microsoft JhengHei' ){
				$font = $docloc."zmsjh.ttf";
			}else if( $font=='Microsoft JhengHei Bold' ){
				$font = $docloc."zmsjhbd.ttf";
			}else if( $font=='黑体' ){
				$font = $docloc."simhei.ttf";
			}else if( $font=='微软雅黑' ){
				$font = $docloc."msyh.ttf";
			}else if( $font=='微软雅黑 Bold' ){
				$font = $docloc."msyhbd.ttf";
			}else if( $font=='幼圆' ){
				$font = $docloc."SIMYOU.TTF";
			}else if( $font=='方正粗宋简' ){
				$font = $docloc."fzcsjt.TTF";
			}else if( $font=='时尚中黑简体' ){
				$font = $docloc."sszhjt.ttf";
			}else if( $font=='方正细等线_GBK' ){
				$font = $docloc."simfzxd.ttf";
			}else if( $font=='方正报宋简体' ){
				$font = $docloc."fzbsjt.TTF";
			}else if( $font=='方正粗倩简体' ){
				$font = $docloc."fzcqjt.TTF";
			}else if( $font=='汉仪中等线简' ){
				$font = $docloc."simhanyi.TTF";
			}else if( $font=='Candara Bold' ){
				$font = $docloc."Candarab.ttf";
			}else if( $font=='Candara Italic' ){
				$font = $docloc."Candarai.ttf";
			}else if( $font=='Candara Bold Italic' ){
				$font = $docloc."Candaraz.ttf";
			}else if( $font=='Candara' ){
				$font = $docloc."Candara.ttf";
			}else if( $font=='Century Gothic Bold Italic' ){
				$font = $docloc."century gothic BOLD.ttf";
			}else if( $font=='Freestyle Script' ){
				$font = $docloc."FREESCPT.TTF";
			}else if( $font=='Century Gothic' ){
				$font = $docloc."GOTHIC_0.TTF";
			}else if( $font=='Century Gothic Bold' ){
				$font = $docloc."GOTHICB_0.TTF";
			}else if( $font=='century Gothic Italic' ){
				$font = $docloc."GOTHICI_0.TTF";
			}else if( $font=='Blackadder ITC' ){
				$font = $docloc."ITCBLKAD.TTF";
			}else if( $font=='Kunstler Script' ){
				$font = $docloc."KUNSTLER.TTF";
			}else if( $font=='Microsoft Sans Serif' ){
				$font = $docloc."micross.ttf";
			}else if( $font=='Stencil' ){
				$font = $docloc."STENCIL.TTF";
			}else if( $font=='隶书'){
				$font = $docloc."SIMLI.TTF";
			}else if( $font=='Microsoft JhengHei Bold'){
				$font = $docloc."msjhbd.ttf";
			}else if( $font=='Microsoft JhengHei'){
				$font = $docloc."msjh.ttf";
			}else{
				$font = $docloc."zmsjhbd.ttf";
			}
			return $font;
		}

		function Hex2RGB( $hexColor ){ //十六进制转GRB
			$color = str_replace('#','' , $hexColor);
			if (strlen($color) > 3) {
				$rgb = array(
					'r' => hexdec(substr($color, 0, 2)),
					'g' => hexdec(substr($color, 2, 2)),
					'b' => hexdec(substr($color, 4, 2))
				);
			} else {
				$color = str_replace('#','' , $hexColor);
				$r = substr($color, 0, 1) . substr($color, 0, 1);
				$g = substr($color, 1, 1) . substr($color, 1, 1);
				$b = substr($color, 2, 1) . substr($color, 2, 1);
				$rgb = array(
					'r' => hexdec($r),
					'g' => hexdec($g),
					'b' => hexdec($b)
				);
			}
			return $rgb;
		}

		function fontPX($font_size, $angle, $font, $txt){ //得到字符串高宽度
			$testbox = imagettfbbox($font_size, $angle, $font, $txt);
			$s['width'] = abs($testbox[2]-$testbox[0]);
			$s['height'] = abs($testbox[1]-$testbox[7]);
			return $s;
		}
		
		function isIncludeChinse($str){	//判断字母或数字的UTF-8编码的长度和其ASCII编码是否一致
			if(strlen(mb_convert_encoding($str,"utf-8"))==strlen(mb_convert_encoding($str,""))){
				return false;
			}else{ 
				return true;
			}
		}

	  for($t=0;$t<4;$t++){	//循环四组文字
		  if($txt_arr[$t]!=''){
			  $colorArr = Hex2RGB($font_color_arr[$t]);	//颜色十六进制转RGB
			  $color = imagecolorallocate($newim, $colorArr['r'], $colorArr['g'], $colorArr['b']);

			  $lineArr = explode("\n",$txt_arr[$t]);
			  for($j=0;$j<count($lineArr);$j++){	//循环每行文字
				  $px_h = fontPX($font_size_arr[$t], 0, getfontPath($font_arr[$t]), $lineArr[$j]);	//一行字的高度
				  if($px_h['height']==0) $px_h['height']=$font_size_arr[$t];
				  if($j==0){//第一行文字
					  if($linespacing_arr[$t]==15 && $font_size_arr[$t]==12 ){
						  $line_y = ceil($txt_y_arr[$t]+$font_size_arr[$t]);//第一行的y
					  }else{
						  $line_y = ceil($txt_y_arr[$t]+$linespacing_arr[$t]/1.15);//第一行的y
					  }
				  }else{
					  $line_y += ceil($px_h['height']+$linespacing_arr[$t]-$font_size_arr[$t]*1.1);	//第二行 第三行 .... 的y
				  }

				  if($spacing_arr[$t]=='0'){	//没有设置行间距
					  imagettftext($newim, $font_size_arr[$t], 0, $txt_x_arr[$t], $line_y, $color, getfontPath($font_arr[$t]), $lineArr[$j]);
				  }else{
					  $c = mb_strlen($lineArr[$j], 'utf-8');	//一行有几个字
					  $font_x_b = 0;
					  $charArr=array(//(向前移或向后移,字后留的距离,向上或向下移)
						  '自'=>array(0,0.25,0),
						  '己'=>array(0,0.08,0),
						  '·'=>array(-0.1,0.08,0),
						  '“'=>array(0,0.6,0),
						  '^'=>array(-0.25,0,0),
						  '”'=>array(0.1,0.6,0),
						  '的'=>array(0,0.2,0),
						  'A'=>array(0,-0.3,0),
						  'B'=>array(0,0.01,0),
						  'Z'=>array(0.2,-0.2,0),
						  'z'=>array(0.02,-0.2,0),
						  'J'=>array(0.02,-0.2,0),
						  'K'=>array(0.05,-0.1,0),
						  ')'=>array(0.02,0,0),
						  'I'=>array(0.02,-0.02,0),
						  'Y'=>array(-0.02,-0.02,0),
					  );
					  for($i=0;$i<$c;$i++){	//循环每个文字
						  $current = mb_substr($lineArr[$j], $i, 1, 'utf-8');
						  if($i==0){//每行第一个字
							  $font_x = $txt_x_arr[$t];	//间距
						  }else{
							  $prev = mb_substr($lineArr[$j], $i-1, 1, 'utf-8'); 	//前一个字
							  $prev_w = fontPX($font_size_arr[$t], 0, getfontPath($font_arr[$t]), $prev);		//前一个字的宽度
							  if($prev_w['width']==0) $prev_w['width']=$font_size_arr[$t];
							  $font_x += $prev_w['width']+$spacing_arr[$t];
						  }
						  if(isset($charArr[$current])){
							  $font_x += ceil($font_size_arr[$t]*$charArr[$current][0]);	//单个字向前或向后
							  $font_x_b = ceil($font_size_arr[$t]*$charArr[$current][1]);	//单个字后留多少像素
							  $line_y += ceil($font_size_arr[$t]*$charArr[$current][2]);	//单个字上下偏移
						  }else{
							  $font_x_b = 0;
						  }
						  imagettftext($newim, $font_size_arr[$t], 0, $font_x, $line_y, $color, getfontPath($font_arr[$t]), $current);
						  $font_x += $font_x_b;
					  }
				  }
			  }
		  }
	  }
		
		$newPic = str_replace('.jpg','_thumb_'.$width.'x'.$height.'.jpg',$imgpath);
		if($newPic == $imgpath){
			exit('生成新裁剪图片路径错误');
		}
		
		imagejpeg($newim,RES_ROOT.$newPic,100);
		echo '|^|'.$newPic.'|'.$width.'|'.$height.'|'.$albumid.'|'.$shareid;
		
		//createFuzzyGraph(RES_ROOT.$newPic, 1, 5);    //生成模糊图
		//imagedestroy($im);
		imagedestroy($newim);
		imagedestroy($sizeim);	
	}
	//采集图片
	public function caitu(){
		$imgs = $_GET['imgUrl'];
		$pageUrl = $_GET['pageUrl'];
		//$img_list = explode(",",$imgs);
		//$count_img = count($img_list);
		$tag_cate=D('GoodsCategory')->where('parent_id=0')->order('sort')->field('cate_id,cate_name')->findAll();
		$tag_list = array();
		foreach($tag_cate as $k=>$v){
			$tag_list[$v['cate_id']] = D('GoodsCategory')->where('parent_id='.$v['cate_id'])->order('sort')->field('cate_id,cate_name')->findAll();
		}
		$category_list = D("AlbumCategory")->order('sort asc,id asc')->findAll();
		$this->assign("category_list",$category_list);
		$this->assign("tag_list",$tag_list);
		$this->assign('imgs',$imgs);
		$this->assign('pageurl',$pageUrl);
		
		$this->display ();		
	}
	public function caitu_insert(){
		$model = D ('Album');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$imgs = $_POST['imgs'];
		$purl = $_POST['purl'];
		$img_list = explode(",",$imgs);
		$data['img_count'] = count($img_list);
		$tag_list = $_POST['tag_list'];
		$data['create_time'] = time();
		$data['uid'] = $_SESSION['strends_shop_share'];
		// 更新数据
		$albumid=$model->data($data)->add();
		$tag_list = $_POST['tag_list'];
		foreach($tag_list as $v){
			$tag_data['tag_id'] = $v;
			$tag_data['album_id'] = $albumid;
			D('album_goods')->add($tag_data);
		}
		$share_list = array();
		$i=0;
		foreach($img_list as $v){
			$img= $v;
			$imgname = substr( $img , strrpos($img , '/')+1 );
			$id_path = getDirsById($albumid);
			$dir = RES_ROOT.'public/upload/share/'.$id_path.'/';
			if(!file_exists($dir)){
				createFolder($dir);
			}
			$filename = $dir.$imgname;
			copy($img,$filename);
			
			$data = array();
			$data['index_img'] = 'public/upload/share/'.$id_path.'/'.$imgname;
			$data['rec_id'] = $albumid;
			$data['create_time'] = time();
			$model = D ('Share');
			$shareid=$model->data($data)->add();
			$album_share = array();
			$album_share['album_id'] = $albumid;
			$album_share['share_id'] = $shareid;
			D('AlbumShare')->data($album_share)->add();
			$share_list[$i]['share_id'] = $shareid;
			$share_list[$i]['index_img'] = 'public/upload/share/'.$id_path.'/'.$imgname;
			$share_list[$i]['title'] = '';
			$share_list[$i]['content'] = '';
			$i++;
		}
		
		if (false !== $albumid)
		{
			$this->assign('albumid',$albumid);
			$this->assign('share_list',$share_list);
			$this->display('Album/uploadpic');
		}
	}
}

/**
 * 根据ID划分目录
 * @return string
 */
function getDirsById($id)
{
	$id = sprintf("%011d", $id);
	$dir1 = substr($id, 0, 3);
	$dir2 = substr($id, 3, 3);
	$dir3 = substr($id, 6, 3);
	$dir4 = substr($id, -2);
	return $dir1.'/'.$dir2.'/'.$dir3.'/'.$dir4;
}
function createFolder($path){
    if(!file_exists($path)){
        createFolder(dirname($path));
        mkdir($path);
    }
}
?>