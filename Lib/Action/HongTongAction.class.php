<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class HongTongAction extends CommonAction
{
	public function index()
	{
		$parameter = array();

		$model = D('HongTong');

		$sql = 'SELECT COUNT(id) AS tcount FROM '.C("DB_PREFIX").'hong_tong';

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'hong_tong';
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	public function add(){
		$list = D("Hong")->where('status=1')->select();
		$this->assign('list',$list);
		parent::add();
	}
	public function insert(){
		$model = D("HongTong");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		$data['type'] = $_REQUEST['type'];
		$ulist = $_REQUEST['ulist'];
		
		$list=$model->add($data);
		if ($list !== false)
		{
			if($data['type']==0){
				$time = time();
				foreach($ulist as $k=>$v){
					D("TongUser")->query("insert into tbl_tong_user (tong_id,uid_id,create_time) values(".$list.",".$v.",".$time.")");
				}
			}
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	public function edit(){
		$id = $_REQUEST['id'];
		$sql = 'SELECT uid_id FROM '.C("DB_PREFIX").'tong_user where tong_id='.$id;
		
		$tuser = D("TongUser")->query($sql);
		$u_arr = array();
		foreach($tuser as $k=>$v){
			$u_arr[] = $v['uid_id'];
		}
		
		$list = D("Hong")->where('status=1')->select();
		$this->assign('list',$list);
		$this->assign('u_arr',$u_arr);
		parent::edit();	
	}
	public function update(){
		$id = intval($_REQUEST['id']);		
		$name=$this->getActionName();
		$model = D ( $name );	

		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['type'] = $_REQUEST['type'];
		$ulist = $_REQUEST['ulist'];
		
		$list=$model->save($data);
		if (false !== $list)
		{
			if($data['type']==0){
				D("TongUser")->query("delete from tbl_tong_user where tong_id=".$id);
				$time = time();
				foreach($ulist as $k=>$v){
					D("TongUser")->query("insert into tbl_tong_user (tong_id,uid_id,create_time) values(".$id.",".$v.",".$time.")");
				}
			}
			
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			if(false !== $model->where ( $condition )->delete ())
			{
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}

?>