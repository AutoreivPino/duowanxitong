<?php

/**
 * 活动海报
 */
class ActivityPostAction extends CommonAction
{
    public function view($activity_id)
    {
        $model = D('ActivityPost');
        $model->find($activity_id);
        $postPath = $model->getPost();

        $this->assign('postPath', $postPath);
        $this->display();
    }
}