<?php

/**
 * 活动红包
 */
class ActivityHbAction extends CommonAction
{
    public function add()
    {
        $list = D('Activity')->findAll();
        $this->assign('list', $list);
        $this->display();
    }

    public function edit()
    {
        $name = $this->getActionName();
        $model = D($name);

        $id = $_REQUEST [$model->getPk()];
        $vo = $model->getById($id);

        $list = D('Activity')->findAll();
        $this->assign('list', $list);
        $this->assign('vo', $vo);
        $this->display();
    }
	
	
	public function update()
	{
		$id = intval($_REQUEST['activity_id']);
		
		$model = D("ActivityHb");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		echo "<pre>";
		var_dump($data);
		exit;
		$list=$model->save($data);
		if (false !== $list)
		{
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}

}