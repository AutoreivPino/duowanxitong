<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class JfUserAddressAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$phone = trim($_REQUEST['phone']);
		
		if(!empty($phone))
		{
			$where .= " ua.phone LIKE '%".mysqlLikeQuote($phone)."%'";
			$this->assign("phone",$phone);
			$parameter['phone'] = $phone;
		}

		$model = D('JfUg');
		
		if(!empty($where))
			$where = 'WHERE 1 and ' . $where;

		$sql = 'SELECT COUNT(*) AS tcount FROM '.C("DB_PREFIX").'jf_ug ug LEFT JOIN '.C("DB_PREFIX").'jf_user_address AS ua ON ug.id = ua.ugid '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'jf_ug ug LEFT JOIN '.C("DB_PREFIX").'jf_user_address AS ua ON ug.id = ua.ugid '.$where;
		
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	public function sendmsg()
	{
		$where = '';
		$parameter = array();

		$model = D('JfMsg');

		if(!empty($where))
			$where = 'WHERE 1 and ' . $where;

		$sql = 'SELECT COUNT(*) AS tcount FROM '.C("DB_PREFIX").'jf_msg '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'jf_msg '.$where;
		
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	public function addmsg()
	{
		$this->display();
	}
	public function insertmsg()
	{
		$phone = $_REQUEST['phone'];
		$content = $_REQUEST['content'];
		if(strlen($phone)<11){
			echo '号码不正确';
			exit;
		}
		if(empty($content)){
			echo '内容不能为空';
			exit;
		}
		
		$userid = 66;
		$account = 'AE00130';
		$password = md5('AE00130686');
		
		$url = "https://dx.ipyy.net/smsJson.aspx?action=send&userid=".$userid."&account=".$account."&password=".$password."&mobile=".$phone."&content=".$content."&sendTime=&extno=";
		
		$response = get_response_get($url);  
		
		$getcode = json_decode($response, true);  
		
		//$getcode = json_decode(file_get_contents($url));
		$msg = $getcode->returnstatus;
		
		$time = time();	
		//if($msg=='Success'){								
		D('JfMsg')->query("insert into tbl_jf_msg (phone,content,create_time,status) values('".$phone."','".$content."',".$time.",1)");
			
		//}else{
		//	D('JfMsg')->query("insert into tbl_jf_msg (phone,content,create_time,status) values('".$phone."','".$content."',".$time.",0)");
		//}
		$this->redirect('JfUserAddress/addmsg');
	}
	public function insertinfo()
	{		
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		$list=$model->save($data);
		if ($list !== false)
		{	
			//$this->saveLog(1,$list);
			//$this->success (L('ADD_SUCCESS'));
			$this->display('Hong/add');
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}

	}
	public function edit()
	{
		$id = intval($_REQUEST['id']);
		$d = D('ServerWxflUg')->where('id='.$id)->find();
		$uid = $d['uid'];
		$gid = $d['gid'];
		$vo = D('ZjUser')->where('id='.$uid)->find();
		$g = D('ServerWxgood')->where('id='.$gid)->find();
		$list = D('UserPhoneAddress')->where('uid='.$uid.' and gid='.$id)->find();
		$p_c = $list['p_c'];
		
		$pc_arr = explode('_',$p_c);
		$p = $pc_arr[0];
		$c = $pc_arr[1];
		
		$pname = D('Region')->where('id='.$p)->getField('name');
		$cname = D('Region')->where('id='.$c)->getField('name');
		
		$this->assign ('list', $list );
		$this->assign ('vo', $vo );
		$this->assign ('g', $g );
		$this->assign ('pname', $pname );
		$this->assign ('cname', $cname );
		
		$this->display();
	}

	public function update()
	{
		$uid = intval($_REQUEST['uid']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		$old_user = $model->getById($uid);
		
		$old_user_name = $old_user['user_name'];
		$old_email = $old_user['email'];
		
		$new_user_name = $_REQUEST['user_name'];
		$new_email = $_REQUEST['email'];
				
		if ($old_email == $new_email){
			$new_email = '';
		}

		if ($old_user_name == $new_user_name){
			$new_user_name = '';
		}
					
		
		if ($_REQUEST['password'] == ''){
			$new_pwd = '';
		}else{
			$new_pwd = $_REQUEST['password'];
		}

		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		if($_REQUEST['password'] == ''){
			unset($data['password']);
		}else{
			$password = $_REQUEST['password'];
			$data['password'] = md5($password);
		}
		
		$list=$model->save($data);
		if (false !== $list)
		{
			
			$avatar_img= '';
			if($upload_list = $this->uploadImages()){
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					
					if($upload_item['key'] == 'avatar_img')
					{
						D("Hong")->where('uid = '.$uid)->setField('avatar_img',$img);
					}elseif($upload_item['key'] == 'file_url')
					{
						D("Hong")->where('uid = '.$uid)->setField('file_url',$img);
					}
				}
			}
			
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}

	}

	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('avatar_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['avatar_img']))
						@unlink(RES_ROOT.$data['avatar_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}
	public function changestatus(){
		$id = intval($_REQUEST['id']);
		D('JfUserAddress')->query('update tbl_jf_user_address set status=1 where ugid='.$id);
	}

}

  
function get_response_get($url)  
{  
	 $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 1);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //执行命令
    $data = curl_exec($curl);
    //关闭URL请求
    curl_close($curl);
    //显示获得的数据
   
    return $data;
} 

function fllownum($uid){
	$list = D("JfUser")->where('id='.$uid)->find();
	return $list['username'];
}
function fllownum1($gid){
	$list = D("JfGoods")->where('id='.$gid)->find();
	return $list['title'];
}
function fllownum2($uid){
	$ulist = D('JfUser')->where('id='.$uid)->find();
	return "<img src='".$ulist['pic']."' width='50'>";
}
function dataname($id){
	$ulist = D('JfUserAddress')->where('ugid='.$id)->find();
	return $ulist['uname'];
}
function dataphone($id){
	$ulist = D('JfUserAddress')->where('ugid='.$id)->find();
	return $ulist['phone'];
}
function datastatus($id){
	$ulist = D('JfUserAddress')->where('ugid='.$id)->find();
	$status = $ulist['status'];
	if($status==1){
		return '已发货';
	}else{
		return "<a href='javascript:void(0);' class='chstatus_".$id."' onclick='changestatus(".$id.")'>未发货</a>";
	}
}

function status1($id){
	$ulist = D('JfMsg')->where('id='.$id)->find();
	$status = $ulist['status'];
	if($status==1){
		return '发送成功';
	}else{
		return "发送失败";
	}
}
?>