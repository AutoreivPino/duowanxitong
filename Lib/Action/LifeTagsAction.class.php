<?php

/**
 +------------------------------------------------------------------------------
  热搜词
 +------------------------------------------------------------------------------
 */
class LifeTagsAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$cate_name = trim($_REQUEST['cate_name']);
		
		if(!empty($cate_name))
		{
			$where .= " AND cate_name LIKE '%".mysqlLikeQuote($cate_name)."%'";
			$this->assign("cate_name",$cate_name);
			$parameter['cate_name'] = $cate_name;
		}

		$model = M();
		
		if(!empty($where))
			$where = 'WHERE 1' . $where;
		
		$sql = 'SELECT COUNT(DISTINCT cate_id) AS wcount 
			FROM '.C("DB_PREFIX").'life_tags '.$where;

		$count = $model->query($sql);
		$count = $count[0]['wcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'life_tags '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'cate_id');
		
		$this->display ();
	}
	
	public function add()
	{
		$this->display ();
	}
	
	public function insert()
	{
		$model = D("LifeTags");
		$cate_name = trim($_REQUEST['cate_name']);
		$type = $_REQUEST['type'];
		
		$cateID = $model->where("cate_name='{$cate_name}',type={$type}")->getField('cate_id');
		if($cateID>0){
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success ('标签已存在');
		}
		
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		//保存当前数据对象
		$list=$model->add($data);
		if ($list !== false)
		{
			if($upload_list = $this->uploadImages())
			{
				$cate_icon = $upload_list[0]['recpath'].$upload_list[0]['savename'];
				D("LifeTags")->where('cate_id = '.$list)->setField('cate_icon',$cate_icon);
			}
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));

		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
	}
	
	public function edit()
	{	
		$id = $_REQUEST['cate_id'];
		$list = D('LifeTags')->where("cate_id = {$id}")->find();
		$this->assign('cate_id',$id);
		$this->assign('list',$list);
		$this->display ();
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$data['sort'] = intval($_REQUEST['sort']);
		$data['cate_name'] = trim($_REQUEST['cate_name']);
		$data['desc'] = trim($_REQUEST['desc']);
		$data['type'] = intval($_REQUEST['type']);
		$model = D('LifeTags');
		
		if($upload_list = $this->uploadImages())
		{
			$data['cate_icon'] = $upload_list[0]['recpath'].$upload_list[0]['savename'];
		}
		
		// 更新数据
		$list=D('LifeTags')->where("cate_id = {$id}")->save($data);
		if (false !== $list)
		{
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			$this->error (L('EDIT_ERROR'));
		}
	}
}	
?>