<?php
/**
 +------------------------------------------------------------------------------
 图片审核管理
 +------------------------------------------------------------------------------
 */
class ReviewAction extends CommonAction
{
	public function index(){
		$model = new Model();
		import('ORG.Util.Page');// 导入分页类
		
		$sql = 'select count(photo.share_id) as count from tbl_album album,tbl_album_share ashare,tbl_share share,tbl_share_photo photo where album.id=ashare.album_id and share.share_id=ashare.share_id and ashare.share_id=photo.share_id and album.cid<53 and share.status=1 and album.is_text=0 ';
		$data = $model->query($sql);
		$count      = $data[0]['count'];
		$Page       = new Page($count);// 实例化分页类 传入总记录数
		$show       = $Page->show();// 分页显示输出
		
		$sql = 'select album.id,album.title,album.audit_status,photo.share_id,photo.img from tbl_album album,tbl_album_share ashare,tbl_share share,tbl_share_photo photo where album.id=ashare.album_id and share.share_id=ashare.share_id and ashare.share_id=photo.share_id and album.cid<53 and album.is_text=0 and share.status=1 order by album.online_time desc limit '.$Page->firstRow.','.$Page->listRows;
		$list = $model->query($sql);
		
		$this->assign('list',$list);
		$this->assign('page',$show);
		$this->display('Review/index');
	}
	public function realbum(){
		$id = $_GET['id'];
		$model = new Model();
		$sql = 'update tbl_album set status=1,audit_status=1 where id='.$id;
		$model->query($sql);
		$result = array('status'=>1);
		
		echo json_encode($result);
		exit();
	}
	public function noalbum(){
		$id = $_GET['id'];
		$model = new Model();
		$sql = 'update tbl_album set status=0,audit_status=2 where id='.$id;
		$model->query($sql);
		$result = array('status'=>1);
		
		echo json_encode($result);
		exit();
	}
	public function reshare(){
		$id = $_GET['id'];
		$model = new Model();
		$sql = 'update tbl_share set status=0 where share_id='.$id;
		$model->query($sql);
		$result = array('status'=>1);
		
		echo json_encode($result);
		exit();
	}
	
}	
?>