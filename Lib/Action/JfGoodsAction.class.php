<?php

class JfGoodsAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$keyword = trim($_REQUEST['keyword']);
		
		if(!empty($keyword))
		{
			$this->assign("keyword",$keyword);
			$parameter['keyword'] = $keyword;
			$where.=" AND title LIKE '%".mysqlLikeQuote($keyword)."%' ";
		}
		
		$model = M();

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(DISTINCT id) AS scount 
			FROM '.C("DB_PREFIX").'jf_goods '.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['scount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'jf_goods '.$where;
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	public function add()
	{
		$a_list = D("Activity")->where('status = 1')->field('id,name')->order('id DESC')->findAll();
		$this->assign("a_list",$a_list);
		parent::add();
	}
	public function edit()
	{
		$id = intval($_REQUEST['id']);
		
		$a_list = D("Activity")->where('status = 1')->field('id,name')->order('id DESC')->findAll();
		$this->assign("a_list",$a_list);
		
		parent::edit();
	}

	public function update()
	{
        $id = intval($_REQUEST['id']);
		
		$model = D ('JfGoods');
		$album = $model->getById($id);
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where("id=".$id)->setField("pic",$img);
				}
			}
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('JfGoods');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		$data['create_day'] = strtotime(date('Y-m-d'));
		
		// 更新数据
		$list=$model->data($data)->add();
		
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where("id=".$list)->setField("pic",$img);
				}
			}
			$this->saveLog(1,$id);
			$this->success (L('ADD_SUCCESS'));

		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
                
				D('JfGoods')->where('id='.$aid)->delete();
				
            }

			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}
}

?>