<?php

class VoguealbumAction extends CommonAction {

	public function index() {
		//列表过滤器，生成查询Map对象
		$map = $this->_search ();
		if (method_exists ( $this, '_filter' )) {
			$this->_filter ( $map );
		}
		$name=$this->getActionName();
		$model = D ($name);
		if (! empty ( $model )) {
			$this->_list ( $model, $map );
		}
		$this->display ();
		return;
	}
	
	public function edit() {
		$name = $this->getActionName();
		$model = D($name);
		
		$id = $_REQUEST [$model->getPk ()];
		$vo = $model->getById($id);

        $vo["effective_time"]=date("Y-m-d H:i:s",$vo["effective_time"]);
        $vo["ineffective_time"]=date("Y-m-d H:i:s",$vo["ineffective_time"]);
		
		$vogue=D("Voguegraphic");
		$res=$vogue->where("albumId=".$id)->select();
		foreach($res as $key=>$val){
			if($val['original']){
				$data[]="<img width='100%' height='100%' name=".$val['id']." src='http://s.stylemode.com.cn".$val['original']."' />";
			}
		}
	
		$this->assign ( 'vo', $vo );
		$this->assign ( 'data', $data );
		$this->display ();
	}
	
	
	public function  deleteimg(){
		if(!empty($_POST)){
			$id=$_POST['id'];
		}elseif(!empty($_GET)){
			$id=$_GET['id'];
		}else{
			$id=null;
		}
		
		if(empty($id)){
			echo json_encode(array("msg"=>"idislost"));
		}else{
			$vogue=D("Voguegraphic");
			$res=$vogue->where("id=".$id)->delete();
			if($res){
				echo json_encode(array("msg"=>"succeess"));
			}else{
				echo json_encode(array("msg"=>"false"));
			}
		}
	}
	protected function _list($model, $map, $sortBy = '', $asc = false,$returnUrl = 'returnUrl') {
		//排序字段 默认为主键名
		if (isset ( $_REQUEST ['_order'] )) {
			$order = $_REQUEST ['_order'];
		} else {
			$order = ! empty ( $sortBy ) ? $sortBy : $model->getPk ();
		}
		//排序方式默认按照倒序排列
		//接受 sost参数 0 表示倒序 非0都 表示正序
		if (isset ( $_REQUEST ['_sort'] )) {
			$sort = $_REQUEST ['_sort'] ? 'asc' : 'desc';
		} else {
			$sort = $asc ? 'asc' : 'desc';
		}
		//取得满足条件的记录数
		$count = $model->where ( $map )->count ($model->getPk ());
	
		if ($count > 0) {
			import ( "@.ORG.Page" );
			//创建分页对象
			if (! empty ( $_REQUEST ['listRows'] )) {
				$listRows = $_REQUEST ['listRows'];
			} else {
				$listRows = '';
			}
			$p = new Page ( $count, $listRows );
			//分页查询数据

			$voList = $model->where($map)->order( "`" . $order . "` " . $sort)->limit($p->firstRow . ',' . $p->listRows)->findAll ( );
			
			//echo $model->getlastsql();
			//分页跳转的时候保证查询条件
			foreach ( $map as $key => $val ) {
				if (! is_array ( $val )) {
					$p->parameter .= "$key=" . urlencode ( $val ) . "&";
				}
			}
			
			foreach($voList as $key => $val){
				if($val['thumbs']){
					$filename="/data/www/resources/strends".$val['thumbs'];
					$showfile="http://s.stylemode.com.cn/".$val['thumbs'];
					if(file_exists($filename)){
						$voList[$key]['thumbs']="<img width='30' height='30' src=".$showfile." />";
					}else{
						$voList[$key]['thumbs']="图片不存在";
					}
				}
			}
		
			
			//分页显示
			$page = $p->show ();
			//列表排序显示
			$sortImg = $sort; //排序图标
			$sortAlt = $sort == 'desc' ? L('ASC_TITLE') : L('DESC_TITLE'); //排序提示
			$sort = $sort == 'desc' ? 1 : 0; //排序方式
			//模板赋值显示
			$this->assign ( 'list', $voList );
			$this->assign ( 'sort', $sort );
			$this->assign ( 'order', $order );
			$this->assign ( 'sortImg', $sortImg );
			$this->assign ( 'sortType', $sortAlt );
			$this->assign ( "page", $page );
			Cookie::set ('_currentUrl_',$p->currentUrl);
			Cookie::set ($returnUrl,$p->currentUrl);
		}
		else
		{
			Cookie::set ( '_currentUrl_', U($this->getActionName() . "/index"));
			Cookie::set ($returnUrl, U($this->getActionName() . "/index"));
		}
		return;
	}
	
	public function insert()
	{
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		//保存当前数据对象
		$id=$model->add ();
		
		if ($id!==false)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'thumbs')
					{
						if(!empty($album['thumbs']))
							@unlink(STRENDS_ROOT.$album['thumbs']);
						
						D($name)->where('id = '.$id)->setField('thumbs',$img);
					}
					
				}
			}
			$this->assign ( 'jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->saveLog(1,$id);
			$this->success (L('ADD_SUCCESS'));
		} else {
			//失败提示
			$this->saveLog(0,$id);
			$this->error (L('ADD_ERROR'));
		}
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ( $name );
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];

					if($upload_item['key'] == 'thumbs')
					{
						if(!empty($album['thumbs']))
							@unlink(STRENDS_ROOT.$album['thumbs']);
						
						D($name)->where('id = '.$id)->setField('thumbs',$img);
					}
					
				}
			}
			$this->assign ( 'jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->saveLog(1,$id);
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
}
