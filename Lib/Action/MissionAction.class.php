<?php
/**
 +------------------------------------------------------------------------------
 任务管理
 +------------------------------------------------------------------------------
 */
class MissionAction extends CommonAction
{
	public function add(){
		$medals = D("Medal")->where('status=1')->field('mid,name')->select();
		$this->assign('medals',$medals);
		$this->display();
	}
	public function insert()
	{
		$_POST['create_time'] = time();
		
		parent::insert();
	}
	
	public function edit()
	{	
		$id = intval($_REQUEST['id']);
		$vo = D("Mission")->where('id='.$id)->find();
		$medals = D("Medal")->where('status=1')->field('mid,name')->select();
		$this->assign('medals',$medals);
		$this->assign ('vo', $vo );
		$this->display();
	}
	
	public function update()
	{
		$desc = trim($_REQUEST['desc']);
		unset($_POST['conditions']);
		$_POST['ulevel'] = $_REQUEST['ulevel'];
		
		parent::update();
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			if(false !== $model->where($condition )->delete())
			{
				$condition = array ('mid' => array ('in', explode(',',$id)));
				D('UserMedal')->where($condition )->delete();
				D('MedalApply')->where($condition )->delete();
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}	
	function getMedalImg($img)
	{
		if(empty($img))
			return '';
		else
			return "<img src='http://www.trendshome.cn/res/public/mission/small/$img' width='24' />";
	}
	function getTypeName($type)
	{
		return L('GIVE_TYPE_'.$type);
	}

?>