<?php
/**
 +------------------------------------------------------------------------------
 * 会员
 +------------------------------------------------------------------------------
 */
class HongMoneyAction extends CommonAction
{
	public function index()
	{
		$parameter = array();

		$model = D('HongMoney');

		$sql = 'SELECT COUNT(mon.id) AS tcount FROM '.C("DB_PREFIX").'hong_money mon,'.C("DB_PREFIX").'hong hong where mon.uid=hong.uid';

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'hong_money mon,'.C("DB_PREFIX").'hong hong where mon.uid=hong.uid';;
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	
	public function asktype(){
		$id = $_REQUEST['id'];
		$type = $_REQUEST['t'];
		if($id>0 || $type>0){
			D("HongMoney")->query('update tbl_hong_money set type='.$type.' where id='.$id);
			if($type==3){
				$data = D('HongMoney')->where('id='.$id)->find();
				$nums = $data['money_num'];
				$hid = $data['uid'];
				D("Hong")->query('update tbl_hong set credits=credits-'.$nums.' where uid='.$hid);
			}
			$result = array('status'=>1);
		}else{
			$result = array('status'=>0);
		}
		die(json_encode($result));
		
	}
	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('avatar_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['avatar_img']))
						@unlink(RES_ROOT.$data['avatar_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}

function changetype($id){
	$data = D("HongMoney")->where("id=".$id)->find();
	$type = $data['type'];
	
	if($type==0){
		$return = "<div class='t_".$id."'><span onclick='asktype(".$id.",1)' style='cursor:pointer;'>同意</span>&nbsp;|&nbsp;<span onclick='asktype(".$id.",4)' style='cursor:pointer;'>驳回</span></div>";
		return $return;
	}elseif($type==1){
		$return = "<div class='t_".$id."'><span>已同意</span>&nbsp;|&nbsp;<span onclick='asktype(".$id.",2)' style='cursor:pointer;'>打款</span></div>";
		return $return;
	}elseif($type==2){
		$return = "<div class='t_".$id."'><span onclick='asktype(".$id.",3)' style='cursor:pointer;'>已打款</span></div>";
		return $return;
	}elseif($type==3){
		$return = "<span>结束</span>";
		return $return;
	}elseif($type==4){
		$return = "<span>已驳回</span>";
		return $return;
	}
	
}
?>