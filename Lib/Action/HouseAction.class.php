<?php
/**
 +------------------------------------------------------------------------------
 车型分类
 +------------------------------------------------------------------------------
 */
class HouseAction extends CommonAction
{
	public function add()
	{
		$home_list = D("GoodsTags")->where('site_key="home"')->order('sort asc')->findAll();
		$this->assign("home_list",$home_list);
		$this->display();
	}
	
	public function insert()
	{
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		//保存当前数据对象
		//$data['create_time'] = gmtTime();
		$data['create_time'] = gmtTime();
		//var_dump($data);die;
		$list=$model->add($data);
		if ($list !== false)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'thumbnail')
						$model->where("id=".$list)->setField("thumbnail",$img);
					elseif($upload_item['key'] == '3dflash')
						$model->where("id=".$list)->setField("3dflash",$img);
				}
			}
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
	}
	
	public function edit()
	{
		$id = intval($_REQUEST['id']);
		$vo = D("House")->getById($id);
		$this->assign ( 'vo', $vo );
		
		$home_list = D("GoodsTags")->where('site_key="home"')->order('sort asc')->findAll();
		$this->assign("home_list",$home_list);
		
		$this->display();
	}

	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}

		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages(0,'images',false,'',true))
			{
				$car = $model->getById($id);
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					if($upload_item['key'] == 'thumbnail')
					{
						if(!empty($car['thumbnail']))
							@unlink(STRENDS_ROOT.$car['thumbnail']);
						$model->where("id=".$id)->setField("thumbnail",$img);
					}
					elseif($upload_item['key'] == '3dflash')
					{
						if(!empty($car['3dflash']))
							@unlink(STRENDS_ROOT.$car['3dflash']);
						$model->where("id=".$id)->setField("3dflash",$img);
					}
				}
			}
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}

	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();

			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('thumbnail,3dflash')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['thumbnail']))
						@unlink(STRENDS_ROOT.$data['thumbnail']);
					
					if(!empty($data['3dflash']))
						@unlink(STRENDS_ROOT.$data['3dflash']);
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}
?>