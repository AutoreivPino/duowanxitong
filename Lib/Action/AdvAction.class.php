<?php
/**
 +------------------------------------------------------------------------------
 广告
 +------------------------------------------------------------------------------
 */
class AdvAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$name = trim($_REQUEST['name']);
		$position_id = intval($_REQUEST['position_id']);
		
		if(!empty($name))
		{
			$where .= " AND a.name LIKE '%".mysqlLikeQuote($name)."%'";
			$this->assign("name",$name);
			$parameter['name'] = $name;
		}

		if($position_id > 0)
		{
			$this->assign("position_id",$position_id);
			$parameter['position_id'] = $position_id;
			$where .= " AND a.position_id = '$position_id'";
		}

		$model = M();
		
		if(!empty($where))
			$where = 'WHERE 1' . $where;
		
		$sql = 'SELECT COUNT(DISTINCT a.id) AS acount 
			FROM '.C("DB_PREFIX").'adv AS a '.$where;

		$count = $model->query($sql);
		$count = $count[0]['acount'];

		$sql = 'SELECT a.*,ap.name AS position_name,ap.width AS pwidth,ap.height AS pheight  
			FROM '.C("DB_PREFIX").'adv AS a 
			LEFT JOIN '.C("DB_PREFIX").'adv_position AS ap ON ap.id = a.position_id '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'a.id');
		
		$ap_list = M("AdvPosition")->where('status = 1')->findAll();
		$this->assign("ap_list",$ap_list);
		
		$this->display ();
	}
	public function add()
	{
		$ap_list = M("AdvPosition")->where('status = 1')->findAll();
		foreach($ap_list as $key=>$val){
			if(in_array($val['id'],array(44,45,27,46,47))){
				$is[]=$val;
			}
		}
		import("@.ORG.form");
		$form=new form();
		$time=date("Y-m-d H:i",time());
		$starttime=$form->date("effective_time",$time,true);
		$endtime=$form->date("ineffective_time",$time,true);
		$this->assign("starttime",$starttime);
		$this->assign("endtime",$endtime);
		$this->assign("ap_list",$ap_list);
		$this->assign("ap_is",$is);
		parent::add();
	}
	
	public function insert()
	{
        /*添加广告时增加生效失效字段*/
        $_POST['effective_time']=strtotime($_POST['effective_time']);
        $_POST['ineffective_time']=strtotime($_POST['ineffective_time']);
		$_POST['time_add']=time();
		$_POST['desc'] = trim($_POST['desc']);
		if($_POST['effective_time']==$_POST['ineffective_time']){
			$_POST['is_efficacy']=1;
			$_POST['efficacy_long']=0;
		}else if(abs($_POST['ineffective_time']-$_POST['effective_time'])>0){
			$_POST['is_efficacy']=2;
			$_POST['efficacy_long']=$_POST['ineffective_time']-$_POST['effective_time'];
		}
		
		$model = D("Adv");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		//保存当前数据对象
		$positions=$_POST['positions'];
		
		$name_add=explode(',',trim($data['target_key'],','));
		foreach($name_add as $key=>$val){
				
				$data['target_key']=$val;
				if(count($positions)){
					$data['position_id']=$positions[$key];
				}
				
				$id = $model->add($data);
				
				$upload_list = $this->uploadImages(0,'adv',false,array(),true);
				
				if($upload_list)
				{
					foreach($upload_list as $plist){
						$info[]=$plist;
					}
				}
				
				if($info)
				{
					foreach($info as $plist){
						if($plist['key']=='code')
						{
							$img = $plist['recpath'].$plist['savename'];
							if(!empty($img))
								D("Adv")->where('id = '.$id)->setField('code',$img);
						}
						if($plist['key']=='small')
						{
							$img = $plist['recpath'].$plist['savename'];
							if(!empty($img))
								D("Adv")->where('id = '.$id)->setField('small',$img);
						}
						if($plist['key']=='small_img')
						{
							$img = $plist['recpath'].$plist['savename'];
							if(!empty($img))
								D("Adv")->where('id = '.$id)->setField('small_img',$img);
						}
					}
				}
		}
		
		
		
		 if ($id !== false)
		{
			
			
			$this->saveLog(1,$id);
			$this->success (L('ADD_SUCCESS'));

		}
		else
		{
			$this->saveLog(0);
			$this->error (L('ADD_ERROR'));
		} 
	}
	
	public function edit()
	{
		$ap_list = M("AdvPosition")->where('status = 1')->findAll();
		foreach($ap_list as $key=>$val){
			if(in_array($val['id'],array(25,26,27,28,29))){
				$is[$i]=$val;
				$is[$i]['name']=substr($val['name'],0,-6);
				
			}
			$i++;
		}
		
		$this->assign("ap_is",$is);
		$this->assign("ap_list",$ap_list);
		$name = $this->getActionName();
		$model = D($name);
		
		$id = $_REQUEST [$model->getPk ()];
		$vo = $model->getById($id);
		
        $vo["effective_time"]=date("Y-m-d H:i:s",$vo["effective_time"]);
        $vo["ineffective_time"]=date("Y-m-d H:i:s",$vo["ineffective_time"]);

		import("@.ORG.form");
		$form=new form();
		$starttime=$form->date("effective_time",$vo["effective_time"],true);
		$endtime=$form->date("ineffective_time",$vo["ineffective_time"],true);
		$this->assign("starttime",$starttime);
		$this->assign("endtime",$endtime);
		
		$this->assign ( 'vo', $vo );
		$this->display ();
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$_POST['desc'] = trim($_POST['desc']);
        $_POST['effective_time']=strtotime($_POST['effective_time']);
        $_POST['ineffective_time']=strtotime($_POST['ineffective_time']);
		
		if($_POST['effective_time']==$_POST['ineffective_time']){
			$_POST['is_efficacy']=1;
			$_POST['efficacy_long']=0;
		}else if(abs($_POST['ineffective_time']-$_POST['effective_time'])>0){
			$_POST['is_efficacy']=2;
			$_POST['efficacy_long']=$_POST['ineffective_time']-$_POST['effective_time'];
		}
		
		$old_img = D("Adv")->where('id = '.$id)->getField('code');
		
		$model = D("Adv");
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		//保存当前数据对象
		$list=$model->save($data);
		if (false !== $list)
		{
			$upload_list = $this->uploadImages(0,'adv',false,array(),true);
			if($upload_list)
			{
				foreach($upload_list as $plist){
					if($plist['key']=='code')
					{
						$img = $plist['recpath'].$plist['savename'];
						if(!empty($img))
						{
							if(!empty($old_img))
								@unlink(STRENDS_ROOT.$old_img);
							D("Adv")->where('id = '.$id)->setField('code',$img);
						}
					}
					
					if($plist['key']=='small')
					{
						$img = $plist['recpath'].$plist['savename'];
						if(!empty($img))
						{
							$old_small_img = D("Adv")->where('id = '.$id)->getField('small');
							if(!empty($old_small_img))
								@unlink(STRENDS_ROOT.$old_small_img);
							D("Adv")->where('id = '.$id)->setField('small',$img);
						}
							
						
					}
					if($plist['key']=='small_img')
					{
						$img = $plist['recpath'].$plist['savename'];
						if(!empty($img))
						{
							$old_small_imges = D("Adv")->where('id = '.$id)->getField('small_img');
							if(!empty($old_small_imges))
								@unlink(STRENDS_ROOT.$old_small_imges);
							D("Adv")->where('id = '.$id)->setField('small_img',$img);
						}
					}
					
				}
			}
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}

	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$advs = $model->where($condition)->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($advs as $adv)
				{
					if(!empty($adv['code']) && ($adv['type'] == 1 || $adv['type'] == 2))
						@unlink(STRENDS_ROOT.$adv['code']);
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}

?>