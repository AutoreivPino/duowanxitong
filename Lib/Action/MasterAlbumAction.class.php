<?php

class MasterAlbumAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$grid = trim($_REQUEST['grid']);
		
		if(!empty($grid))
		{
			$this->assign("grid",$grid);
			$parameter['grid'] = $grid;
			$where.=" grid LIKE '%".mysqlLikeQuote($grid)."%' ";
		}
		
		$model = M('MasterAlbum');

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(id) AS count 
			FROM '.C("DB_PREFIX").'master_album '.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['count'];
		
		$sql = 'SELECT *  FROM '.C("DB_PREFIX").'master_album '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	
	public function editPic(){
		$id = intval($_REQUEST['id']);
		$share_list = D("Mshare")->where('album_id='.$id)->order('sort asc,share_id asc')->findAll();
		
		$this->assign('id',$id);
		$this->assign('share_list',$share_list);
		
		$this->display('MasterAlbum/uploadpic');
	}
	public function delpic(){
		$shareid = intval($_REQUEST['shareid']);
		$albumid = intval($_REQUEST['albumid']);
		$imgpath = D("Mshare")->where('share_id='.$shareid)->getField('index_img');
		unlink(RES_ROOT.$imgpath);
		
		D("Mshare")->where('share_id='.$shareid)->delete();
	}
	public function update()
	{
        $id = intval($_REQUEST['id']);
		$model = D ('MasterAlbum');
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		//var_dump($_FILE); exit;
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where('id = '.$id)->setField('imgs',$img);
				}
			}
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('MasterAlbum');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		// 更新数据
		$list=$model->data($data)->add();
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where('id = '.$list)->setField('imgs',$img);
				}
			}
			$this->assign('id',$list);
			$showtype = $_POST['show_type'];
			if($showtype==0){
				$this->display('MasterAlbum/uploadpic');
			}else{
				$this->success (L('EDIT_SUCCESS'));
			}
		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
				D('MasterAlbum')->where('id='.$aid)->delete();
                $share_sql = "select * from tbl_mshare where album_id=".$aid;
				$share_data = D('Mshare')->query($share_sql);
				foreach($share_data as $k=>$v){
					if(!empty($v['index_img']))
						@unlink(RES_ROOT.$v['index_img']);
					D("Mshare")->where('share_id='.$v['share_id'])->delete();
				}
            }

			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}
	public function uploadpic(){
		$id = $_GET['id'];
		import("@.ORG.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = 3292200;
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg');
		$id_path = getDirsById($id);
		$dir = RES_ROOT.'public/upload/masteralbum/'.$id_path.'/';
		if(!file_exists($dir)){
			createFolder($dir);
		}
		
		$upload->savePath = RES_ROOT.'public/upload/masteralbum/'.$id_path.'/';
		 
		$upload->imageClassPath = '@.ORG.Image';
		 
		 //设置上传文件规则
		$upload->saveRule = 'uniqid';
		
		 if (!$upload->upload()) {
			//捕获上传异常
			$this->error($upload->getErrorMsg());
		 } else {
			$uploadList = $upload->getUploadFileInfo();
			//import("@.ORG.Image");
			//Image::water($uploadList[0]['savepath'].$uploadList[0]['savename'], APP_PATH.'Tpl/Public/Images/logo.png');
			$data = array();
			$data['index_img'] = 'public/upload/masteralbum/'.$id_path.'/'.$uploadList[0]['savename'];
			$data['album_id'] = $id;
			$data['sort'] = 100;
			$data['create_time'] = time();
			$model = M ('Mshare');
			
			$list=$model->data($data)->add();
			
			$result=array('shareid'=>$list,'id'=>$id,'shareimg'=>'public/upload/masteralbum/'.$id_path.'/'.$uploadList[0]['savename']);
			die(json_encode($result));
		 }
	}
	public function wxpic(){
		 file_put_contents('./1112.txt',222);
		import("@.ORG.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = 3292200;
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg');
		
		$day = date('Ym');
		$dir = RES_ROOT.'public/upload/wx/'.$day.'/';
		if(!file_exists($dir)){
			createFolder($dir);
		}	
		$upload->savePath = RES_ROOT.'public/upload/wx/'.$day.'/';
		$upload->imageClassPath = '@.ORG.Image';
		 file_put_contents('./12.txt',$dir);
		 //设置上传文件规则
		$upload->saveRule = 'uniqid';
		
		 if (!$upload->upload()) {
			//捕获上传异常
			$this->error($upload->getErrorMsg());
		 } else {
			$uploadList = $upload->getUploadFileInfo();
			//import("@.ORG.Image");
			//Image::water($uploadList[0]['savepath'].$uploadList[0]['savename'], APP_PATH.'Tpl/Public/Images/logo.png');
			$shareimg = 'public/upload/wx/'.$day.'/'.$uploadList[0]['savename'];
			$result=array('shareimg'=>$shareimg);
			die(json_encode($result));
		 }
	}
	public function wxdelpic(){
		$path = $_GET['path'];
		unlink(RES_ROOT.$path);
	}
	public function wxinsert(){
		$paths = $_POST['pathArray'];
		$sortArray = $_POST['sortArray'];
		$grid = $_POST['grid'];
		
		$model = D ('MasterAlbum');
		$data = array();
		$data['grid'] = $grid;
		$data['show_type'] = 0;
		$data['status'] = 1;
		$data['online_time'] = time();
		$data['create_time'] = time();
		// 更新数据
		$list=$model->data($data)->add();
		$mod = M ('Mshare');
		foreach($paths as $k=>$v){
			$dd = array();
			$dd['index_img'] = $v;
			$dd['album_id'] = $list;
			if($sortArray[$k]>0){
				$dd['sort'] = $sortArray[$k];
			}else{
				$dd['sort'] = $k;
			}			
			$dd['create_time'] = time();		
			$mod->data($dd)->add();
		}
		$result=array('status'=>1);
		die(json_encode($result));
	}
	public function textedit(){
		$aid = $_POST['aidArray'];
		$sortArray = $_POST['sortArray'];
		$model = M ('Mshare');
		$data = array();
		foreach($aid as $k=>$v){
			$data['share_id'] = $v;
			$data['sort'] = $sortArray[$k];
			$data['create_time'] = time();
			// 更新数据
			$model->save($data);
		}
		$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
		$this->success (L('EDIT_SUCCESS'));
	}
}

/**
 * 根据ID划分目录
 * @return string
 */
function getDirsById($id)
{
	$id = sprintf("%011d", $id);
	$dir1 = substr($id, 0, 3);
	$dir2 = substr($id, 3, 3);
	$dir3 = substr($id, 6, 3);
	$dir4 = substr($id, -2);
	return $dir1.'/'.$dir2.'/'.$dir3.'/'.$dir4;
}
function createFolder($path){
    if(!file_exists($path)){
        createFolder(dirname($path));
        mkdir($path);
    }
}
?>