<?php 
class TodayVoiceAction extends CommonAction{
	
		public function add(){
			$this->display();
		} 
		
		public function insert(){
			$condition['content'] = $_POST['content'];
			$condition['url'] = $_POST['url'];
			$condition['img'] = $_POST['img'];
			$path = '/public/upload/todayvoice/';
			import("ORG.Net.UploadFile");
			$upload = new UploadFile();
			$upload->savePath =  RES_ROOT.'public/upload/todayvoice/';
			$upload->saveRule = time().'_'.mt_rand();
			if(!$upload->upload()) {
			}else{
				$info =  $upload->getUploadFileInfo();
				$condition['img'] = $path.$info[0]["savename"];
			}
			$condition['addtime'] = time();
			$result = D("today_voice")->data($condition)->add();
			$this->redirect("TodayVoice/add");
		}
		
		public function push(){
			$id = $_POST['id'];
			$table = D("today_voice");
			$admin_id = $_SESSION[C('USER_AUTH_KEY')];
			if(isset($id)){
				$table->where('is_show=1')->setField('is_show',0);
				$table->where("id={$id}")->setField('is_show',1);
			}
			$time = strtotime("-1 day");
			$list = $table ->where("addtime >= {$time}")->select();
			$this->assign('list',$list);
			$this->assign('admin_id',$admin_id);
			$this->display();
		}
}
?>