<?php

class StarCommentAction extends CommonAction
{
	public function update()
	{
        $id = intval($_REQUEST['id']);
		$model = D ('StarComment');
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		//var_dump($_FILE); exit;
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('StarComment');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		// 更新数据
		$list=$model->data($data)->add();
		if (false !== $list)
		{
			$this->assign('id',$list);
			$this->success (L('EDIT_SUCCESS'));
		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
				$model->where('id='.$aid)->delete();
            }
			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		die(json_encode($result));
	}
	public function editField()
	{
		$id = intval($_REQUEST['id']);
		
		if($id == 0)
			exit;
		
		$val = trim($_REQUEST['val']);
		if($val == '')
			exit;
		
		$field = trim($_REQUEST['field']);
		if(empty($field))
			exit;
		
		$result = array('isErr'=>0,'content'=>'');
		$model = D("StarComment");
		$condition = array('id' => $id);
		
		$data = array();
		$data[$field] = $val;
		
		if(false !== $model->where($condition)->save($data))
		{
			$this->saveLog(1,$id,$field);
			$result['content'] = $val;
		}
		else
		{
			$this->saveLog(0,$id,$field);
			$result['isErr'] = 1;
			$result['content'] = L('EDIT_ERROR');
		}
		
		die(json_encode($result));
	}
	
}

?>