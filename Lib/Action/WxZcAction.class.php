<?php

class WxZcAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$title = trim($_REQUEST['title']);
		
		if(!empty($title))
		{
			$this->assign("title",$title);
			$parameter['title'] = $title;
			$where.=" title LIKE '%".mysqlLikeQuote($title)."%' ";
		}
		
		$model = M('WxZc');

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(id) AS count 
			FROM '.C("DB_PREFIX").'wx_zc '.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['count'];
		
		$sql = 'SELECT *  FROM '.C("DB_PREFIX").'wx_zc '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	
	public function update()
	{
        $id = intval($_REQUEST['id']);
		$model = D ('WxZc');
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		//var_dump($_FILE); exit;
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where('id = '.$id)->setField('best_img',$img);
				}
			}
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function insert(){
		$model = D ('WxZc');
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['create_time'] = time();
		// 更新数据
		$list=$model->data($data)->add();
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where('id = '.$list)->setField('best_img',$img);
				}
			}
			$this->assign('id',$list);
			$this->success (L('EDIT_SUCCESS'));
		}else{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
				$model->where('id='.$aid)->delete();
            }
			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		die(json_encode($result));
	}
}

?>