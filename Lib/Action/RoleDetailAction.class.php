<?php
	class RoleDetailAction extends CommonAction{
		public function index() {
			//列表过滤器，生成查询Map对象
			$map = $this->_search ();
			if (method_exists ( $this, '_filter' )) {
				$this->_filter ( $map );
			}
			$name=$this->getActionName();
			$model = D ($name);
			if (! empty ( $model )) {
				$this->_list ( $model, $map );
			}
			$this->display ();
			return;
		}
	
		protected function _list($model, $map, $sortBy = '', $asc = false,$returnUrl = 'returnUrl') {
		//排序字段 默认为主键名
		if (isset ( $_REQUEST ['_order'] )) {
			$order = $_REQUEST ['_order'];
		} else {
			$order = ! empty ( $sortBy ) ? $sortBy : $model->getPk ();
		}
		//排序方式默认按照倒序排列
		//接受 sost参数 0 表示倒序 非0都 表示正序
		if (isset ( $_REQUEST ['_sort'] )) {
			$sort = $_REQUEST ['_sort'] ? 'asc' : 'desc';
		} else {
			$sort = $asc ? 'asc' : 'desc';
		}
		//取得满足条件的记录数
		$count = $model->where ( $map )->count ($model->getPk ());
	
		if ($count > 0) {
			import ( "@.ORG.Page" );
			//创建分页对象
			if (! empty ( $_REQUEST ['listRows'] )) {
				$listRows = $_REQUEST ['listRows'];
			} else {
				$listRows = '';
			}
			$p = new Page ( $count, $listRows );
			//分页查询数据

			$voList = $model->where($map)->order( "`" . $order . "` " . $sort)->limit($p->firstRow . ',' . $p->listRows)->findAll ( );
			$admin=D('Admin')->select();
			$user=D('User')->select();
			$Role=D('Role')->select();
			foreach($voList as $key=>$val){
				foreach($admin as $akey=>$aval){
					if($val['from']==2){
						$data[$key]['id']=$val['id'];
						if($val['mid']==$aval['id']){
							$data[$key]['mid']=$aval['admin_name'];
							foreach($user as $ukey=>$uval){
								if($val['uid']==$uval['uid']){
									$data[$key]['uid']=$uval['show_name'];
								}	
							}
							foreach($Role as $rkey=>$rval){
								if($val['type']==$rval['id']){
									$data[$key]['type']=$rval['name'];
								}	
							}
							$data[$key]['from']='admin';
							
						}
					}
				}
			}
			
			foreach($voList as $key=>$val){
				foreach($user as $akey=>$aval){
					if($val['from']==1){
						$data[$key]['id']=$val['id'];
						if($val['mid']==$aval['uid']){
							$data[$key]['mid']=$aval['user_name'];
							foreach($user as $ukey=>$uval){
								if($val['uid']==$uval['uid']){
									$data[$key]['uid']=$uval['show_name'];
								}	
							}
							foreach($Role as $rkey=>$rval){
								if($val['type']==$rval['id']){
									$data[$key]['type']=$rval['name'];
								}	
							}
							$data[$key]['from']='user';
							
						}
					}
				}
			}
		
			//echo $model->getlastsql();
			//分页跳转的时候保证查询条件
			foreach ( $map as $key => $val ) {
				if (! is_array ( $val )) {
					$p->parameter .= "$key=" . urlencode ( $val ) . "&";
				}
			}
			//分页显示
			$page = $p->show ();
			//列表排序显示
			$sortImg = $sort; //排序图标
			$sortAlt = $sort == 'desc' ? L('ASC_TITLE') : L('DESC_TITLE'); //排序提示
			$sort = $sort == 'desc' ? 1 : 0; //排序方式
			//模板赋值显示
			$this->assign ( 'list', $data );
			$this->assign ( 'sort', $sort );
			$this->assign ( 'order', $order );
			$this->assign ( 'sortImg', $sortImg );
			$this->assign ( 'sortType', $sortAlt );
			$this->assign ( "page", $page );
			Cookie::set ('_currentUrl_',$p->currentUrl);
			Cookie::set ($returnUrl,$p->currentUrl);
		}
		else
		{
			Cookie::set ( '_currentUrl_', U($this->getActionName() . "/index"));
			Cookie::set ($returnUrl, U($this->getActionName() . "/index"));
		}
		return;
	}
	
	
		public function add(){
			$fashion=D('user')->where('site_key!=""')->field('uid,user_name,site_key')->select();
			$role=D('role')->select();			

			$this->assign('fashion',$fashion);
			$this->assign('role',$role);
			$this->display();
		}
		
		public function edit(){
			if($_GET['id']){
				$id=$_GET['id'];
				$RoleDetail=D('RoleDetail')->where('id="'.$id.'"')->select();

			}
			$mo=D('RoleDetail')->where('id='.$id)->select();
			if($mo[0]['from']==1){
				$qian=D('user')->where('uid='.$mo[0]['mid'])->field('uid,user_name,site_key')->select();
			}else{
				$hou=D('admin')->where('id='.$mo[0]['mid'])->field('admin_name,id')->select();
			}
			$admin=D('user')->where('site_key!=""')->field('uid,user_name,site_key')->select();
			$role=D('Role')->select();
			foreach($role as $key=>$val){
				if($val['id']==$RoleDetail[0]['type']){
					$role[$key]['have']=1;
				}
			}
			$admins=D('Admin')->select();
			$user=D('RoleDetail')->where('mid='.$RoleDetail[0]['mid'].' and type='.$RoleDetail[0]['type'])->select();
			foreach($admin as $key=>$val){
				foreach($user as $k=>$v){
					if($val['uid']==$v['uid']){
						$admin[$key]['is_have']='1';
					}
				}
			}
			
			$this->assign('RoleDetail',$RoleDetail);
			$this->assign('admin',$qian);
			$this->assign('hou',$hou);
			$this->assign('fashion',$admin);
			$this->assign('role',$role);
			$this->display();
		}
		public function res(){
			$sel=$_POST['model'];
			if($sel=='1'){
				$admin=D('user')->where('site_key!=""')->field('uid,user_name,site_key')->select();
			}else{
				$admin=D('admin')->field('admin_name,id')->select();
			}
			echo  json_encode(array('res'=>$admin));
		}
		public function insert(){
			$mid=$_POST['mid'];
			$uid=$_POST['uid'];
			$type=$_POST['type'];
			$redis=$_POST['redis'];
			
			if(is_array($uid)){
				D('RoleDetail')->where('mid='.$mid.' and `from` ='.$redis.' and type='.$type)->delete();
				foreach($uid as $key=>$val){
					$result=D('RoleDetail')->execute('insert into tbl_role_detail(mid,uid,type,`from`) value("'.$mid.'","'.$val.'","'.$type.'","'.$redis.'")');
				}
			}

			if ($result!==false) { //保存成功
				$this->saveLog(1,$list);
				$this->assign ( 'jumpUrl', Cookie::get ( '_currentUrl_' ) );
				$this->success (L('ADD_SUCCESS'));
			} else {
				//失败提示
				$this->saveLog(0,$list);
				$this->error (L('ADD_ERROR'));
			} 
		}
		
		
		public function update(){
			$mid=$_POST['mid'];
			$uid=$_POST['uid'];
			$type=$_POST['type'];
			$form=$_POST['redis'];
			
			if(is_array($uid)){
				D('RoleDetail')->where('mid='.$mid.' and `from` ="'.$form.'" and type='.$type)->delete();
				foreach($uid as $key=>$val){
					$result=D('RoleDetail')->execute('insert into tbl_role_detail(mid,uid,type,`from`) value("'.$mid.'","'.$val.'","'.$type.'","'.$form.'")');
				}
			}

			if ($result!==false) { //保存成功
				$this->saveLog(1,$list);
				$this->assign ( 'jumpUrl', Cookie::get ( '_currentUrl_' ) );
				$this->success (L('ADD_SUCCESS'));
			} else {
				//失败提示
				$this->saveLog(0,$list);
				$this->error (L('ADD_ERROR'));
			} 
		}
		
		public function edit_type(){
			$mid=$_POST['mid'];
			$redis=$_POST['redis'];
			$type=$_POST['type'];
			$data['mid']=$mid;
			$data['from']=$redis;
			$data['type']=$type;
			$result=D('RoleDetail')->where($data)->select();
			$user=D('user')->where('site_key!=""')->field('uid,user_name,site_key')->select();
			
			foreach ($user as $key=>$val){
				$user[$key]['have']=0;
			}
			
			foreach($user as $key=>$val){
				foreach($result as $k=>$v){
					if($val['uid']==$v['uid']){
						$user[$key]['have']=1;
					}
				}
			}
			foreach($user as $key=>$val){
				if($val['site_key']=='home'){
					$home[]=$val;
				}else if($val['site_key']=='auto'){
					$auto[]=$val;
				}else if($val['site_key']=='fashion'){
					$fashion[]=$val;
				}
			}
			$da[0]=$home;
			$da[1]=$auto;
			$da[2]=$fashion;
			echo  json_encode(array('res'=>$da));
		}
	
	
	}
	