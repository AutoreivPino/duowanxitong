<?php
// +----------------------------------------------------------------------
// | 方维购物分享网站系统 (Build on ThinkPHP)
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://fanwe.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: awfigq <awfigq@qq.com>
// +----------------------------------------------------------------------
/**
 +------------------------------------------------------------------------------
 会员等级
 +------------------------------------------------------------------------------
 */
class LevelAction extends CommonAction
{
	public function insert()
	{
		$desc = trim($_REQUEST['desc']);
		parent::insert();
	}
	
	public function edit()
	{	
		$id = intval($_REQUEST['lid']);
		$vo = D("Level")->where('lid='.$id)->find();
		
		$user_groups = D("UserGroup")->where('status = 1')->findAll();
		$this->assign("user_groups",$user_groups);
		
		$this->assign ('vo', $vo );
		$this->display();
	}
	
	public function update()
	{
		$desc = trim($_REQUEST['desc']);
		
		parent::update();
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			if(false !== $model->where($condition )->delete())
			{
				$condition = array ('lid' => array ('in', explode(',',$id)));
				
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}
function getLevelImg($img)
{
	if(empty($img))
		return '';
	else
		return "<img src='".__ROOT__."/public/medal/small/$img' width='24' />";
}
?>