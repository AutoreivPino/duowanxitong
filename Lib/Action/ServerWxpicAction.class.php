<?php

class ServerWxpicAction extends CommonAction
{
	public function index(){
		$model = M();

		$sql = 'SELECT COUNT(DISTINCT id) AS tcount FROM '.C("DB_PREFIX").'server_wxpic ';
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'server_wxpic ';
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	public function klist(){
		$model = M();

		$sql = 'SELECT COUNT(DISTINCT id) AS tcount FROM '.C("DB_PREFIX").'server_wxpic where ptype=2 ';
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'server_wxpic where ptype=2';
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	
	public function update()
	{
		
		$type = 'image';
		$offset = isset($_GET['offset'])?$_GET['offset']:0;
		$count = 20;
		$access_token = get_token();
		$url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=".$access_token;  
		$data = '{"type":"'.$type.'","offset":"'.$offset.'","count":"'.$count.'"}';  
		
		//返回的数据  
		$response = get_response_post($url, $data);  
		//echo strip_tags($response);  
		$res = json_decode($response, true); 
		
		
		foreach($res['item'] as $k=>$v){
			$title = $v['name'];
			$media_id = $v['media_id'];
			$best_img = $v['url'];
			$create_time = $v['update_time'];
			if($k==0 && $offset==0){
				file_put_contents('/server/www/res/public/upload/sermediaid.txt',$media_id);
			}
			
			$list = D('ServerWxpic')->where("media_id='".$media_id."'")->select();		
			
			if(count($list)<=0){
				$y = date('Y');
				$title = strtolower($title);
				if(preg_match('/^'.$y.'.*/',$title)){
					$img_data = @file_get_contents($best_img);
					if(!empty($img_data)){
						$dtime = date('md');
						
						$dir = '/server/www/res/public/upload/sermenu/'.$dtime.'/';
						
						if(!file_exists($dir)){
							
							mkdir('/server/www/res/public/upload/sermenu/'.$dtime.'/');
						}
						$pdir = '/server/www/res/';
						$img_index = 'public/upload/sermenu/'.$dtime.'/'.$title;
						
						$img_size = @file_put_contents($pdir.$img_index, $img_data);
						$local_img = $img_index; 
					} 
					$sql = "insert into tbl_server_wxpic (title,create_time,best_img,media_id,local_img,ptype) values('{$title}',{$create_time},'{$best_img}','{$media_id}','{$local_img}',1)";			
					D('ServerWxpic')->query($sql);
				}elseif(preg_match('/^zaijia.*/',$title) || preg_match('/^qrcode.*/',$title)){
					$img_data = @file_get_contents($best_img);
					if(!empty($img_data)){
						$dtime = date('md');
						
						$dir = '/server/www/res/public/upload/sermenu/'.$dtime.'/';
						
						if(!file_exists($dir)){
							
							mkdir('/server/www/res/public/upload/sermenu/'.$dtime.'/');
						}
						$pdir = '/server/www/res/';
						$img_index = 'public/upload/sermenu/'.$dtime.'/'.$title;
						
						$img_size = @file_put_contents($pdir.$img_index, $img_data);
						$local_img = $img_index; 
					} 
					$sql = "insert into tbl_server_wxpic (title,create_time,best_img,media_id,local_img,ptype) values('{$title}',{$create_time},'{$best_img}','{$media_id}','{$local_img}',2)";			
					D('ServerWxpic')->query($sql);
				}
				
			}else{
				$mediaid = file_get_contents('/server/www/res/public/upload/sermediaid.txt');
				$msql = "insert into tbl_server_wxpic (media_id,ptype) values('{$media_id}',3)";			
				D('ServerWxpic')->query($msql);
				$this->redirect('ServerWxpic/index');
			}			
		}
		echo "waiting....";
		sleep(5);
		$oset = $offset+20;
		if($oset<6458){
			header('Location:/trends/index.php?m=ServerWxpic&a=update&offset='.$oset);
		}else{
			echo $oset;
		}
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			if(D("Album")->where(array("cid"=>array('in',explode(',',$id))))->count()>0)
			{
				$result['isErr'] = 1;
				$result['content'] = L('ALBUM_EXIST');
				die(json_encode($result));
			}
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('best_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['best_img']))
						@unlink(RES_ROOT.$data['best_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
}

 
function get_token() {  
	$appid = 'wx33901c7e2cb9bb6a';
	$secret = '61dcc1e7c3a7aa84c4912e1f8dd9d4c4';
	
	$data = D("ServerToken")->find();
	if(count($data)>0){
		$token = $data['tk'];
		$ctime = $data['create_time'];	
		if ($ctime<time()) {
			$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
			$access_token = json_decode(file_get_contents($token_url));
			if (isset($access_token->errcode)) {
				echo '<h1>错误：</h1>'.$access_token->errcode;
				echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
				exit;
			}
			$token = $access_token->access_token;
			$time = time()+7000;
			$tsql = "update tbl_server_token set tk='".$token."',create_time=".$time;
			D("ServerToken")->query($tsql);
			
		}
	}else{
		$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
		$access_token = json_decode(file_get_contents($token_url));
		if (isset($access_token->errcode)) {
			echo '<h1>错误：</h1>'.$access_token->errcode;
			echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
			exit;
		}
		$token = $access_token->access_token;
		$time = time()+7000;
		$tsql = "insert into tbl_server_token (tk,create_time) values('".$token."',".$time.")";
		D("ServerToken")->query($tsql);
	}
	
  return $token;  
}
  
function get_response_post($url, $data)  
{  
	 $params = array('http' => array(
                 'method' => 'POST',
                 'content' => $data
              ));
    if ($optional_headers !== null) {
       $params['http']['header'] = $optional_headers;
    }
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if (!$fp) {
       throw new Exception("Problem with $url, $php_errormsg");
    }
    $response = @stream_get_contents($fp);
    if ($response === false) {
       throw new Exception("Problem reading data from $url, $php_errormsg");
    }
    return $response;
} 
?>