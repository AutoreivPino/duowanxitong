<?php
/**
 +------------------------------------------------------------------------------
 autodiy
 +------------------------------------------------------------------------------
 */
class AutoDiyAction extends CommonAction
{
	public function add(){
		$nav = array('pcm'=>'通讯管理系统','sw'=>'方向盘','dew'=>'主驾电动车窗','fcc_bl'=>'前部中控台左下','fcc_tl'=>'前部中控台左上','fcc_c'=>'前部中控台中','fcc_tr'=>'前部中控台右上','fcc_br'=>'前部中控台右下','db'=>'仪表盘','msb'=>'记忆设置按钮','acs'=>'后中控系统','oc'=>'顶置控制台',);
		
		$rules = array('btn_pcm_sa'=>'音源设置','btn_pcm_drm'=>'收音机菜单','btn_pcm_dasm'=>'音频源菜单','btn_pcm_r'=>'接听按钮','btn_pcm_dtm'=>'电话菜单','btn_pcm_ec'=>'结束通话','btn_pcm_dnm'=>'导航菜单','btn_pcm_ablr'=>'左/右箭头按钮','btn_pcm_lk'=>'左侧旋钮','btn_pcm_vasm'=>'音响设置','btn_pcm_sim'=>'SIM卡插槽','btn_pcm_dgi'=>'常规信息','btn_pcm_ddd'=>'车辆/行程数据','btn_pcm_rk'=>'右侧旋钮','btn_pcm_mm'=>'相应主菜单','btn_pcm_bd'=>'后退/删除','btn_pcm_eb'=>'弹出按钮','btn_pcm_dnfm'=>'导航地图','btn_sw_ch'=>'音量控制','btn_sw_sil'=>'转向指示灯','btn_sw_ab'=>'安全气囊','btn_sw_ccscs'=>'巡航定速控制系统','btn_sw_horn'=>'喇叭','btn_sw_shb'=>'换档按钮','btn_sw_cb'=>'自定义按钮','btn_sw_bd'=>'后退/删除','btn_sw_sb'=>'选择按钮','btn_sw_ww'=>'挡风玻璃雨刷器','btn_sw_sb'=>'选择按钮','btn_sw_r'=>'接听按钮','btn_sw_ec'=>'通话结束','btn_dew_cspw'=>'副驾驶侧电动车窗','btn_dew_lrpw'=>'左后侧电动车窗','btn_dew_rrpw'=>'右后侧电动车窗','btn_dew_ls'=>'童锁开关','btn_dew_tgs'=>'尾门开关','btn_dew_dspw'=>'驾驶员侧电动车窗','btn_dew_cscrm'=>'选择副驾车外后视镜','btn_dew_scrm'=>'选择主驾车外后视镜','btn_dew_arm'=>'调节车外后视镜','btn_dew_fem'=>'折合车外后视镜','btn_fcc_bl_airdc'=>'加热式座椅','btn_fcc_bl_sport'=>'运动模式','btn_fcc_bl_asms'=>'主动悬挂管理系统','btn_fcc_bl_ha'=>'加高高度','btn_fcc_bl_sms'=>'稳定管理系统','btn_fcc_tl_tc'=>'温控','btn_fcc_tl_wc'=>'风控','btn_fcc_tl_hm'=>'余热模式','btn_fcc_tl_airw'=>'气流吹向挡风玻璃','btn_fcc_tl_airc'=>'气流吹向中央和侧出风口','btn_fcc_tl_airf'=>'气流吹向脚坑','btn_fcc_c_wl'=>'危险警示灯','btn_fcc_c_cl'=>'中控锁','btn_fcc_c_wd'=>'挡风玻璃除霜','btn_fcc_c_hrw'=>'加热式后窗','btn_fcc_c_ars'=>'空气再循环系统','btn_fcc_c_mono'=>'全车统一模式','btn_fcc_c_acmax'=>'空调最大制冷','btn_fcc_c_ac'=>'空调','btn_fcc_tr_tc'=>'温控','btn_fcc_tr_wc'=>'风控','btn_fcc_tr_rear'=>'REAE模式','btn_fcc_tr_airw'=>'气流吹向挡风玻璃','btn_fcc_tr_airc'=>'气流吹向中央和侧出风口','btn_fcc_tr_airf'=>'气流吹向脚坑','btn_fcc_br_airdc'=>'加热式座椅','btn_fcc_br_rrs'=>'可伸缩后扰流板','btn_fcc_br_ass'=>'自动启动/停止','btn_db_ot'=>'机油压力/温度表','btn_db_sm'=>'车速表','btn_db_tm'=>'转速表','btn_db_mfd'=>'多功能显示器','btn_db_cf'=>'冷却液温度/燃油表','btn_msb_mb'=>'记忆按钮','btn_msb_pb1'=>'个性化按钮1','btn_msb_pb2'=>'个性化按钮2','btn_msb_cas'=>'取消自动设置','btn_acs_wc'=>'风控','btn_acs_tc'=>'温控','btn_acs_airw'=>'气流吹向挡风玻璃','btn_acs_airc'=>'气流吹向中央和侧出风口','btn_acs_airf'=>'气流吹向脚坑','btn_acs_rear'=>'REAR模式','btn_acs_airdc'=>'加热式座椅','btn_oc_pl'=>'定位灯','btn_oc_frl'=>'前排阅读灯','btn_oc_oidl'=>'内部车门开启照明','btn_oc_hm'=>'免提麦克风','btn_oc_fcl'=>'前部车内照明','btn_oc_tsr'=>'可倾/滑动式天窗','btn_oc_pas'=>'停车辅助系统','btn_oc_pcms'=>'乘客舱监控系统',); 
		$configs = array(
				'pcm'=>array(
					'btn_pcm_sa','btn_pcm_drm','btn_pcm_dasm','btn_pcm_r','btn_pcm_dtm','btn_pcm_ec','btn_pcm_dnm','btn_pcm_ablr','btn_pcm_lk','btn_pcm_vasm','btn_pcm_sim','btn_pcm_dgi','btn_pcm_ddd','btn_pcm_rk','btn_pcm_mm','btn_pcm_bd','btn_pcm_eb','btn_pcm_dnfm'
				),
				'sw'=>array(
					'btn_sw_ch','btn_sw_sil','btn_sw_ccscs','btn_sw_ab','btn_sw_horn','btn_sw_shb','btn_sw_cb','btn_sw_bd','btn_sw_ww','btn_sw_sb','btn_sw_r','btn_sw_ec',
				),'dew'=>array(
					'btn_dew_cspw','btn_dew_lrpw','btn_dew_rrpw','btn_dew_ls','btn_dew_tgs','btn_dew_dspw','btn_dew_cscrm','btn_dew_scrm','btn_dew_arm','btn_dew_fem',
				),'fcc_bl'=>array(
					'btn_fcc_bl_airdc','btn_fcc_bl_sport','btn_fcc_bl_asms','btn_fcc_bl_ha','btn_fcc_bl_sms',
				),'fcc_tl'=>array(
					'btn_fcc_tl_tc','btn_fcc_tl_wc','btn_fcc_tl_hm','btn_fcc_tl_airw','btn_fcc_tl_airc','btn_fcc_tl_airf',
				),'fcc_c'=>array(
					'btn_fcc_c_wl','btn_fcc_c_cl','btn_fcc_c_wd','btn_fcc_c_hrw','btn_fcc_c_ars','btn_fcc_c_mono','btn_fcc_c_acmax','btn_fcc_c_ac',
				),'fcc_tr'=>array(
					'btn_fcc_tr_tc','btn_fcc_tr_wc','btn_fcc_tr_rear','btn_fcc_tr_airw','btn_fcc_tr_airc','btn_fcc_tr_airf',
				),'fcc_br'=>array(
					'btn_fcc_br_airdc','btn_fcc_br_rrs','btn_fcc_br_ass',
				),'db'=>array(
					'btn_db_ot','btn_db_sm','btn_db_tm','btn_db_mfd','btn_db_cf'
				),'msb'=>array(
					'btn_msb_mb','btn_msb_pb1','btn_msb_pb2','btn_msb_cas',
				),'acs'=>array(
					'btn_acs_airc','btn_acs_airf','btn_acs_rear','btn_acs_airdc','btn_acs_airw','btn_acs_tc','btn_acs_wc',
				),'oc'=>array(
					'btn_oc_fcl','btn_oc_tsr','btn_oc_pas','btn_oc_pcms','btn_oc_hm','btn_oc_oidl','btn_oc_frl','btn_oc_pl',
				),
		);
		//$ppost = $_SESSION['post_session'];
		$rmepconfig = file_get_contents(RES_ROOT.'public/upload/autodiy/tempconfig.txt');
		$ppost=unserialize($rmepconfig);
		
		foreach($ppost as $k=>$v){
			$arr = explode('_',$k);
			$str = array_pop($arr);
			$key = implode('_',$arr);
			$ppost[$key][$str] = $v;
		}
		
		$this->assign('rules',$rules);
		$this->assign('nav',$nav);
		$this->assign('ppost',$ppost);
		$this->assign('configs',$configs);
		
		$this->display();		
	}
    public function insert(){
		$posts =  array();
		$configs =  array();
		$hostpath = 'http://s.styleauto.com.cn/';
		$path_time = date('md');
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->savePath =  RES_ROOT.'public/upload/autodiy/images/'.$path_time.'/';
		$upload->maxSize  = 204800000; // 设置附件上传大小
		$upload->saveRule = uniqid;
		
		$posts = $_POST;
		$rmepconfig = file_get_contents(RES_ROOT.'public/upload/autodiy/tempconfig.txt');
		$ppost=unserialize($rmepconfig);
		
		foreach($_FILES as $key=>$file){
			if(!empty($file['name'])){
				$info = $upload->uploadOne($file);
				if($info){
					$posts[$key] = $info[0]['savename'];
				}else{
					$posts[$key] = '';
				}
			}else{
				$posts[$key] = $ppost[$key];
			}
			
		}
		
		$buttons = array(
				'pcm'=>array(
					'btn_pcm_sa','btn_pcm_drm','btn_pcm_dasm','btn_pcm_r','btn_pcm_dtm','btn_pcm_ec','btn_pcm_dnm','btn_pcm_ablr','btn_pcm_lk','btn_pcm_vasm','btn_pcm_sim','btn_pcm_dgi','btn_pcm_ddd','btn_pcm_rk','btn_pcm_mm','btn_pcm_bd','btn_pcm_eb','btn_pcm_dnfm'
				),
				'sw'=>array(
					'btn_sw_ch','btn_sw_sil','btn_sw_ccscs','btn_sw_ab','btn_sw_horn','btn_sw_shb','btn_sw_cb','btn_sw_bd','btn_sw_ww','btn_sw_sb','btn_sw_r','btn_sw_ec',
				),'dew'=>array(
					'btn_dew_cspw','btn_dew_lrpw','btn_dew_rrpw','btn_dew_ls','btn_dew_tgs','btn_dew_dspw','btn_dew_cscrm','btn_dew_scrm','btn_dew_arm','btn_dew_fem',
				),'fcc_bl'=>array(
					'btn_fcc_bl_airdc','btn_fcc_bl_sport','btn_fcc_bl_asms','btn_fcc_bl_ha','btn_fcc_bl_sms',
				),'fcc_tl'=>array(
					'btn_fcc_tl_tc','btn_fcc_tl_wc','btn_fcc_tl_hm','btn_fcc_tl_airw','btn_fcc_tl_airc','btn_fcc_tl_airf',
				),'fcc_c'=>array(
					'btn_fcc_c_wl','btn_fcc_c_cl','btn_fcc_c_wd','btn_fcc_c_hrw','btn_fcc_c_ars','btn_fcc_c_mono','btn_fcc_c_acmax','btn_fcc_c_ac',
				),'fcc_tr'=>array(
					'btn_fcc_tr_tc','btn_fcc_tr_wc','btn_fcc_tr_rear','btn_fcc_tr_airw','btn_fcc_tr_airc','btn_fcc_tr_airf',
				),'fcc_br'=>array(
					'btn_fcc_br_airdc','btn_fcc_br_rrs','btn_fcc_br_ass',
				),'db'=>array(
					'btn_db_ot','btn_db_sm','btn_db_tm','btn_db_mfd','btn_db_cf'
				),'msb'=>array(
					'btn_msb_mb','btn_msb_pb1','btn_msb_pb2','btn_msb_cas',
				),'acs'=>array(
					'btn_acs_airc','btn_acs_airf','btn_acs_rear','btn_acs_airdc','btn_acs_airw','btn_acs_tc','btn_acs_wc',
				),'oc'=>array(
					'btn_oc_fcl','btn_oc_tsr','btn_oc_pas','btn_oc_pcms','btn_oc_hm','btn_oc_oidl','btn_oc_frl','btn_oc_pl',
				),
		);
		//$front_arr = array();
		//$temp_buttons = array();
		// $_SESSION['post_session'] = $posts;
		/* setcookie("postsession",serialize($posts),(time()+(3600*24*14)));
		setcookie("ss_sss",serialize($posts),time()+3600);
		$_COOKIE['post_session']=$posts; */
		file_put_contents(RES_ROOT.'public/upload/autodiy/tempconfig.txt',serialize($posts));
		foreach($buttons as $key=>$vo){
			$pathxml = '';
			if($key=='acs'){
				foreach($vo as $index=>$value){
					if(!empty($posts[$value.'_url'])){
						$posts[$value.'_img'] = $hostpath."public/upload/autodiy/images/".$posts[$value.'_url'];
					}else{
						$posts[$value.'_url'] = '';
					}
					if($value == 'btn_db_sm' || $value == 'btn_db_tm'){
						$posts[$value.'_info'] =  preg_replace("/<p>([\s\S]*)(.*?)<\/p>/","$1",$posts[$value.'_info']);
						
						$pathxml .="<Introduce symbol='".$value."'>\n<title text='".$posts[$value.'_text']."'></title>\n<info type='list'>".$posts[$value.'_info']."</info>\n</Introduce>\n";
					}else{
						$pathxml .="<Introduce symbol='".$value."'>\n<title url='".$posts[$value.'_img']."' text='".$posts[$value.'_text']."'></title>\n<info>".$posts[$value.'_info']."</info>\n</Introduce>\n";
					}
				}
			
				$pathbottom .= "<button name='".$key."'>\n<Introduces url='".$hostpath."public/upload/autodiy/".$key.".swf'>\n".$pathxml."</Introduces>\n</button>\n";
			}else{
				if($key=='dew'){
					foreach($vo as $index=>$value){
						if(!empty($posts[$value.'_url'])){
							$posts[$value.'_img'] = $hostpath."public/upload/autodiy/images/".$path_time."/".$posts[$value.'_url'];
						}else{
							$posts[$value.'_url'] = '';
						}
						if($value == 'btn_db_sm' || $value == 'btn_db_tm'){
							$posts[$value.'_info'] =  preg_replace("/<p>([\s\S]*)(.*?)<\/p>/","$1",$posts[$value.'_info']);
							
							$pathxml .="<Introduce symbol='".$value."'>\n<title text='".$posts[$value.'_text']."'></title>\n<info type='list'>".$posts[$value.'_info']."</info>\n</Introduce>\n";
						}else{
							$pathxml .="<Introduce symbol='".$value."'>\n<title url='".$posts[$value.'_img']."' text='".$posts[$value.'_text']."'></title>\n<info>".$posts[$value.'_info']."</info>\n</Introduce>\n";
						}
					
					}
				
					$pathleft .= "<button name='".$key."'>\n<Introduces url='".$hostpath."public/upload/autodiy/".$key.".swf'>\n".$pathxml."</Introduces>\n</button>\n";
				}
				$pathxml = '';
				foreach($vo as $index=>$value){
					if(!empty($posts[$value.'_url'])){
						$posts[$value.'_img'] = $hostpath."public/upload/autodiy/images/".$path_time."/".$posts[$value.'_url'];
					}else{
						$posts[$value.'_url'] = '';
					}
					if($value == 'btn_db_sm' || $value == 'btn_db_tm'){
						$posts[$value.'_info'] =  preg_replace("/<p>([\s\S]*)(.*?)<\/p>/","$1",$posts[$value.'_info']);
						
						$pathxml .="<Introduce symbol='".$value."'>\n<title text='".$posts[$value.'_text']."'></title>\n<info type='list'>".$posts[$value.'_info']."</info>\n</Introduce>\n";
					}else{
						$pathxml .="<Introduce symbol='".$value."'>\n<title url='".$posts[$value.'_img']."' text='".$posts[$value.'_text']."'></title>\n<info>".$posts[$value.'_info']."</info>\n</Introduce>\n";
					}
				
				}
			
				$pathfront .= "<button name='".$key."'>\n<Introduces url='".$hostpath."public/upload/autodiy/".$key.".swf'>\n".$pathxml."</Introduces>\n</button>\n";
			}
			
		}
		$frontconfigs = "<frontconfig>\n<Mtl url = '".$hostpath."public/upload/autodiy/front.swf' id='front'/>\n".$pathfront."\n</frontconfig>";
		$bottomconfigs = "<bottomconfig>\n<Mtl url = '".$hostpath."public/upload/autodiy/bottom.swf' id='bottom'/>\n".$pathbottom."\n</bottomconfig>";
		$leftconfigs = "<leftconfig>\n<Mtl url = '".$hostpath."public/upload/autodiy/left.swf' id='left'/>\n".$pathleft."\n</leftconfig>";
		
		$xmlcont = file_get_contents(RES_ROOT.'public/upload/autodiy/config.xml');
		
		$frontdiy =  preg_replace("/<frontconfig>(.*?)<\/frontconfig>/s",$frontconfigs,$xmlcont);
		$bottomdiy =  preg_replace("/<bottomconfig>(.*?)<\/bottomconfig>/s",$bottomconfigs,$frontdiy);
		$leftdiy =  preg_replace("/<leftconfig>(.*?)<\/leftconfig>/s",$leftconfigs,$bottomdiy);
		$path_time = date('mdhis');
		file_put_contents(RES_ROOT.'public/upload/autodiy/config'.$path_time.'.xml',$leftdiy);
		/* exit;
		
		$test = json_encode($configs);
		file_put_contents(AUTODIY_PATH.'public/upload/autodiy/autodiy.txt',$test);
		echo "<pre>";
		var_dump($configs);
		
		exit; */
        $this->redirect('AutoDiy/add', array(), 2,'上传成功~');
   } 
 
}

?>