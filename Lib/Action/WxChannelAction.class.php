<?php

class WxChannelAction extends CommonAction
{
	public function index(){
		$model = M();

		$sql = 'SELECT COUNT(DISTINCT id) AS tcount FROM '.C("DB_PREFIX").'wx_channel ';
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'wx_channel';
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	
	public function insert()
	{		
		$name=$this->getActionName();
		$model = D ($name);
		
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		//保存当前数据对象
		$list=$model->add($data);
		if ($list !== false)
		{
			$title = 'channel_'.$list;
			$model->where("id=".$list)->setField("title",$title);
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
		
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		$list=$model->save($data);
		
		if (false !== $list)
		{
			if($upload_list = $this->uploadImages())
			{
				
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					$model->where("id=".$id)->setField("best_img",$img);
				}
			}
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			//$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			if(D("Album")->where(array("cid"=>array('in',explode(',',$id))))->count()>0)
			{
				$result['isErr'] = 1;
				$result['content'] = L('ALBUM_EXIST');
				die(json_encode($result));
			}
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('best_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['best_img']))
						@unlink(RES_ROOT.$data['best_img']);
					
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
	public function downpic(){
		$id = $_REQUEST['id'];
		$name=$this->getActionName();
		$model = D ($name);
		$list = $model->where("id=".$id)->find();
		ob_start(); 
		 $filename=RES_ROOT.$list['best_img'];
		 $date=$list['title'];
		 header( "Content-type:  application/octet-stream "); 
		 header( "Accept-Ranges:  bytes "); 
		 header( "Content-Disposition:  attachment;  filename= {$date}.png"); 
		 $size=readfile($filename); 
		  header( "Accept-Length: " .$size);
	}
	
	public function showlist(){
		$id = '1000000'.$_REQUEST['tid'];
		$type = $_REQUEST['type'];
		$page = $_REQUEST['page'];
		$count = D("User")->query('select count(uid) as ucount,reg_day from tbl_user where status=1 and channel_id='.$id.' group by reg_day');
		$c = count($count);
		$allpage = $c/20;
		
		if($type==1){
			$n = ($page-1)*20;
			$next = ' limit '.$n.',20';
		}elseif($type==2){
			$n = ($page+1)*20;
			$next = ' limit '.$n.',20';
		}else{
			$next = ' limit 0,20 ';
		}
		
		$list = D("User")->query('select count(uid) as ucount,reg_day from tbl_user where status=1 and channel_id='.$id.' group by reg_day order by reg_day desc '.$next);
		foreach($list as $k=>$v){
			$list[$k]['day'] = date('Y-m-d',$v['reg_day']);
		}
		$list[0]['allpage'] = $allpage;
		die(json_encode($list));
	}
}

?>