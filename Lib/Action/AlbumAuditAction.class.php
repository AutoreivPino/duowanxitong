<?php
/**
 +------------------------------------------------------------------------------
 专辑审核
 +------------------------------------------------------------------------------
 */
class AlbumAuditAction extends CommonAction
{
	public function log(){
		$user_list = D("user")->field('uid,show_name')->findAll();
		$album_list = D("album")->field('id,title,source')->findAll();
		
		$content_list = D("audit_log")->findAll();
		for( $i=0; $i<count($content_list); $i++){
			foreach($user_list as $user_arr){
				if($content_list[$i]['audit_id']==$user_arr['uid']){
					$content_list[$i]['audit_id']=$user_arr['show_name'];
					continue;
				}
				if($content_list[$i]['editor_id']==$user_arr['uid']){
					$content_list[$i]['editor_id']=$user_arr['show_name'];
					continue;
				}
			}
			
			foreach($album_list as $album_arr){
				if($content_list[$i]['album_id']==$album_arr['id']){
					$content_list[$i]['album_id']=$album_arr['title'];
					$content_list[$i]['source']=$album_arr['source'];
					break;
				}
			}
			
			if($content_list[$i]['status']==0){
				$content_list[$i]['status']='待审核';
			}else if($content_list[$i]['status']==1){
				$content_list[$i]['status']='未通过';
			}else if($content_list[$i]['status']==2){
				$content_list[$i]['status']='已通过';
			}
			
			$content_list[$i]['time']=date('Y/m/d/ H:i',$content_list[$i]['time']);
		}
		$this->assign("content_list",$content_list);
		$this->display ();
	}
	
	public function statistics(){
		$user_list = D("user")->field('uid,show_name')->findAll();
		$content_list = D("audit_statistics")->findAll();
		for( $i=0; $i<count($content_list); $i++){
			foreach($user_list as $user_arr){
				if($content_list[$i]['audit_id']==$user_arr['uid']){
					$content_list[$i]['audit_id']=$user_arr['show_name'];
					continue;
				}
				if($content_list[$i]['editor_id']==$user_arr['uid']){
					$content_list[$i]['editor_id']=$user_arr['show_name'];
					continue;
				}
			}
			$content_list[$i]['total']=$content_list[$i]['pass_nums']+$content_list[$i]['fail_nums'];
			$content_list[$i]['rate']=round($content_list[$i]['fail_nums']/$content_list[$i]['total']*100).'%';
			$content_list[$i]['time']=date('Y/m/d/ H:i',$content_list[$i]['time']);
		}
		$this->assign("content_list",$content_list);
		$this->display ();
	}
}
?>