<?php

class WxAutoanswerAction extends CommonAction
{
	public function index(){
		$model = M();

		$sql = 'SELECT COUNT(DISTINCT id) AS tcount FROM '.C("DB_PREFIX").'wx_autoanswer ';
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'wx_autoanswer ';
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	public function add(){
		$model = D ('WxPic');
		$list = $model->where('status=1 and ptype=2')->order('sort,create_time desc')->select();
		$new_list = D('WxNew')->where('status=1')->order('create_time desc,id')->limit('10')->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;			
		}
		
		$this->assign('list',$list);
		$this->assign('new_list',$new_list);
		
		$this->display();
	}
	public function edit(){
		$key_id = $_REQUEST['id'];
		$vo = D('WxAutoanswer')->where('id='.$key_id)->find();
		$key_item = '';
		if($vo['gtype']==3){
			$key_item = D('WxNewItem')->where('pid='.$vo['medianew_id'])->order('sort,id')->select();
		}
		$model = D ('WxPic');
		$list = $model->where('status=1 and ptype=2')->order('sort,create_time desc')->select();
		$new_list = D('WxNew')->where('status=1')->order('create_time desc,id')->limit('10')->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;	
		}
		
		$this->assign('vo',$vo);
		$this->assign('key_item',$key_item);
		$this->assign('list',$list);
		$this->assign('new_list',$new_list);
		
		parent::edit();
	}
	public function insert()
	{
	
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		//保存当前数据对象
		$list=$model->add($data);
		if ($list !== false)
		{
		
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
		
	}
	 
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		//$data['is_all'] = isset($_REQUEST['is_all'])?$_REQUEST['is_all']:0;
		$list=$model->save($data);
		
		if (false !== $list)
		{
			//$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			//$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			if(false !== $model->where ( $condition )->delete ())
			{
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
	
	public function addajax(){
		$page = $_REQUEST['page'];
		$ajaxtype = $_REQUEST['ajaxtype'];
		$count = D('WxNew')->where('status=1')->order('id,create_time desc')->count();
		$all_page = ceil($count/10);
		$response = array('status'=>1,'data'=>'');
		if($ajaxtype=='next'){
			if($page<$all_page){
				$current = $page*10;
				$next = ($page+1)*10;
				$next_page = $page+1;
			}else{
				$response['status'] = 2;
				die(json_encode($response));
			}
		}else{
			if($page>1){
				$current = ($page-2)*10;
				$next = ($page-1)*10;	
				$next_page = $page-1;
			}else{
				$response['status'] = 2;
				die(json_encode($response));
			}
		}
		$new_list = D('WxNew')->where('status=1')->order('id,create_time desc')->limit("{$current},{$next}")->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;			
		}
		$new_list[0]['page'] = $next_page;
		$response['data'] = $new_list;
		
		die(json_encode($response));
	}
}
 
function get_token() {  
	$appid = 'wxbcb9db2f64155de8';
	$secret = '64dc86abe049f26e0656158c39d8eb4e';
	
	$data = D("Token")->find();
	if(count($data)>0){
		$token = $data['tk'];
		$ctime = $data['create_time'];	
		if ($ctime<time()) {
			$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
			$access_token = json_decode(file_get_contents($token_url));
			if (isset($access_token->errcode)) {
				echo '<h1>错误：</h1>'.$access_token->errcode;
				echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
				exit;
			}
			$token = $access_token->access_token;
			$time = time()+3600;
			$tsql = "update tbl_token set tk='".$token."',create_time=".$time;
			D("Token")->query($tsql);
			
		}
	}else{
		$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
		$access_token = json_decode(file_get_contents($token_url));
		if (isset($access_token->errcode)) {
			echo '<h1>错误：</h1>'.$access_token->errcode;
			echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
			exit;
		}
		$token = $access_token->access_token;
		$time = time()+3600;
		$tsql = "insert into tbl_token (tk,create_time) values('".$token."',".$time.")";
		D("Token")->query($tsql);
	}
	
  return $token;  
}


?>