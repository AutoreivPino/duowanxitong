<?php

class UserSettingAction extends CommonAction
{
	public function index()
	{
		$settings = array();
		$list = D("SysConf")->where('group_id = 6')->findAll();
		foreach($list as $item)
		{
			$settings[$item['name']] = $item['val'];
		}
		$this->assign("settings",$settings);
		$this->display();
	}
	
	public function update()
	{
		$settings = $_REQUEST['settings'];
		
		foreach($settings as $key => $val)
		{
			D("SysConf")->query("update tbl_sys_conf set val='$val' where name='$key'");
			//D("SysConf")->where("name = '$key'")->setField('val',$val);
		}
		
		$this->saveLog(1);
		$this->success(L('EDIT_SUCCESS'));
	}
}
?>