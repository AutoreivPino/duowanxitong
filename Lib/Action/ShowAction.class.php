<?php

class ShowAction extends CommonAction{
	
	function insert(){
	
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		
		
		if($upload_list = $this->uploadImages(0,'showcover'))
		{
		
			foreach($upload_list as $upload_item)
			{
				$img = $upload_item['recpath'].$upload_item['savename'];
			
				$data['cover']=$img;
	
			}
		
		}
	
		
		$list=$model->add($data);
		//保存当前数据对象
		
		
		
		if ($list !== false)
		{
			
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
	}
	
	
	function  update(){
		$id = $_REQUEST ['id'];
		$name=$this->getActionName();
		$model = D ($name);
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		// 更新数据
		$list=$model->save($data);
		
		if($upload_list = $this->uploadImages(0,'showcover'))
		{
		
		
			foreach($upload_list as $upload_item)
			{
				$img = $upload_item['recpath'].$upload_item['savename'];
			
				if($upload_item['key'] == 'cover')
				{
				
					$flag=D($name)->where('id = '.$id)->setField('cover',$img);
				
				}
				
			}
		}
		
		if (false !== $list)
		{
			
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	
	}


}