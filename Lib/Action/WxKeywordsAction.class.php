<?php

class WxKeywordsAction extends CommonAction
{
	public function index(){
		$model = M();

		$sql = 'SELECT COUNT(DISTINCT id) AS tcount FROM '.C("DB_PREFIX").'wx_keywords ';
		
		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'wx_keywords ';
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
		return;
	}
	public function add(){
		$model = D ('WxPic');
		$list = $model->where('status=1 and ptype=2')->order('sort,create_time desc')->select();
		$new_list = D('WxNew')->where('status=1')->order('create_time desc,id')->limit('10')->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;
			/*
			foreach($item_list as $key=>$val){
				
				$img = $val['thumb_url'];
				if(!empty($img)){
					$img_data = @file_get_contents($img);
					if(!empty($img_data)){
						$img_index = '';
						$dtime = date('md');
						
						$dir = '/server/www/res/public/upload/news/'.$dtime.'/';
						
						if(!file_exists($dir)){
							
							mkdir('/server/www/res/public/upload/news/'.$dtime.'/');
						}
						$pdir = '/server/www/res/';
						$img_index = 'public/upload/news/'.$dtime.'/'.$val['thumb_media_id'].'.jpg';
						
						$img_size = @file_put_contents($pdir.$img_index, $img_data);
						
						$isql = "update tbl_wx_new_item set local_img='".$img_index."' where id=".$val['id'];
						mysql_query($isql);
					}else{
						return;   
					}      
					 
				}
				
			}
			*/
			
		}
		
		$this->assign('list',$list);
		$this->assign('new_list',$new_list);
		
		$this->display();
	}
	public function edit(){
		$key_id = $_REQUEST['id'];
		$vo = D('WxKeywords')->where('id='.$key_id)->find();
		$key_item = '';
		if($vo['gtype']==3){
			$key_item = D('WxNewItem')->where('pid='.$vo['medianew_id'])->order('sort,id')->select();
		}
		$model = D ('WxPic');
		$list = $model->where('status=1 and ptype=2')->order('sort,create_time desc')->select();
		$new_list = D('WxNew')->where('status=1')->order('create_time desc,id')->limit('10')->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;	
		}
		
		$this->assign('vo',$vo);
		$this->assign('key_item',$key_item);
		$this->assign('list',$list);
		$this->assign('new_list',$new_list);
		
		parent::edit();
	}
	public function insert()
	{
	
		$name=$this->getActionName();
		$model = D ($name);
		if(false === $data = $model->create())
		{
			$this->error($model->getError());
		}
		$data['create_time'] = time();
		//保存当前数据对象
		$list=$model->add($data);
		if ($list !== false)
		{
		
			$this->saveLog(1,$list);
			$this->success (L('ADD_SUCCESS'));
		}
		else
		{
			$this->saveLog(0,$list);
			$this->error (L('ADD_ERROR'));
		}
		
	}
	 
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ($name);
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		$data['is_all'] = isset($_REQUEST['is_all'])?$_REQUEST['is_all']:0;
		$list=$model->save($data);
		
		if (false !== $list)
		{
			//$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			//$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		//删除指定记录
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			$datas = $model->where($condition )->field('best_img')->findAll();
			if(false !== $model->where ( $condition )->delete ())
			{
				foreach($datas as $data)
				{
					if(!empty($data['best_img']))
						@unlink(RES_ROOT.$data['best_img']);
				}
				$this->saveLog(1,$id);
			}
			else
			{
				$this->saveLog(0,$id);
				$result['isErr'] = 1;
				$result['content'] = L('REMOVE_ERROR');
			}
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		die(json_encode($result));
	}
	
	public function updatemenu(){
		$name=$this->getActionName();
		$model = D($name);
		$token = get_token();
		$list = $model->where('id=5')->find();
		$url = $list['url'];
		$data = '{
			 "button":[
			  {
					"name": "菜单", 
					"sub_button": [
						{
							"type": "view", 
							"name": "精选内容", 
							"url": "'.$url.'"
						}, 
						{
							"type": "click", 
							"name": "征集", 
							"key": "V1001_ZHENG"
						}, 
						{
							"type": "click", 
							"name": "猫宁", 
							"key": "V1001_GOOD"
						}
					]
				}]
		}';
		
		echo createMenu($token,$data); 		
	}
	
	public function addajax(){
		$page = $_REQUEST['page'];
		$ajaxtype = $_REQUEST['ajaxtype'];
		$count = D('WxNew')->where('status=1')->order('id,create_time desc')->count();
		$all_page = ceil($count/10);
		$response = array('status'=>1,'data'=>'');
		if($ajaxtype=='next'){
			if($page<$all_page){
				$current = $page*10;
				$next = ($page+1)*10;
				$next_page = $page+1;
			}else{
				$response['status'] = 2;
				die(json_encode($response));
			}
		}else{
			if($page>1){
				$current = ($page-2)*10;
				$next = ($page-1)*10;	
				$next_page = $page-1;
			}else{
				$response['status'] = 2;
				die(json_encode($response));
			}
		}
		$new_list = D('WxNew')->where('status=1')->order('id,create_time desc')->limit("{$current},{$next}")->select();
		foreach($new_list as $k=>$v){
			$id = $v['id'];
			$item_list = D('WxNewItem')->where('status=1 and pid='.$id)->order('sort,id')->select();
			$new_list[$k]['item'] = $item_list;			
		}
		$new_list[0]['page'] = $next_page;
		$response['data'] = $new_list;
		
		die(json_encode($response));
	}
}
 
 
function get_token() {  
	$appid = 'wxbcb9db2f64155de8';
	$secret = '64dc86abe049f26e0656158c39d8eb4e';
	
	$data = D("Token")->find();
	if(count($data)>0){
		$token = $data['tk'];
		$ctime = $data['create_time'];	
		if ($ctime<time()) {
			$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
			$access_token = json_decode(file_get_contents($token_url));
			if (isset($access_token->errcode)) {
				echo '<h1>错误：</h1>'.$access_token->errcode;
				echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
				exit;
			}
			$token = $access_token->access_token;
			$time = time()+3600;
			$tsql = "update tbl_token set tk='".$token."',create_time=".$time;
			D("Token")->query($tsql);
			
		}
	}else{
		$token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$secret;
		$access_token = json_decode(file_get_contents($token_url));
		if (isset($access_token->errcode)) {
			echo '<h1>错误：</h1>'.$access_token->errcode;
			echo '<br/><h2>错误信息：</h2>'.$access_token->errmsg;
			exit;
		}
		$token = $access_token->access_token;
		$time = time()+3600;
		$tsql = "insert into tbl_token (tk,create_time) values('".$token."',".$time.")";
		D("Token")->query($tsql);
	}
	
  return $token;  
}

function createMenu($token,$data){
	/*
	$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$token;
	$params = array('http' => array(
                 'method' => 'POST',
                 'content' => $data
              ));
    if ($optional_headers !== null) {
       $params['http']['header'] = $optional_headers;
    }
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if (!$fp) {
       throw new Exception("Problem with $url, $php_errormsg");
    }
    $response = @stream_get_contents($fp);
    if ($response === false) {
       throw new Exception("Problem reading data from $url, $php_errormsg");
    }
    return $response;
	*/
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$token);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$tmpInfo = curl_exec($ch);
	if (curl_errno($ch)) {
	  return curl_error($ch);
	}

	curl_close($ch);
	return $tmpInfo;

}


?>