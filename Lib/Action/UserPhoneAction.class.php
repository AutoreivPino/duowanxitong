<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class UserPhoneAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$user_name = trim($_REQUEST['user_name']);
		$phone = trim($_REQUEST['phone']);

		if(!empty($user_name))
		{
			$this->assign("user_name",$user_name);
			$parameter['user_name'] = $user_name;
			$like_name = mysqlLikeQuote($user_name);
            $where .= ' AND user_name LIKE \'%'.$like_name.'%\'';
		}

		if(!empty($phone))
		{
			$this->assign("phone",$phone);
			$parameter['phone'] = $phone;
			$where .= " AND phone = '$phone'";
		}

		$model = D('UserPhone');

		$where = 'WHERE use_type=1 ' . $where;

		$sql = 'SELECT COUNT(*) AS tcount FROM '.C("DB_PREFIX").'user_phone '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'user_phone '.$where;
		
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}

	public function edit()
	{
		$uid = intval($_REQUEST['id']);

		$u_list = D("UserPhoneAddress")->where('uid = '.$uid)->find();
		$this->assign("u_list",$u_list);
		
		parent::edit();
	}

	public function update()
	{
		$uid = intval($_REQUEST['id']);
		
		$model = D ('UserPhoneAddress');
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		$l = $model->where('uid='.$uid)->find();
		if($l['uid']>0){
			// 更新数据
			$list=$model->save($data);
		}else{
			$list=$model->data($data)->add();
		}
	
		if (false !== $list)
		{
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}
	}

	public function remove()
	{
		$id = $_REQUEST['id'];
	}

}

function getaimg($img){
	return "<img width='40' height='40' src='".$img."'>";
}
function showuse($type){
	if($type==1){
		return "完成游戏";
	}else{
		return "未完成游戏";
	}
}
function showgtype($showgtype){
	if($showgtype==0){
		return "优惠券";
	}elseif($showgtype==1){
		return "红包";
	}elseif($showgtype==2){
		return "二等奖";
	}elseif($showgtype==3){
		return "一等奖";
	}
}

?>