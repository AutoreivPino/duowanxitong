<?php

class JfUserAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$acid = trim($_REQUEST['acid']);
		$username = $_REQUEST['username'];
		$id = $_REQUEST['id'];
		
		if(!empty($username))
		{
			$this->assign("username",$username);
			$parameter['username'] = $username;
			$where.=" AND username  LIKE '%".mysqlLikeQuote($username)."%' ";
		}
		if(!empty($id))
		{
			$this->assign("id",$id);
			$parameter['id'] = $id;
			$where.=" AND id =".$id;
		}
		if(!empty($acid))
		{
			$this->assign("acid",$acid);
			$parameter['acid'] = $acid;
			$where.=" AND activity_id =".$acid;
		}
		
		$model = M();

		if(!empty($where))
		{
			$where = 'WHERE' . $where;
			$where = str_replace('WHERE AND','WHERE',$where);
		}

		$sql = 'SELECT COUNT(DISTINCT id) AS scount 
			FROM '.C("DB_PREFIX").'jf_user '.$where;
		
		$count = $model->query($sql);
		$count = $count[0]['scount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'jf_user '.$where;
			
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		$ac_list = D('Activity')->where('status=1')->select();
		$this->assign('ac_list',$ac_list);
		$this->display ();
		return;
	}
	
	public function update()
	{
        $id = intval($_REQUEST['id']);
		
		$model = D ('JfUser');
		$album = $model->getById($id);
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		// 更新数据
		$list=$model->save($data);
		if (false !== $list)
		{
			$this->saveLog(1,$id);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			//错误提示
			$this->saveLog(0,$id);
			$this->error (L('EDIT_ERROR'));
		}
	}
	
	public function remove()
	{
		@set_time_limit(0);
		if(function_exists('ini_set'))
			ini_set('max_execution_time',0);
	
		//删除指定记录
		//Vendor("common");
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];
		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
            $ids = explode (',',$id);
            foreach($ids as $aid)
            {
                
				D('JfUser')->where('id='.$aid)->delete();
               
            }

			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}

		die(json_encode($result));
	}
}

function showacname($aid){
	$name = D('Activity')->where('id='.$aid)->getField('name');
	return $name;
}

function pcount($id){
	$count = D('JfUser')->where('parent_id='.$id)->count();
	return $count;
}
function ppcount($id){
	$data = D('JfUser')->where('parent_id='.$id)->select();
	$count = 0;
	foreach($data as $k=>$v){
		$t = D('JfUser')->where('parent_id='.$v['id'])->count();
		$count += $t;
	}
	return $count;
}
?>