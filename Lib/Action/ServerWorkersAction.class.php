<?php
/**
 +------------------------------------------------------------------------------
 * 
 +------------------------------------------------------------------------------
 */
class ServerWorkersAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();

		$model = D('ServerWorkers');

		if(!empty($where))
			$where = 'WHERE 1' . $where;

		$sql = 'SELECT COUNT(id) AS tcount FROM '.C("DB_PREFIX").'server_workers '.$where;

		$count = $model->query($sql);
		$count = $count[0]['tcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'server_workers '.$where;
		$this->_sqlList($model,$sql,$count,$parameter);

		$this->display();
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$name=$this->getActionName();
		$model = D ( $name );
		
		if (false === $data = $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		$list=$model->save($data);
		
		if (false !== $list)
		{
			
			$pic= '';
			if($upload_list = $this->uploadImages()){
				foreach($upload_list as $upload_item)
				{
					$img = $upload_item['recpath'].$upload_item['savename'];
					
					if($upload_item['key'] == 'pic')
					{
						D("ServerWorkers")->where('id = '.$id)->setField('pic',$img);
					}
				}
			}
			
			$this->saveLog(1,$uid);
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ));
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			
			$this->saveLog(0,$uid);
			$this->error (L('EDIT_ERROR'));
		}

	}

	public function remove()
	{
		$result = array('isErr'=>0,'content'=>'');
		$id = $_REQUEST['id'];

		if(!empty($id))
		{
			$name=$this->getActionName();
			$model = D($name);
			$pk = $model->getPk ();
			
			$condition = array ($pk => array ('in', explode ( ',', $id ) ) );
			
			$this->saveLog(1,$id);
		}
		else
		{
			$result['isErr'] = 1;
			$result['content'] = L('ACCESS_DENIED');
		}
		
		//die(json_encode($result));

	}

}


?>