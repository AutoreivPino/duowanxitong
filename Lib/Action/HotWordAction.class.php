<?php

/**
 +------------------------------------------------------------------------------
  热搜词
 +------------------------------------------------------------------------------
 */
class HotWordAction extends CommonAction
{
	public function index()
	{
		$where = '';
		$parameter = array();
		$word = trim($_REQUEST['word']);
		
		if(!empty($word))
		{
			$where .= " AND word LIKE '%".mysqlLikeQuote($word)."%'";
			$this->assign("word",$word);
			$parameter['word'] = $word;
		}

		$model = M();
		
		if(!empty($where))
			$where = 'WHERE 1' . $where;
		
		$sql = 'SELECT COUNT(DISTINCT id) AS wcount 
			FROM '.C("DB_PREFIX").'hot_word '.$where;

		$count = $model->query($sql);
		$count = $count[0]['wcount'];

		$sql = 'SELECT * FROM '.C("DB_PREFIX").'hot_word '.$where;
		$this->_sqlList($model,$sql,$count,$parameter,'id');
		
		$this->display ();
	}
	
	public function add()
	{
		$this->display ();
	}
	
	public function insert()
	{
		$words = trim($_REQUEST['words']);
		$url = trim($_REQUEST['url']);
		$num = strpos($url,'http');
		if(!$num){
			$url = 'http://'.$url;
		}
		$data['word'] = $words;
		$data['url'] = $url;
		$old = D('HotWord')->where("word = '{$word}'")->find();
		if(isset($old['id'])){
			D('HotWord')->where("id = ".intval($old['id']))->save($data);
		}else{
			D('HotWord')->add($data);
		}	
		$this->success (L('ADD_SUCCESS'));
	}
	
	public function edit()
	{	
		$id = $_REQUEST['id'];
		$list = D('HotWord')->where("id = {$id}")->find();
		$this->assign('id',$id);
		$this->assign('list',$list);
		$this->display ();
	}
	
	public function update()
	{
		$id = intval($_REQUEST['id']);
		$data['word'] = trim($_REQUEST['words']);
		$data['url'] = trim($_REQUEST['url']);
		$num = strpos($data['url'],'http');
		if(!$num){
			$data['url'] = 'http://'.$data['url'];
		}
		// 更新数据
		$list=D('HotWord')->where("id = {$id}")->save($data);
		if (false !== $list)
		{
			$this->assign('jumpUrl', Cookie::get ( '_currentUrl_' ) );
			$this->success (L('EDIT_SUCCESS'));
		}
		else
		{
			$this->error (L('EDIT_ERROR'));
		}
	}
}	
?>