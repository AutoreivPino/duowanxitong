// JavaScript Document
$(function(){
	$(".m_title").bind('mouseover',function(){
		$(this).css("cursor","move")
	});
	
    var $list = $("#dropzoneForm");
	
	$list.sortable({
		opacity: 0.6,
		revert: true,
		cursor: 'move',
		handle: '.m_title',
		update: function(){
			$("#dropzoneForm .sharepic").each(function(i) {
                this.title=50+i;
             });
		}
	});
});